#ifndef _tga_h
#define _tga_h

#include "window.h"

int loadTGA (const char *name, int id); /* id is the texture id to bind too */

#endif