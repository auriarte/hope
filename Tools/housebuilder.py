from euclid import *

m_w = 521288629
m_z = 36243606;
def resetRandom(seed):
    global m_w,m_z
    m_w = 521288629+seed
    m_z = 36243606+seed

def rand():
    global m_w,m_z
    m_z = 36969 * (m_z & 65535) + (m_z >> 16)
    m_w = 18000 * (m_w & 65535) + (m_w >> 16)
    return (((m_z << 16) + m_w) % 36243606 / 36243606.0)
def rand_choice(seq):
    return seq[int(rand() * len(seq))]


class Shape():
    def __init__(self,points,color=0,height=0.0,rotation = 0, ratio = [1,1],center=[0,0]):
        self.points = points
        self.color = color
        self.height = height
        self.rotation = rotation
        self.ratio = ratio
        self.center = center
    def resize(self,w,h=None):
        if h==None:
            w2 = self.ratio[0]-self.ratio[0]*w
            h = (self.ratio[1]-w2)/self.ratio[1]
            self.ratio[0] *= w
            self.ratio[1] *= h
            #h = w
        else:
            self.ratio = [w,h]
        contour = [Point2(i[0],i[1]) for i in self.points]
        #center1 = Point2(sum([i[0] for i in contour])/len(contour),sum([i[0] for i in contour])/len(contour))
        #t = Matrix3.new_translate(center1[0], center1[1])
        #contour = [t*i for i in contour]
        
        t = Matrix3.new_rotate(-1*self.rotation)
        contour = [t*i for i in contour]
        
        t = Matrix3.new_translate(self.center[0]*-1, self.center[1]*-1)
        contour = [t*i for i in contour]

        
        t = Matrix3.new_scale(w,h)
        contour = [t*i for i in contour]
        
        t = Matrix3.new_rotate(self.rotation)
        contour = [t*i for i in contour]

        self.center[0]*=w
        self.center[1]*=h
        
        t = Matrix3.new_translate(self.center[0], self.center[1])
        contour = [t*i for i in contour]
        

        
        return Shape([Point3(i[0],i[1],self.height) for i in contour],self.color,self.height,self.rotation,self.ratio,self.center)
    def rotate(self,angle,reduce=True):
        contour = [Point2(i[0],i[1]) for i in self.points]
        #center1 = Point2(sum([i[0] for i in contour])/len(contour),sum([i[0] for i in contour])/len(contour))
        #t = Matrix3.new_translate(-1*center1[0], -1*center1[1])
        #contour = [t*i for i in contour]

        t = Matrix3.new_translate(self.center[0]*-1, self.center[1]*-1)
        contour = [t*i for i in contour]


        t = Matrix3.new_rotate(-1*self.rotation)
        contour = [t*i for i in contour]
        w1 = max([i[0] for i in contour])-min([i[0] for i in contour])
        h1 = max([i[1] for i in contour])-min([i[1] for i in contour])

        t = Matrix3.new_rotate(angle)
        contour = [t*i for i in contour]
        w2 = max([i[0] for i in contour])-min([i[0] for i in contour])
        h2 = max([i[1] for i in contour])-min([i[1] for i in contour])

        t = Matrix3.new_rotate(self.rotation)
        contour = [t*i for i in contour]
        
        r = min(w1/w2,h1/h2)
        #w2 = self.ratio[0]-self.ratio[0]*r
        #h = (self.ratio[1]-w2)/self.ratio[1]
        #self.ratio[0] *= r
        #self.ratio[1] *= r
        #self.ratio = [r/self.ratio[0],r/self.ratio[1]]
        if reduce:
            t = Matrix3.new_scale(r,r)
            contour = [t*i for i in contour]

            self.center[0]*=r
            self.center[1]*=r
    
            t = Matrix3.new_translate(self.center[0], self.center[1])
            contour = [t*i for i in contour]


        #t = Matrix3.new_translate(-1*center1[0], -1*center1[1])
        #contour = [t*i for i in contour]
        return Shape([Point3(i[0],i[1],self.height) for i in contour],self.color,self.height,self.rotation+angle,self.ratio,self.center)
    def move(self,ox,oy):
        contour = [Point2(i[0],i[1]) for i in self.points]
        
        t = Matrix3.new_rotate(self.rotation)*Matrix3.new_translate(ox, oy)*Matrix3.new_rotate(-1*self.rotation)
        contour = [t*i for i in contour]

        
        point = Point2(ox,oy)
        t = Matrix3.new_rotate(-1*self.rotation)
        point = t*point
        self.center = [point[0],point[1]]
        
        
        return Shape([Point3(i[0],i[1],self.height) for i in contour],self.color,self.height,self.rotation,self.ratio,self.center)

class Building:
    def __init__(self):
        self.shapes = [Shape([Point3(0.1,0.1,0.0),Point3(0.9,0.1,0.0),Point3(0.9,0.9,0.0),Point3(0.1,0.9,0.0)],0,0.0)]
        self.shapes = [Shape([Point3(-1,-1,0.0),Point3(1,-1,0.0),Point3(1,1,0.0),Point3(-1,1,0.0)],0,0.0)]
        self.builder = None
        self.axiom = 'base'
        self.max_h = 5
        self.start_h = 0.0
        productions = {'base':  [['sub_b'],
                                 ['res_b'],
                                 ['com_b'],
                                 ['ind_b'],
                                 ],
                       'sub_b': [['base_t','fence_t','flat_t','base_t','size_t','res_b'],
                                 ['base_t','fence_t','flat_t','base_t','size_t','rotate_t','res_b'],
                                 ],
                       'res_b': [['base_t', 'separator_t', 'floor1_t','top'],
                                 ['base_t', 'separator_t', 'floor1_t', 'border_t','floor1_t','top']
                                 ],
                       'com_b': [['base_t','floor2_t','com2_b'],
                                 ],
                       'ind_b': [['base_t','floor2_t','top'],
                                ],
                       'com2_b':[['separator_t','floor2_t','com2_b'],
                                 ['border_t','floor2_t','com2_b'],
                                 ['top'],
                                 ['flat_t','rotate_t','base_t','com2_b'],
                                 ['flat_t','size_t','base_t','com2_b'],
                                 #['separator_t','swirl_t','separator_t','swirl_t','separator_t','swirl_t','separator_t','top'],
                                 ['separator_t','flat_t','rotate_t','base_t','separator_t','flat_t','rotate_t','base_t','separator_t','flat_t','rotate_t','base_t','separator_t','flat_t','rotate_t','base_t','separator_t','com2_b'],
                                 ['floor1_t','rotate_t','floor1_t','rotate_t','floor1_t','rotate_t','com2_b'],
                                ],
                       'top':   [['roof'],
                                 ['separator_t','roof'],
                                 ['border_t','roof'],
                                 ['in_border_t','roof'],
                                 ['border_t','base_t','size_t','base_t','_connect','in_border_t','flat_t','size_t','base_t','roof']
                                ],
                       'roof': [['roof1_t'],
                                ['roof2_t'],
                                ['roof3_t','flat_t'],
                                ['flat_t'],
                                ],
                       'kill':  [['top']]
                       } 
        self.productions = productions
    def plan(self,seed,size,population,public,roads,cluster):
        resetRandom(seed)
        builder = self.build(None)
        self.shapes = builder.shapes
        self.builder = builder
    def generateProduction(self,max_h):
        productions = self.productions
        production = [self.axiom]
        h = 0
        while not production[-1][-2:]=='_t':
            h += 1
            production += rand_choice(productions[production.pop()])
            if h == max_h and not production[-1][-2:]=='_t':
                production.pop()
                production += rand_choice(productions['kill'])
        return production
    def build(self,filename="building.obj"):
        class Builder:
            def __init__(self,shape,h,max_h):
                self.obj_v = []
                self.obj_f = []
                self.obj_m = []
                self.shape = shape
                self.h = h
                self.shapes = []
                self.max_h = max_h
            def color(self,color):
                self.obj_m+=[(len(self.obj_f),color)]
            def _extrude(self,h):
                self.h += h
                self.base_t()
                self._connect()
            def _connect(self):
                ts = len(self.obj_v)-1
                ss = len(shape.points)
                for i in range(len(shape.points)-1):
                    self.obj_f+=[(ts-i,ts-1-i,ts-ss-1-i,ts-ss-i)]
                self.obj_f+=[(ts-ss+1,ts,ts-ss,ts-ss-ss+1)]               
            def base_t(self):
                for side in self.shape.points:
                    self.obj_v+=[(side[0],self.h,side[1])]
                self.shapes.append(Shape(self.shape.points,0,self.h))
            def separator_t(self):
                self.color('aqua')
                self._extrude(0.1)
            def border_t(self):
                self.color('bronze')
                self.base_t()
                smaller = self.shape
                self.shape = self.shape.resize(1.05)
                self.base_t()
                self._connect()
                self.h += 0.1
                self.base_t()
                self._connect()
                self.shape = smaller
                self.base_t()
                self._connect()
            def in_border_t(self,h=0.1):
                self.color('red')
                self.base_t()
                self.shape = self.shape.resize(0.95)
                self.base_t()
                self._connect()
                self.h -= h
                self.base_t()
                self._connect()
            def fence_t(self):
                self.color('brown')
                self._extrude(0.4)
                self.in_border_t(0.4)
            def floor1_t(self):
                self.color('ltgrey')
                self._extrude(0.5)
            def floor2_t(self):
                if self.max_h>0.0:
                    self.color('blue')
                    h = max(0.5,rand()*self.max_h)
                    self._extrude(h)
                    self.max_h -= h
            def flat_t(self):
                self.color('ltgrey')
                ts = len(self.obj_v)-1
                self.obj_f+=[(ts-3,ts-2,ts-1,ts-0)]
            def roof1_t(self):
                self.color('orange')
                ss = len(shape.points)
                c = Point2(sum([i[0] for i in shape.points])/ss,sum([i[1] for i in shape.points])/ss)
                self.obj_v+=[(c[0],self.h+0.3,c[1])]
                ts = len(self.obj_v)-1
                first=ts-ss
                for i in range(ss-1):
                    self.obj_f+=[(ts,first+i,first+i+1)]
                self.obj_f+=[(ts,ts-1,ts-ss)]
            def roof2_t(self):
                self.color('orange')
                self.base_t()
                self.shape = self.shape.resize(1.0,0.001)
                self.h += 0.3
                self.base_t()
                self._connect()
                ts = len(self.obj_v)-1
                self.obj_f+=[(ts-3,ts-2,ts-1,ts-0)]
            def roof3_t(self):
                self.color('orange')
                self.base_t()
                self.shape = self.shape.resize(0.6,0.1)
                self.h += 0.3
                self.base_t()
                self._connect()
            def rotate_t(self):
                self.shape = self.shape.rotate(math.pi/10)
            def swirl_t(self):
                self.shape = self.shape.rotate(math.pi/10,False)
            def size_t(self):
                self.shape = self.shape.resize(0.8)
            def save(self,filename):
                print "Printing file",filename
                with open(filename,'w') as f:
                    f.write("mtllib ./v.mtl\n")
                    for i in self.obj_v:
                        f.write("v %f %f %f\n" % i)
                    for k,i in enumerate(self.obj_f):
                        if self.obj_m and self.obj_m[0][0]==k:
                            f.write("usemtl %s\n" % self.obj_m.pop(0)[1])
                        if len(i)==4:
                            f.write("f %d %d %d\n" % tuple([j+1 for j in [i[0],i[1],i[3]]]))
                            f.write("f %d %d %d\n" % tuple([j+1 for j in [i[1],i[2],i[3]]]))
                        elif len(i)==3:
                            f.write("f %d %d %d\n" % tuple([j+1 for j in i]))
        production = self.generateProduction(20)
        #production = ['base_t','floor1_t','flat_t','rotate_t','base_t','floor1_t','rotate_t','floor1_t','rotate_t','floor1_t','rotate_t','floor1_t',]
        #production = ['base_t','floor1_t','flat_t','rotate_t','base_t','separator_t','swirl_t','separator_t','swirl_t','separator_t','swirl_t','separator_t','swirl_t','separator_t','swirl_t','separator_t','swirl_t','separator_t','swirl_t','separator_t','swirl_t','separator_t','swirl_t','separator_t','swirl_t','separator_t','swirl_t','separator_t','swirl_t','separator_t','swirl_t','separator_t','swirl_t','separator_t','swirl_t','separator_t','swirl_t','separator_t','swirl_t','separator_t','swirl_t','separator_t','swirl_t','separator_t','swirl_t','separator_t','swirl_t','separator_t','roof_t']
        #production = ['base_t','floor1_t','separator_t','floor1_t','roof_t']
        #production = ['base_t','floor1_t','separator_t','separator_t','separator_t','roof3_t']
        shape = Shape([Point2(-1,1),Point2(1,1),Point2(1,-1),Point2(-1,-1)])
        builder = Builder(shape,self.start_h,self.max_h)
        print "Building string: ",production
        for rule in production:
            getattr(builder,rule)()
        if filename:
            builder.save(filename)
        return builder

if __name__ == "__main__":
    b = Building()
    b.axiom = 'sub_b'
    b.plan(1,0,0,0,0,0)
    b.builder.save('new_building.obj')   