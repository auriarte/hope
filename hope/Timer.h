#ifndef __TIMER_H_
#define __TIMER_H_

#include "SDL/SDL.h"

class Timer {
public:
            Timer();
    
    void    Start();
    void    Stop();
    void    Pause();
    void    Unpause();
    int     GetTicks();
    bool    IsStarted();
    bool    IsPaused();
    
private:
    int     _startTicks;
    int     _pausedTicks;
    bool    _paused;
    bool    _started;
    
};

#endif