// For all the magic numbers, see http://www.codeproject.com/Articles/25172/Simple-Random-Number-Generation

#include "DeterministicRandomNumberGenerator.h"

DeterministicRandomNumberGenerator::DeterministicRandomNumberGenerator(){
    m_w = 521288629;
    m_z = 362436069;
    m_w0 = m_w;
    m_z0 = m_z;
}

DeterministicRandomNumberGenerator::~DeterministicRandomNumberGenerator(){
    
}
uint32_t DeterministicRandomNumberGenerator::nextInt(void){
    m_z = 36969 * (m_z & 65535) + (m_z >> 16);
    m_w = 18000 * (m_w & 65535) + (m_w >> 16);
    return (m_z << 16) + m_w;
    
}
double DeterministicRandomNumberGenerator::nextDouble(void){
    return (double)this->nextInt()/(double)UINT_MAX;
}

void DeterministicRandomNumberGenerator::setSeed(uint32_t z){
    m_z = z;
    m_z0 = z;
}

void DeterministicRandomNumberGenerator::reset(){
    m_z = m_z0;
    m_w = m_w0;
}

void DeterministicRandomNumberGenerator::randomize(){
    this->setSeedFromSystemTime();
}

void DeterministicRandomNumberGenerator::setSeedFromSystemTime(){
    this->setSeed( (uint32_t)time( NULL ) );
}
