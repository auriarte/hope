'''
820856226
2331188998
4033440000
3169966213
2572821606
100826968
1697244543
4074869929
573303256
3663481940
'''
m_w = 521288629
m_z = 36243606;
def resetRandom(seed):
    global m_w,m_z
    m_w = 521288629+seed
    m_z = 36243606+seed

def rand():
    global m_w,m_z
    m_z = 36969 * (m_z & 65535) + (m_z >> 16)
    m_w = 18000 * (m_w & 65535) + (m_w >> 16)
    return (((m_z << 16) + m_w) % 36243606 / 36243606.0)
def choice(l):
    return l[int(len(l)*rand())]


from Tkinter import *
import itertools
import math
from housebuilder import *

def s(x):
    return math.tanh(x*2)

class MapType:
    MAP_EMPTY = 0
    MAP_BLOCK = 1
class MapStyle:
    MAP_EMPTY_UNDEVELOPED = 0
    MAP_EMPTY_STREET = 1
    MAP_EMPTY_PUBLIC = 2
    MAP_BLOCK_PUBLIC = 3
    MAP_BLOCK_RESIDENTIAL = 4
    MAP_BLOCK_COMERCIAL = 5
    MAP_BLOCK_INDUSTRIAL = 6
    
styleColors = ["#%02x%02x%02x" % (0, 0, 0),
               "#%02x%02x%02x" % (128, 128, 128),
               "#%02x%02x%02x" % (0, 255, 255),
               "#%02x%02x%02x" % (255, 0, 255),
               "#%02x%02x%02x" % (50, 192, 50),
               "#%02x%02x%02x" % (50, 50, 192),
               "#%02x%02x%02x" % (160, 160, 0),
               "#%02x%02x%02x" % (255, 255, 255),
               ]


class Tile:
    type = MapType.MAP_EMPTY
    style = MapStyle.MAP_EMPTY_UNDEVELOPED
    tag = None
    value = 0

class Map:
    def color(self,color):
        self.obj_m+=[(len(self.obj_f),color)]
    def __init__(self,size):
        self.tiles = []
        self.pois = []        
        self.size = size
        for i in xrange(size**2):
            self.tiles.append(Tile())
        self.tiles.append(Tile())
        self.blocks = []
        self.streets_w = [1000,1000]
        self.streets_h = [1000,1000]
        self.building_seed = 0
    def block_xy(self,x,y,style,tag,override):
        if True or override or not self.tiles[y*self.size+x].style:
            self.tiles[y*self.size+x].style = style
            self.tiles[y*self.size+x].tag = tag
    def line_w(self,y,style,tag,override):
        if y >= self.size: return
        prox = [abs(i-y) for i in self.streets_w]
        if False and min(prox)<4 and not min(prox)==1:
            return
        self.streets_w.append(y)
        for x in xrange(self.size):
            self.block_xy(x, y, style, tag, override)
    def line_h(self,x,style,tag,override):
        if x >= self.size: return
        prox = [abs(i-x) for i in self.streets_h]
        if False and min(prox)<4 and not min(prox)==1:
            return
        self.streets_h.append(x)
        for y in xrange(self.size):
            self.block_xy(x, y, style, tag, override)
    def block_xyc(self,cx,cy,c,style,tag,override):
        c += 1
        for x in xrange(int(max(0,cx-c/2)),int(min(self.size,cx+c/2))):
            for y in xrange(int(max(0,cy-c/2)),int(min(self.size,cy+c/2))):
                self.block_xy(x, y, style, tag, override)
    def block_xywh(self,cx,cy,w,h,style,tag,override):
        for x in xrange(cx,cx+w):
            for y in xrange(cy,cy+h):
                self.block_xy(x, y, style, tag, override)
    def plan(self,seed,size,population,public,roads,cluster):
        resetRandom(seed)
        #self.plan_public(seed,size,population,public,roads)
        #self.plan_roads(seed,size,population,roads)
        self.plan_urban_simple(seed,size,population,public,population)
        self.plan_suburban_simple(seed,size,population,public,roads)
        self.plan_industrial_simple(seed,size,population,public,roads)
        self.plan_public(seed,size,population,public,roads)
        self.propagate_interest(seed,size,population,public,roads,cluster)
        self.fix_boundaries(seed,size,population,public,roads,cluster)
        self.fix_inconsistencies()
        self.plan_maze(seed)
        #self.recalificate(seed,size,population,public,roads,cluster)
    def plan_maze(self,seed):
        def n(tile):
            ret = []
            if not tile % self.size == 0:
                ret.append(tile-1)
            if not tile-1 % self.size == 0:
                ret.append(tile+1)
            if tile >= self.size:
                ret.append(tile-self.size)
            if tile < self.size**2-self.size:
                ret.append(tile+self.size)
            return ret
        for i in xrange(self.size**2):
            if self.tiles[i].style==MapStyle.MAP_EMPTY_STREET:
                self.tiles[i].style=MapStyle.MAP_EMPTY_PUBLIC
        open = set([i for i in range(self.size**2) if self.tiles[i].style==MapStyle.MAP_EMPTY_PUBLIC])
        closed = set([])
        fringe = [0]
        while fringe:
            print len(open),len(closed),len(fringe)
            t = choice(fringe)
            fringe.remove(t)
            self.tiles[t].style = MapStyle.MAP_EMPTY_STREET
            closed.add(t)
            if t in open: open.remove(t)
            for j in [i for i in n(t) if i in open]:
                if j not in closed and [i for i in n(j) if i in open]:
                    fringe.append(j)
                open.remove(j)
        for i in xrange(self.size**2):
            if self.tiles[i].style==MapStyle.MAP_EMPTY_PUBLIC and sum([1 for j in n(i) if self.tiles[j].style==MapStyle.MAP_EMPTY_STREET])<2:
                self.tiles[i].style=MapStyle.MAP_EMPTY_STREET
            
    def fix_inconsistencies(self):
        def nn(x,y):
            n = self.size*y+x
            return [n,n+1,n+self.size,n+self.size+1]
        for y in xrange(self.size-1):
            for x in xrange(self.size-1):
                styles = [self.tiles[i].style for i in nn(x,y)]
                if len([i for i in styles if i==MapStyle.MAP_BLOCK_INDUSTRIAL])==3:
                    for i in nn(x,y):
                        self.tiles[i].style = MapStyle.MAP_BLOCK_INDUSTRIAL
                        self.tiles[i].value = max(0,self.tiles[i].value)
                elif len([i for i in styles if i==MapStyle.MAP_BLOCK_RESIDENTIAL])==2 and MapStyle.MAP_BLOCK_COMERCIAL in styles:
                    for i in nn(x,y):
                        if self.tiles[i].style == MapStyle.MAP_BLOCK_COMERCIAL:
                            self.tiles[i].style = MapStyle.MAP_EMPTY_STREET
        for y in xrange(self.size):
            for i in [self.size*(self.size-1)+y,self.size*(y+1)-1]:
                self.tiles[i].style = MapStyle.MAP_EMPTY_STREET
                self.tiles[i].type = MapType.MAP_BLOCK

                
    def draw_obj(self,seed,size,population,public,roads,cluster,navigation,lots_counter=0):
        self.obj_v = []
        self.obj_f = []   
        self.obj_m = []
        def qbi(x,y):
            x = min(max(0,x),self.size-1)
            y = min(max(0,y),self.size-1)
            return x+y*self.size
        def ibq(current):
            x = current % self.size
            y = int(math.floor(current/self.size))
            return [x,y]
        def qex1(poi):
            l = zip([poi.c[0]-1]*(poi.s[1]+2),range(poi.c[1]-1,poi.c[1]+poi.s[1]+1)) #l
            l += zip([poi.c[0]+poi.s[0]]*(poi.s[1]+2),range(poi.c[1]-1,poi.c[1]+poi.s[1]+1)) #r
            l += zip(range(poi.c[0],poi.c[0]+poi.s[0]+1),[poi.c[1]-1]*(poi.s[0])) #t
            l += zip(range(poi.c[0],poi.c[0]+poi.s[0]+1),[poi.c[1]+poi.s[1]]*(poi.s[0])) #t
            return l
        def gn(x,y):
            #x = n % self.size
            #y = math.floor(n/self.size)
            if x < 0 or x >= self.size or y < 0 or y >= self.size:
                return -1
            else:
                return x+y*self.size
        def n(n):
            x = n % self.size
            y = int(math.floor(n/self.size))
            w = [gn(x-1,y),gn(x+1,y),gn(x,y-1),gn(x,y+1)]
            return w
        def xy(n):
            x = n % self.size
            y = math.floor(n/self.size)
            return (x,y)
        def add_building(n,s,z,axiom='res_b',start_h=0.0):
            if not navigation:
                bl = Building()
                bl.axiom = axiom
                bl.start_h = start_h
                bl.max_h = start_h+3
                self.building_seed += 1
                bl.plan(self.building_seed,0,0,0,0,0)
                base_v = len(self.obj_v)
                base_f = len(self.obj_f)
                b = 1.0/self.size
                w = s[0]
                h = s[1]
                x = n % self.size
                y = math.floor(n/self.size)
                x *= b
                y *= b
    
                current_color = self.obj_m[-1][1]        
                self.obj_v+=[(i[0]*b/2*w+x+b/2*w,i[1]*b,i[2]*b/2*h+y+b/2*h) for i in bl.builder.obj_v]
                self.obj_f+=[[j+base_v+1 for j in i] for i in bl.builder.obj_f]
                self.obj_m+=[(i[0]+base_f,i[1]) for i in bl.builder.obj_m]
                self.color(current_color)
            else:
                addblock(n,s,z)
        def addblock(n,s,z):
            self.color('black')
            b = 1.0/self.size
            # b = 16m
            w = s[0]
            h = s[1]
            x = n % self.size
            y = math.floor(n/self.size)
            x *= b
            y *= b
            w *= b
            h *= b
            z *= b*0.125*1.25 #2.5m per floor
            z += b*0.125 #2m roof
            if not navigation:
                sep = 0.1*b
            else:
                sep = 0.0
            #ceiling vertices
            self.obj_v += [(x+sep,z,y+sep),(x+w-sep,z,y+sep),(x+w-sep,z,y+h-sep),(x+sep,z,y+h-sep)]
            #ceiling face
            self.obj_f += [tuple(range(len(self.obj_v),len(self.obj_v)-4,-1))]
            #floor vertices
            z = 0
            self.obj_v += [(x+sep,z,y+sep),(x+w-sep,z,y+sep),(x+w-sep,z,y+h-sep),(x+sep,z,y+h-sep)]
            #south, east, north faces         
            for i in range(len(self.obj_v),len(self.obj_v)-3,-1):
                self.obj_f += [(i,i-1,i-1-4,i-4)]
            #west face
            i = len(self.obj_v)
            self.obj_f += [(i,i-4,i-3-4,i-3)]
        def addblockade(n):
            self.color('white')
            b = 1.0/self.size
            # b = 16m
            for c in range(8):
                w = 1.0/8
                h = 1.0/8
                x = n % self.size
                y = math.floor(n/self.size)
                x *= b
                
                y *= b
                
                w *= b
                x+=w*c
                h *= b
                y+=h*c
                z = b*0.125*1.25 #2.5m per floor
                sep = 0.0
                #ceiling vertices
                self.obj_v += [(x+sep,z,y+sep),(x+w-sep,z,y+sep),(x+w-sep,z,y+h-sep),(x+sep,z,y+h-sep)]
                #ceiling face
                self.obj_f += [tuple(range(len(self.obj_v),len(self.obj_v)-4,-1))]
                #floor vertices
                z = 0
                self.obj_v += [(x+sep,z,y+sep),(x+w-sep,z,y+sep),(x+w-sep,z,y+h-sep),(x+sep,z,y+h-sep)]
                #south, east, north faces         
                for i in range(len(self.obj_v),len(self.obj_v)-3,-1):
                    self.obj_f += [(i,i-1,i-1-4,i-4)]
                #west face
                i = len(self.obj_v)
                self.obj_f += [(i,i-4,i-3-4,i-3)]
        def addboundariesground():
            d = len(self.obj_v)
            self.obj_v += [(0.0, 0.0, 0.0),(1.0,0.0,0.0),(1.0,0.0,1.0),(0.0,0.0,1.0)]
            self.obj_f += [(d+4,d+3,d+2,d+1)]
        def addboundaries():
            d = len(self.obj_v)
            self.obj_v += [(0.0, 0.0, 0.0),(1.0,0.0,0.0),(1.0,0.0,1.0),(0.0,0.0,1.0)]
            self.obj_v += [(0.0,0.5,0.0),(1.0,0.5,0.0),(1.0,0.5,1.0),(0.0,0.5,1.0)]
            #ground
            self.obj_f += [(d+4,d+3,d+2,d+1)]
            #north wall, facing south
            self.obj_f += [(d+1,d+2,d+2+4,d+1+4)]
            #east, south
            self.obj_f += [(d+2,d+3,d+3+4,d+2+4)]
            self.obj_f += [(d+3,d+4,d+4+4,d+3+4)]
            #west
            self.obj_f += [(d+4,d+1,d+4+1,d+4+4)]
        def find_contourn(block):
            dirs = [[1,0],[0,1],[-1,0],[0,-1]]
            dirs = dirs * 3
            d = 4
            start = ibq(min(block))
            current = list(start)
            print current
            w = 1#/self.size
            x,y =start
            c = [[x,y],[x+1,y]]
            while True:
                current2 = qbi(current[0],current[1])
                nextd = -2
                p = c.pop()
                #check previous
                next = list(current)
                next[0]+=dirs[d][0]+dirs[d-1][0]
                next[1]+=dirs[d][1]+dirs[d-1][1]
                next2 = qbi(next[0],next[1])
                if qbi(next[0],next[1]) in block:
                    nextd = -1
                #check straight
                next = list(current)
                next[0]+=dirs[d][0]
                next[1]+=dirs[d][1]
                next2 = qbi(next[0],next[1])
                if nextd==-2:
                    if qbi(next[0],next[1]) in block:
                        nextd = 0
                    else:
                        nextd = 1
                d += nextd
                if not nextd==0:
                    p2 = list(p)
                    c.append(p2)
                    if nextd==-1:
                        current[0]+=dirs[d][0]+dirs[d+1][0]
                        current[1]+=dirs[d][1]+dirs[d+1][1]
                else:                
                    current[0]+=dirs[d][0]
                    current[1]+=dirs[d][1]
                p[0]+=dirs[d][0]*w
                p[1]+=dirs[d][1]*w
                if p[0] == c[0][0] and p[1]==c[0][1]: break  
                c.append(p)
            #print "contour",block
            #print c
            return c
        def extrude(c,h):
            last = c[0]
            self.obj_v+=[(last[0]*1.0/self.size,0.0,last[1]*1.0/self.size)]
            self.obj_v+=[(last[0]*1.0/self.size,h,last[1]*1.0/self.size)]
            j = len(self.obj_v)
            ceiling = [j]
            for side in c[1:]:
                self.obj_v+=[(side[0]*1.0/self.size,0.0,side[1]*1.0/self.size)]
                self.obj_v+=[(side[0]*1.0/self.size,h,side[1]*1.0/self.size)]
                i = len(self.obj_v)+1
                ceiling += [i-1]
                self.obj_f+=[(i-4,i-3,i-1,i-2)]
            self.obj_f+=[(i-2,i-1,j,j-1)]
            draw_contour(ceiling)
            
        def draw_contour(c):
            #http://www.critterai.org/javadoc/nmgen/org/critterai/nmgen/PolyMeshFieldBuilder.html#build(org.critterai.nmgen.ContourSet)
            pass

        def draw_contourn(block,z,block_size=None,m_color='ltgrey'):
            self.color(m_color)
            c = find_contourn(block)
            if block_size:
                b = block_size
            else:
                b = 1.0/self.size
            z *= b*0.125*1.25 #2.5m per floor
            #This makes side walls only
            extrude(c,z)
            #Make roof
            print "block",block
            c = find_grid(block)
            for i,w in c:
                draw_tile(i,z,w)
        def find_grid(block):
            l = []
            while block:
                i= min(block)
                for w in range(int(math.sqrt(len(block))),0,-1):
                    s = set()
                    for x in range(w):
                        for y in range(w):
                            s.add(i+x+self.size*y)
                    if not s - block:
                        l.append((i,w))
                        block -= s
            return l
        def draw_tile(tile,h,ww=1):
            x,y = xy(tile)
            w = 1.0/self.size
            x *= w
            y *= w
            self.obj_v +=[(x,h,y),(x,h,y+w*ww),(x+w*ww,h,y+w*ww),(x+w*ww,h,y)]
            i=len(self.obj_v)
            self.obj_f +=[(i-3,i-2,i-1,i)]
        def do_map(do_ground=True):
            tagged = set([])
            #Add all vertices on the ground
            w = 1.0/self.size
            for y in xrange(self.size+1):
                for x in xrange(self.size+1):
                    if do_ground:
                        self.obj_v.append((x*w,0.000,y*w))
            for i in xrange(self.size**2):
                if False:
                    #Add all tiles to the ground mesh
                    x,y = xy(i)
                    self.obj_f += [(x+y*(self.size+1)+1,
                                    x+(y+1)*(self.size+1)+1,
                                    x+1+(y+1)*(self.size+1)+1,
                                    x+1+(y)*(self.size+1)+1
                                    )]
    
                if i in tagged: continue
                if self.tiles[i].style==MapStyle.MAP_EMPTY_STREET or self.tiles[i].style==MapStyle.MAP_BLOCK_PUBLIC:
                    if self.tiles[i].style==MapStyle.MAP_BLOCK_PUBLIC:
                        first = self.tiles[i]
                        stack = 0
                        block = set()
                        fringe = [i]
                        while fringe:
                            c = fringe.pop()
                            stack += 1
                            block.add(c)
                            self.tiles[c].value = -0.1
                            tagged.add(c)
                            for p in n(c):
                                if p not in tagged and self.tiles[p].style ==MapStyle.MAP_BLOCK_PUBLIC:
                                    fringe.append(p)
                        if block and not navigation:
                            draw_contourn(block,1.1,None,'green')
                        if block and navigation:
                            if do_ground:
                                #Add road grid
                                for j in block:
                                    x,y = xy(j)
                                    self.obj_f += [(x+y*(self.size+1)+1,
                                                    x+(y+1)*(self.size+1)+1,
                                                    x+1+(y+1)*(self.size+1)+1,
                                                    x+1+(y)*(self.size+1)+1
                                                    )]
                    else:
                        tagged.add(i)
                        if do_ground:
                            self.color('asphalt')
                            #Add road grid
                            x,y = xy(i)
                            self.obj_f += [(x+y*(self.size+1)+1,
                                            x+(y+1)*(self.size+1)+1,
                                            x+1+(y+1)*(self.size+1)+1,
                                            x+1+(y)*(self.size+1)+1
                                            )]
    
                elif self.tiles[i].style==MapStyle.MAP_EMPTY_PUBLIC:
                    tagged.add(i)
                    if do_ground:
                        self.color('dkgreen')
                        x,y = xy(i)
                        self.obj_f += [(x+y*(self.size+1)+1,
                                        x+(y+1)*(self.size+1)+1,
                                        x+1+(y+1)*(self.size+1)+1,
                                        x+1+(y)*(self.size+1)+1
                                        )]
                    addblockade(i)
                    
                elif self.tiles[i].style==MapStyle.MAP_BLOCK_RESIDENTIAL:
                    add_building(i,[1,1],self.tiles[i].value,'res_b' if (int(self.tiles[i].value/4)+1 > 2) else 'sub_b')
                    tagged.add(i)
                    if self.tiles[i].value==0:
                            self.tiles[i].style=MapStyle.MAP_EMPTY_STREET
                            self.tiles[i].value=-0.1
                else:
                    first = self.tiles[i]
                    stack = 0
                    block = set()
                    fringe = [i]
                    while fringe:
                        c = fringe.pop()
                        stack += 1
                        block.add(c)
                        self.tiles[c].value = -0.1
                        tagged.add(c)
                        for p in n(c):
                            if p not in tagged and self.tiles[p].style in [MapStyle.MAP_BLOCK_COMERCIAL,MapStyle.MAP_BLOCK_INDUSTRIAL]:
                                fringe.append(p)
                    if len(block) > 9:
                        for j in block:
                            self.tiles[j].style = MapStyle.MAP_BLOCK_INDUSTRIAL
                        first.value = i
                    else:
                        for j in block:
                            self.tiles[j].style = MapStyle.MAP_BLOCK_COMERCIAL
                        first.value = i
                    #square blocks
                    if self.tiles[i].style==MapStyle.MAP_BLOCK_INDUSTRIAL:
                        x1,y1 = xy(min(block))
                        x2,y2 = xy(max(block))
                        blocks = set()
                        for x in range(x1,x2+1):
                            for y in range(int(y1),int(y2)+1):
                                blocks.add(x+y*self.size)
                        for b in block-blocks:
                            addblock(b,[1,1],3)

                        #addblock(min(block),[x2-x1+1,y2-y1+1],10)
                        add_building(min(block),[x2-x1+1,y2-y1+1],4,'ind_b')
                    if self.tiles[i].style==MapStyle.MAP_BLOCK_COMERCIAL:
                        if navigation:
                            draw_contourn(block,5)
                        else:
                            val = max(2,math.sqrt(sum([self.tiles[i].value for i in block])/len(block)))
                            if len(block)==9:
                                add_building(min(block),[3,3],val,'com_b')
                            elif rand()>0.5:
                                add_building(min(block),[len(set([min(block),min(block)+1,min(block)+2]) & block),1],val/2,'com_b',val*0.5*0.125*1.25)
                                draw_contourn(block,val/2)
                                
                            #elif set([min(block),min(block)+self.size,min(block)+self.size*2]) & block:
                            #elif set([max(block),min(block)-self.size,min(block)-self.size*2]) & block:
                            else:
                                draw_contourn(block,val)
                        #com_b 
        def do_sidewalk():
            m = 3
            side = 1
            tiles2 = [0]*len(self.tiles)*m*m

            for y in range(self.size):
                for x in range(self.size):
                    val = 0 if self.tiles[qbi(x,y)].style in [MapStyle.MAP_EMPTY_STREET, MapStyle.MAP_EMPTY_PUBLIC] else 3
                    print val,
                    for y2 in range(y*m,y*m+m):
                        for x2 in range(x*m,x*m+m):
                            tiles2[x2+y2*self.size*m]=val
                print
            for s in range(side):
                for y in range(self.size*m):
                    for x in range(self.size*m):
                        if tiles2[x+y*self.size*m]>=2:
                            for n in [x+1+y*self.size*m,x-1+y*self.size*m,x+(y+1)*self.size*m,x+(y-1)*self.size*m,
                                      x+(y+1)*self.size*m+1,x+(y-1)*self.size*m+1,x+(y+1)*self.size*m-1,x+(y-1)*self.size*m-1]:
                            #for n in [x+1+y*self.size*m,x-1+y*self.size*m,x+(y+1)*self.size*m,x+(y-1)*self.size*m]:
                                if not tiles2[n]:
                                    tiles2[n]=1
                                
                for i in range(self.size*self.size*m*m):
                    if tiles2[i]==1:
                        tiles2[i]=2

            for y in range(self.size*m):
                for x in range(self.size*m):
                    print tiles2[x+y*self.size*m],
                print
                    

    
            tagged = set([])
            w = 1.0/(self.size*m)
            for i in xrange((self.size*m)**2):
    
                if i in tagged: continue
                if tiles2[i]==0:
                    tagged.add(i)
                else:
                    first = tiles2[i]
                    stack = 0
                    block = set()
                    fringe = [i]
                    while fringe:
                        c = fringe.pop()
                        stack += 1
                        block.add(c)
                        tagged.add(c)
                        
                        for p in [c+1,c-1,c+self.size*m,c-self.size*m]:
                            if p not in tagged and tiles2[p]:
                                fringe.append(p)
                    print "block",i,len(block)
                    self.size *= m
                    draw_contourn(block,1,None,'sidewalk')
                    self.size /= m
    

        self.color('asphalt')
        if not navigation:
            addboundariesground()
        do_map(navigation)
        self.color('aqua')
        #addblock(self.size**2/2,[5,5],50)
        if navigation:
            pass
            #addboundaries()
            #addboundariesground()
        else:
            pass
            #addboundariesground()
            do_sidewalk()
        print "Printing file"
        with open('map_%d_nav.obj'%lots_counter if navigation else 'map_%d_normalized.obj'%lots_counter,'w') as f:
            if not navigation: f.write("mtllib ./v.mtl\n")
            for i in self.obj_v:
                f.write("v %f %f %f\n" % tuple([j*self.size*8 for j in i]))
            for k,i in enumerate(self.obj_f):
                if not navigation and self.obj_m and self.obj_m[0][0]==k:
                    while self.obj_m and self.obj_m[0][0]==k:
                        mat = self.obj_m.pop(0)[1]
                    f.write("usemtl %s\n" % mat)
                if len(i)==4:
                    #f.write("f %d %d %d %d\n" % i)
                    f.write("f %d %d %d\n" % tuple([j for j in [i[0],i[1],i[3]]]))
                    f.write("f %d %d %d\n" % tuple([j for j in [i[1],i[2],i[3]]]))
                elif len(i)==3:
                    f.write("f %d %d %d\n" % tuple([j for j in i]))



        print "Printing done"

    def fix_boundaries(self,seed,size,population,public,roads,cluster):
        def qbi(x,y):
            x = min(max(0,x),self.size-1)
            y = min(max(0,y),self.size-1)
            return x+y*self.size
        def qex1(poi):
            l = zip([poi.c[0]-1]*(poi.s[1]+2),range(poi.c[1]-1,poi.c[1]+poi.s[1]+1)) #l
            l += zip([poi.c[0]+poi.s[0]]*(poi.s[1]+2),range(poi.c[1]-1,poi.c[1]+poi.s[1]+1)) #r
            l += zip(range(poi.c[0],poi.c[0]+poi.s[0]+1),[poi.c[1]-1]*(poi.s[0])) #t
            l += zip(range(poi.c[0],poi.c[0]+poi.s[0]+1),[poi.c[1]+poi.s[1]]*(poi.s[0])) #t
            return l
        def gn(x,y):
            #x = n % self.size
            #y = math.floor(n/self.size)
            if x < 0 or x >= self.size or y < 0 or y >= self.size:
                return -1
            else:
                return x+y*self.size
        def n(n):
            x = n % self.size
            y = int(math.floor(n/self.size))
            w = [gn(x-1,y),gn(x+1,y),gn(x,y-1),gn(x,y+1)]
            return w 
        tagged = set([])
        for i in xrange(self.size**2):
            if i in tagged: continue
            if self.tiles[i].style==MapStyle.MAP_EMPTY_STREET or self.tiles[i].style==MapStyle.MAP_BLOCK_PUBLIC:
                tagged.add(i)
            elif self.tiles[i].style==MapStyle.MAP_BLOCK_RESIDENTIAL:
                tagged.add(i)
                if self.tiles[i].value==0:
                        self.tiles[i].style=MapStyle.MAP_EMPTY_STREET
                        self.tiles[i].value=-0.1
            else:
                first = self.tiles[i]
                pop = 0
                stack = 0
                block = set()
                fringe = [i]
                while fringe:
                    c = fringe.pop()
                    pop += self.tiles[c].value
                    stack += 1
                    block.add(c)
                    self.tiles[c].value = -0.1
                    tagged.add(c)
                    for p in n(c):
                        if p not in tagged and self.tiles[p].style in [MapStyle.MAP_BLOCK_COMERCIAL,MapStyle.MAP_BLOCK_INDUSTRIAL]:
                            fringe.append(p)
                if stack > 13:
                    for j in block:
                        self.tiles[j].style = MapStyle.MAP_BLOCK_INDUSTRIAL
                    first.value = i
                else:
                    for j in block:
                        self.tiles[j].style = MapStyle.MAP_BLOCK_COMERCIAL
                    first.value = i
                    
                

    def propagate_interest(self,seed,size,population,public,roads,cluster):
        def qbi(x,y):
            x = min(max(0,x),self.size-1)
            y = min(max(0,y),self.size-1)
            return x+y*self.size
        def qex1(poi):
            l = zip([poi.c[0]-1]*(poi.s[1]+2),range(poi.c[1]-1,poi.c[1]+poi.s[1]+1)) #l
            l += zip([poi.c[0]+poi.s[0]]*(poi.s[1]+2),range(poi.c[1]-1,poi.c[1]+poi.s[1]+1)) #r
            l += zip(range(poi.c[0],poi.c[0]+poi.s[0]+1),[poi.c[1]-1]*(poi.s[0])) #t
            l += zip(range(poi.c[0],poi.c[0]+poi.s[0]+1),[poi.c[1]+poi.s[1]]*(poi.s[0])) #t
            return l
        def gn(x,y):
            #x = n % self.size
            #y = math.floor(n/self.size)
            if x < 0 or x >= self.size or y < 0 or y >= self.size:
                return -1
            else:
                return x+y*self.size
        def n(n):
            x = n % self.size
            y = int(math.floor(n/self.size))
            w = [gn(x-1,y),gn(x+1,y),gn(x,y-1),gn(x,y+1)]
            return w 
        for poi in self.pois:
            closed = set([])
            fringe = set([])
            i = 0
            for t in qex1(poi):
                if self.tiles[qbi(t[0],t[1])].style  == MapStyle.MAP_EMPTY_STREET: self.tiles[qbi(t[0],t[1])].value = poi.value
                closed.add(qbi(t[0],t[1]))
                for p in n(qbi(t[0],t[1])):
                    if p not in closed and self.tiles[p].style == MapStyle.MAP_EMPTY_STREET:
                        fringe.add(p)
            fringe = list(fringe-closed)
            while fringe:
                i += 1
                c = fringe.pop(0)
                self.tiles[c].value = sum([self.tiles[i].value for i in n(c)])/4.0
                closed.add(c)
                for p in n(c):
                    if p not in closed and self.tiles[p].style == MapStyle.MAP_EMPTY_STREET:
                        fringe.append(p)
        urbanizable = []
        for i in xrange(self.size**2):
            if self.tiles[i].style==MapStyle.MAP_EMPTY_STREET:
                for j in n(i):
                    if self.tiles[j].style in [MapStyle.MAP_BLOCK_COMERCIAL,MapStyle.MAP_BLOCK_INDUSTRIAL,MapStyle.MAP_BLOCK_RESIDENTIAL]:
                        self.tiles[j].value += self.tiles[i].value
            elif self.tiles[i].style in [MapStyle.MAP_BLOCK_COMERCIAL,MapStyle.MAP_BLOCK_INDUSTRIAL,MapStyle.MAP_BLOCK_RESIDENTIAL]:
                urbanizable.append(i)
        urbanizable.sort(key=lambda i:self.tiles[i].value,reverse=True)
        for i in urbanizable:
            self.tiles[i].value = 0
        for i in xrange(int(size**2*population*2)):
            s = int((1-cluster if rand()<0.5 else 1.0)*rand()*len(urbanizable))
            self.tiles[urbanizable[s]].value += 1
                
                    
        
    def plan_suburban_simple(self,seed,size,population,public,roads):
        suburban_size = int((1-population)*size*0.75)
        street_length = max(4,int((1-roads)*16))
        #Roads
        for y in xrange(0,size):
            for x in xrange(0,suburban_size+int(rand()*street_length)):
                if y%2==0:
                    self.block_xy(x, y, MapStyle.MAP_EMPTY_STREET, 1, True)
                else:
                    self.block_xy(x, y, MapStyle.MAP_BLOCK_RESIDENTIAL, 1, True)
        for x in xrange(0,size,2):
            if x % street_length == 0 and x < suburban_size:
                self.line_h(x, MapStyle.MAP_EMPTY_STREET, 1, False)  
    def plan_industrial_simple(self,seed,size,population,public,roads):
        ind_size_x = int((population+0.25)*size*0.5)
        ind_size_y = int((1-population+0.5)*size*0.75)
        street_length = max(4,int((1-roads)*8))
        for y in xrange(size-ind_size_y,size):
            ind_size_x2 = int(size - ind_size_x * (0.25+rand()*0.25))
            ind_size_x2 -= ind_size_x2 % 4
            do_road = True if rand()<roads else False
            for x in xrange(size-ind_size_x,size):
                if x%street_length==0 or (y%12==0) or (y%4==0 and do_road and x < ind_size_x2) :
                    self.block_xy(x, y, MapStyle.MAP_EMPTY_STREET, 1, True)
                else:
                    self.block_xy(x, y, MapStyle.MAP_BLOCK_INDUSTRIAL, 1, True)      
    def plan_urban_simple(self,seed,size,population,public,roads):
        for i in xrange(size**2):
            self.tiles[i].style=MapStyle.MAP_BLOCK_COMERCIAL
        #Roads
        for i in xrange(0,size,4):
            self.line_w(i, MapStyle.MAP_EMPTY_STREET, 1, False)
            self.line_h(i, MapStyle.MAP_EMPTY_STREET, 1, False)
        clear = False
        #Sides W
        for x in xrange(1,size,2):
            for y in xrange(2,size,4):
                if clear:
                    self.block_xy(x, y, -1, 1, False)
                    if rand()<roads:
                        clear = False
                else:
                    if rand()>roads:
                        clear = True
        #Sides H
        for x in xrange(1,size,2):
            for y in xrange(2,size,4):
                if clear:
                    self.block_xy(y, x, -1, 1, False)
                if rand()<roads:
                        clear = False
                else:
                    if rand()>roads:
                        clear = True
        def qbi(x,y):
            x = min(max(0,x),self.size-1)
            y = min(max(0,y),self.size-1)
            return x+y*self.size
        def qbn(x,y):
            return [qbi(x+1,y),qbi(x-1,y),qbi(x,y+1),qbi(x,y-1)]
        def qbn2(x,y):
            return [qbi(x+1,y),qbi(x-1,y),qbi(x,y+1),qbi(x,y-1),qbi(x+1,y+1),qbi(x-1,y+1),qbi(x+1,y-1),qbi(x-1,y-1)]
        def qbn3(x,y):
            return [qbi(x+1,y+1),qbi(x-1,y+1),qbi(x+1,y-1),qbi(x-1,y-1)]
        #Centers
        for x in xrange(2,size,4):
            for y in xrange(2,size,4):
                if sum([1 if self.tiles[i].style==-1 else 0 for i in qbn(x,y)])>0:
                    self.block_xy(x, y, MapStyle.MAP_EMPTY_STREET, 1, True)
                for i in qbn(x,y):
                    if self.tiles[i].style==-1:
                        self.tiles[i].style=MapStyle.MAP_EMPTY_STREET
        #Corners
        for x in xrange(1,size,2):
            for y in xrange(1,size,2):
                if sum([0 if self.tiles[i].style==MapStyle.MAP_EMPTY_STREET else 1 for i in qbn2(x,y)])==0:
                    self.block_xy(x, y, MapStyle.MAP_EMPTY_STREET, 1, True)
    def plan_public(self,seed,size,population,public,roads):
        class Poi():
            def __init__(self,value,c,s):
                self.value = value
                self.c = c
                self.s = s
        pois = []
        publicSize = public*population*size/4 #residential+comercial+industrial+public
        public_ = public
        while public_>0:
            value = min(public_,rand())
            public_ -= value   
            sx = 7 if rand()>0.4 else 3
            sy = 7 if rand()>0.6 and sx == 3 else 3
            x = (rand()*0.4+0.2)*self.size
            y = (rand()*0.4+0.2)*self.size
            x = int(x)
            y = int(x)
            x -= x % 4 -1
            y -= y % 4 -1
            pois.append(Poi(value,[x,y],[sx,sy]))
        if not pois:
            pois = [Poi(public,1)]
        pois.sort(key=lambda i:i.value)
        for poi in pois:
            self.block_xywh(poi.c[0],poi.c[1],poi.s[0],poi.s[1],MapStyle.MAP_BLOCK_PUBLIC,poi,True)               
        self.pois = pois
               
    def plan_public2(self,seed,size,population,public,roads):
        class Poi():
            def __init__(self,value,size):
                self.value = value
                self.size = size
                self.c = None
        class Block():
            def __init__(self,x1,y1,x2,y2):
                self.x1=x1
                self.x2=x2
                self.y1=y1
                self.y2=y2
        pois = []
        publicSize = public*population*size/4 #residential+comercial+industrial+public
        public_ = public
        blocks = [Block(0,0,size,size)]
        while public_>0:
            value = min(public_,rand())
            public_ -= value            
            pois.append(Poi(value,max(1,int(publicSize/public*value))))
        if not pois:
            pois = [Poi(public,1)]
        pois.sort(key=lambda i:i.value)
        for poi in pois:
            x = int((rand()*0.5+0.25)*self.size)
            y = int((rand()*0.5+0.25)*self.size)
            poi.c = [x,y]
            self.block_xyc(x,y,poi.size,MapStyle.MAP_BLOCK_PUBLIC,poi,True)
        for poi in pois:
            if rand()*roads<0.25:
                self.line_w(poi.c[1], MapStyle.MAP_EMPTY_STREET, 1, False)
                self.line_w(poi.c[1]-1, MapStyle.MAP_EMPTY_STREET, 1, False)
            else:
                self.line_w(poi.c[1]+(poi.size+1)/2, MapStyle.MAP_EMPTY_STREET, 1, False)
                self.line_w(poi.c[1]-(poi.size+1)/2-1, MapStyle.MAP_EMPTY_STREET, 1, False)
                self.line_w(poi.c[1]+(poi.size+1)/2+1, MapStyle.MAP_EMPTY_STREET, 1, False)
                self.line_w(poi.c[1]-(poi.size+1)/2-2, MapStyle.MAP_EMPTY_STREET, 1, False)
            if rand()*roads<0.25:
                self.line_h(poi.c[0], MapStyle.MAP_EMPTY_STREET, 1, False)
                self.line_h(poi.c[0]-1, MapStyle.MAP_EMPTY_STREET, 1, False)
            else:
                self.line_h(poi.c[0]+(poi.size+1)/2, MapStyle.MAP_EMPTY_STREET, 1, False)
                self.line_h(poi.c[0]-(poi.size+1)/2-1, MapStyle.MAP_EMPTY_STREET, 1, False)
                self.line_h(poi.c[0]+(poi.size+1)/2+1, MapStyle.MAP_EMPTY_STREET, 1, False)
                self.line_h(poi.c[0]-(poi.size+1)/2-2, MapStyle.MAP_EMPTY_STREET, 1, False)

                
                
        self.pois = pois
    def plan_roads(self,seed,size,population,roads):
        return 0
        i = 0
        roads = []
        while(i<self.size):
            i += 4 + int(4 * rand())
            if rand()<roads:
                self.line_x(i,MapStyle.MAP_EMPTY_STREET,i,False)
                self.line_x(i+1,MapStyle.MAP_EMPTY_STREET,i,False)
                i+=2
            i += 4
            if rand()<roads:
                self.line_x(i,MapStyle.MAP_EMPTY_STREET,i,False)
                i+=1
class Vis:
    def __init__(self,root,width,height):
        self.enable = False
        self.frame = Frame(root)
        self.width = width
        self.height = height
        self.canvas = Canvas(self.frame,bg="black",width=width,height=height)
        self.canvas.pack()
        
        self.size = 0
        self.seed = 0
        self.population = 0
        self.roads = 0
        self.public = 0
        self.cluster = 0
        self.residential = 0
        self.comercial = 0
        self.industrial = 0
        Button(self.frame, text="Save files", command=self.save).pack()
        Button(self.frame, text="Generate a bunch of files (like 10 or so)", command=self.save_lots).pack()
        #Label(self.frame, text="Seed").pack()
        s = Scale(self.frame, from_=0, to=100, resolution=1, orient=HORIZONTAL, length=width, command=self.reseed )
        s.set(55)
        s.pack()
        #Label(self.frame, text="Size").pack()
        s = Scale(self.frame, from_=0, to=100, resolution=1, orient=HORIZONTAL, length=width, command=self.resize )
        s.set(37)
        #s.set(8)
        s.pack()
        Label(self.frame, text="Population").pack()
        s = Scale(self.frame, from_=0, to=1.0, resolution=0.001, orient=HORIZONTAL, length=width, command=self.repopulation )
        s.set(0.6)
        s.pack()
        Label(self.frame, text="Road network").pack()
        s = Scale(self.frame, from_=0, to=1.0, resolution=0.001, orient=HORIZONTAL, length=width, command=self.reroads )
        s.set(0.5)
        s.pack()
        Label(self.frame, text="Public development").pack()
        s = Scale(self.frame, from_=0, to=1.0, resolution=0.001, orient=HORIZONTAL, length=width, command=self.republic )
        s.set(0.5)
        s.pack()
        Label(self.frame, text="Cluster bias").pack()
        s = Scale(self.frame, from_=0, to=1.0, resolution=0.001, orient=HORIZONTAL, length=width, command=self.recluster )
        s.set(0.5)
        s.pack()
        #Label(self.frame, text="Residential bias").pack()
        #s = Scale(self.frame, from_=0, to=1.0, resolution=0.001, orient=HORIZONTAL, length=width, command=self.reresidential )
        #s.set(0.5)
        s.pack()
        Label(self.frame, text="Comercial bias").pack()
        s = Scale(self.frame, from_=0, to=1.0, resolution=0.001, orient=HORIZONTAL, length=width, command=self.recomercial )
        s.set(0.5)
        s.pack()
        Label(self.frame, text="Industrial bias").pack()
        s = Scale(self.frame, from_=0, to=1.0, resolution=0.001, orient=HORIZONTAL, length=width, command=self.reindustrial )
        s.set(0.5)
        s.pack()
        self.frame.pack()
        self.frame.update()
        self.enable = True
    def save(self,lots_counter=0):
        #Nav
        self.map.draw_obj(self.seed,self.size,self.population,self.public,self.roads,self.cluster,True,lots_counter)
        #Normal
        self.map.draw_obj(self.seed,self.size,self.population,self.public,self.roads,self.cluster,False,lots_counter)
    def save_lots(self):
        for i in xrange(10):
            if i == 1:
                pass
            size = self.size #int(self.size/2+rand()*self.size)
            self.map = Map(size)
            self.map.plan(i,size,rand()*0.8+0.1,rand()*0.8+0.1,rand()*0.8+0.1,rand()*0.8+0.1)
            self.save(i)
    def resize(self,i):
        self.size = int(i)
        self.draw()
    def reseed(self,i):
        self.seed = int(i)
        self.draw()
    def repopulation(self,i):
        self.population = float(i)
        self.draw()
    def republic(self,i):
        self.public = float(i)
        self.draw()
    def reroads(self,i):
        self.roads = float(i)
        self.draw()
    def recluster(self,i):
        self.cluster = float(i)
        self.draw()
    def reresidential(self,i):
        self.residential = float(i)
        self.draw()
    def recomercial(self,i):
        self.comercial = float(i)
        self.draw()
    def reindustrial(self,i):
        self.industrial = float(i)
        self.draw()
    def rearrange(self):
        print self.size
    def rearrange2(self,i):
        print i
    def draw(self):
        draw_grid = False
        draw_tile_value = True
        if self.enable:
            self.map = Map(self.size)
            self.map.plan(6,self.size,self.population,self.public,self.roads,self.cluster)
            self.canvas.delete(ALL)
            #Tiles
            w = self.width / self.size
            for y in xrange(self.size):
                for x in xrange(self.size):
                    tile = self.map.tiles[x+y*self.size]
                    self.draw_tile(x * self.width / self.size, y * self.width / self.size, w, styleColors[tile.style])
                    if draw_tile_value:
                        self.draw_label([x * self.width / self.size+2, y * self.width / self.size+2],w,tile.value)
            #Grid
            if draw_grid:
                for i in xrange(self.size+1):
                    self.draw_line([0,i*self.width / self.size], [self.width,i*self.width / self.size])
                    self.draw_line([i*self.width / self.size,0,], [i*self.width / self.size,self.width])
    def draw_tile(self,x,y,w,color):  
        self.canvas.create_rectangle(x,y,x+w,y+w,fill=color) 
    def draw_line(self,p1,p2):
        self.canvas.create_line(p1,p2,fill='white')
    def draw_point(self,p):
        self.canvas.create_oval(p[0]-2,p[1]-2,p[0]+3,p[1]+3,fill='white',state=DISABLED)
    def draw_label(self,p,w,s):
        if isinstance(s,float):
            s = "-"#s = "%.2f"%s
        if isinstance(s,int):
            s = "%d"%s
        self.canvas.create_text(p[0]+w/2,p[1]+w/2,text = s,font=("Sans", 8),fill = "white")
        


# show it
root = Tk()
vis = Vis(root,400,400)
vis.draw()
vis.save()
root.mainloop()

'''
        def qb(x,y):
            x = min(max(0,x),self.size)
            y = min(max(0,y),self.size)
            return self.tiles[x+y*self.size].style
        def qbi(x,y):
            x = min(max(0,x),self.size)
            y = min(max(0,y),self.size)
            return x+y*self.size
        def qbw(x,y):
            return qb(x-1,y)+qb(x+1,y)
        def qbh(x,y):
            return qb(x,y-1)+qb(x,y+1)
        def sbw(x,y,style):
            self.block_xy(x+1, y, style, 1, True)
            self.block_xy(x-1, y, style, 1, True)
        def sbh(x,y,style):
            self.block_xy(x, y+1, style, 1, True)
            self.block_xy(x, y-1, style, 1, True)
        def qbn(x,y):
            return [qbi(x+1,y),qbi(x-1,y),qbi(x,y+1),qbi(x,y-1)]
        def qbn2(x,y):
            return [qbi(x+1,y),qbi(x-1,y),qbi(x,y+1),qbi(x,y-1),qbi(x+1,y+1),qbi(x-1,y+1),qbi(x+1,y-1),qbi(x-1,y-1)]
        #Centers
        for x in xrange(2,size,4):
            for y in xrange(2,size,4):
                if qbw(x,y)==qbh(x,y):
                    if qb(x-1,y)==MapStyle.MAP_BLOCK_COMERCIAL and rand()<roads:
                        for i in qbn2(x,y):
                            self.tiles[i].style=MapStyle.MAP_EMPTY_STREET
                    else:
                        for i in qbn2(x,y):
                            self.tiles[i].style=MapStyle.MAP_EMPTY_UNDEVELOPED
                elif qbw(x,y)==2 and qbh(x,y)==1:
                    sbh(x,y,MapStyle.MAP_EMPTY_UNDEVELOPED)
                    self.block_xy(x, y, MapStyle.MAP_EMPTY_STREET, 1, False)
                elif qbw(x,y)==1 and qbh(x,y)==2:
                    sbw(x,y,MapStyle.MAP_EMPTY_UNDEVELOPED)
                    self.block_xy(x, y, MapStyle.MAP_EMPTY_STREET, 1, False)
                for i in qbn(x,y):
                    if self.tiles[i].style==MapStyle.MAP_BLOCK_COMERCIAL:
                        self.tiles[i].style=MapStyle.MAP_EMPTY_STREET'''