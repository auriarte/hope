#include "Map.h"
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <fstream>
//#include "scene.h"
#include "model_obj.h"

using namespace std;

void Map::appendChildToParentNode(aiNode *pParent, aiNode *pChild)
{
    
	// Assign parent to child
	pChild->mParent = pParent;
	size_t sNumChildren = 0;
	(void)sNumChildren; // remove warning on release build
	
	// If already children was assigned to the parent node, store them in a
	std::vector<aiNode*> temp;
	if (pParent->mChildren != NULL)
	{
		sNumChildren = pParent->mNumChildren;
		for (size_t index = 0; index < pParent->mNumChildren; index++)
		{
			temp.push_back(pParent->mChildren [ index ] );
		}
		delete [] pParent->mChildren;
	}
	
	// Copy node instances into parent node
	pParent->mNumChildren++;
	pParent->mChildren = new aiNode*[ pParent->mNumChildren ];
	for (size_t index = 0; index < pParent->mNumChildren-1; index++)
	{
		pParent->mChildren[ index ] = temp [ index ];
	}
	pParent->mChildren[ pParent->mNumChildren-1 ] = pChild;
}

Map::Map(){

    if (!g_model.import("meshes/map_buildings.obj"))
    {
        std::cout<< "error";
        //throw std::runtime_error("Failed to load model.");
    } else{
//        std::cout<< "loaded";
    }
    g_model.normalize();
  
    aiVector3D * v3 = new aiVector3D();
//    cout << g_model.getVertexSize() << endl;
    const ModelOBJ::Vertex * v = new ModelOBJ::Vertex();

    vertexarray = (float*) malloc (3*3*g_model.getNumberOfVertices()*sizeof(float));
    float mMasterScale = 0.05f;
    aiMatrix4x4t<float> ms = aiMatrix4x4(
                                        mMasterScale,0.0f, 0.0f, 0.0f,
                                        0.0f, mMasterScale,0.0f, 0.0f,
                                        0.0f, 0.0f, mMasterScale,0.0f,
                                        0.0f, 0.0f, 0.0f, 1.0f);
/*    aiMatrix4x4t<float> mt0 = aiMatrix4x4(
                                        1.0f,0.0f, 0.0f, 1.25f,
                                        0.0f, 1.0f,0.0f, 0.0f,
                                        0.0f, 0.0f, 1.0f,0.0f,
                                        0.0f, 0.0f, 0.0f, 1.0f);
 */
    aiMatrix4x4t<float> mt = aiMatrix4x4(
                                         1.0f,0.0f, 0.0f, 1.0f,
                                         0.0f, 1.0f,0.0f, 0.0f,
                                         0.0f, 0.0f, 1.0f,0.0f,
                                         0.0f, 0.0f, 0.0f, 1.0f);
    
    int p = 0;
    for(int j=0;j<3;j++){
        for(int i=0;i<g_model.getNumberOfVertices();i++){
            v = g_model.getVertexBuffer()+i;
            v3->x =v->position[0];
            v3->y =v->position[1];
            v3->z =v->position[2];
            (*v3) *= ms;
            vertexarray[p++]=v3->x;
            vertexarray[p++]=v3->y;
            vertexarray[p++]=v3->z;
        }
        ms *= mt;
    }
    
    
    
    //g_model.getCenter(g_targetPos[0], g_targetPos[1], g_targetPos[2]);
    g_targetPos[0]=0.0f;
    g_targetPos[1]=0.0f;
    g_targetPos[2]=0.0f;
    
    g_cameraPos[0] = g_targetPos[0];
    g_cameraPos[1] = g_targetPos[1] + 0.1f;
    g_cameraPos[2] = g_targetPos[2] +0.01f;
    
    g_pitch = 0.0f;
    g_heading = 0.0f;
    /*
     scene = new aiScene();
     scene->mRootNode = new aiNode("root");
     scene->mMeshes = new ModelOBJ*[1];
     scene->mMeshes[0] = &g_model;
     
    aiNode *pNode1 = new aiNode("cube1");
    aiNode *pNode2 = new aiNode("cube2");
    aiNode *pNode3 = new aiNode("cube3");
    
    appendChildToParentNode(scene->mRootNode, pNode1);
    appendChildToParentNode(scene->mRootNode, pNode2);
    appendChildToParentNode(scene->mRootNode, pNode3); 
    
    
    
    pNode1->mMeshes = new unsigned int[1];
    pNode1->mMeshes[0] = 0;
    pNode2->mMeshes = new unsigned int[1];
    pNode2->mMeshes[0] = 0;
    pNode3->mMeshes = new unsigned int[1];
    pNode3->mMeshes[0] = 0;
    
	scene->mRootNode->mTransformation *= aiMatrix4x4(
                                                      mMasterScale,0.0f, 0.0f, 0.0f,
                                                      0.0f, mMasterScale,0.0f, 0.0f,
                                                      0.0f, 0.0f, mMasterScale,0.0f,
                                                      0.0f, 0.0f, 0.0f, 1.0f);
    pNode1->mTransformation *= aiMatrix4x4(
                                           mMasterScale,0.0f, 0.0f, 0.0f,
                                           0.0f, mMasterScale,0.0f, 0.0f,
                                           0.0f, 0.0f, mMasterScale,0.0f,
                                           0.0f, 0.0f, 0.0f, 1.0f);
    */

};
Map::~Map(){
    
};

bool Map::ai_assert(bool assert){
    return assert;
}

void Map::parseTilesFromFile(){
    SDL_Surface* data = NULL;
    //system("PWD");
    data = IMG_Load( "images/map_demo.tga" );
    w = data->w;
    h = data->h;
    
    uint8_t r,g,b;
    MapTile* tile;
    
    this->tiles = new MapTile *[w*h];
    
    char* index = (char*) data->pixels;
    for(int i=0;i<w*h;i++){
        tile = new MapTile();
        r=(*index);
        g=(*(index+1));
        b=(*(index+2));
        if(r||g||b){
            tile->type=MAP_BLOCK;
            if(r)
                tile->style=MAP_BLOCK_COMERCIAL;
            else if (g)
                tile->style=MAP_BLOCK_INDUSTRIAL;
            else
                tile->style=MAP_BLOCK_RESIDENTIAL;
        } else {tile->type=MAP_EMPTY;}
        this->tiles[i]=tile;
        index += data->format->BytesPerPixel;
    }
    SDL_FreeSurface(data);
    this->debugTiles();
}

void Map::debugTiles(){
    int i;
    //printf("Map size (w x h): %i x %i",w,h);
    for(int y=0;y<h;y++){
        for(int x=0;x<w;x++){
            i = y*w+x;
            //printf("%c", (tiles[i]->type==MAP_BLOCK ? 'X' : ' '));
        }
        //printf("\n");
    }
    
}