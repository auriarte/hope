
#include <iostream>

#include "Global.h"

#include "Timer.h"
#include "KeyboardMouse.h"
#include "GameEngine.h"
#include "CollisionWorld.h"

//#include "SearchFiles.h"

int main( int argc, char *argv[] ) {
    
    bool quit = false;
    
    GameEngine * game = new GameEngine();
    KeyboardMouse * input = new KeyboardMouse();
    Timer fps; // the frame rate regulator
    SDL_Event events; // SDL event handler
    std::list<action_t*> actions;
    std::list<collision_t*> collisions;
//    std::cout << "Game engine start" << std::endl;
    while( !quit ) {
        fps.Start();
        actions = input->HandleEvents( events, quit );
        game->HandleInput(actions);
        game->Update();
        collisions = game->collisionWorld->GetCollisions();
        game->HandleCollisions(collisions);
        game->Render();
        
        int ms = (int)(1000.0f / FRAMES_PER_SECOND);
        if( fps.GetTicks() < ms ) {
            // cap the frame rate
            SDL_Delay( ms - fps.GetTicks() );
        }
    }
    
    delete game;
    
    return 0;
}
