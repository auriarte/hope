#include "Timer.h"

Timer::Timer() {
    // initialize the variables
    _startTicks = 0;
    _pausedTicks = 0;
    _paused = false;
    _started = false;
}

void Timer::Start() {
    _started = true;
    _paused = false;
    _startTicks = SDL_GetTicks();
}

void Timer::Stop() {
    _started = false;
    _paused = false;
}

void Timer::Pause() {
    if( ( _started == true ) && ( _paused == false ) ) {
        _paused = true;
        
        //Calculate the paused ticks
        _pausedTicks = SDL_GetTicks() - _startTicks;
    }
}

void Timer::Unpause() {
    if( _paused == true ) {
        _paused = false;
        
        //Reset the starting ticks
        _startTicks = SDL_GetTicks() - _pausedTicks;
        
        //Reset the paused ticks
        _pausedTicks = 0;
    }
}

int Timer::GetTicks() {
    if( _started == true ) {
        if( _paused == true ) {
            //Return the number of ticks when the timer was paused
            return _pausedTicks;
        } else {
            //Return the current time minus the start time
            return SDL_GetTicks() - _startTicks;
        }
    }
    
    //If the timer isn't running
    return 0;
}

bool Timer::IsStarted() {
    return _started;
}

bool Timer::IsPaused() {
    return _paused;
}