#include <stdint.h>
#include <climits>
#include <time.h>

class DeterministicRandomNumberGenerator {
public:
    DeterministicRandomNumberGenerator();
    ~DeterministicRandomNumberGenerator();
    void setSeed(uint32_t seed);
    void setSeedFromSystemTime();
    void randomize();
    void reset();
    uint32_t nextInt();
    double nextDouble();

private:
    uint32_t m_w;
    uint32_t m_z;
    uint32_t m_w0;
    uint32_t m_z0;
};