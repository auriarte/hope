//
//  Vecrtor3d.h
//  hope
//
//  Created by Ehsan Khosroshahi on 2/13/13.
//
//

#ifndef __hope__Vecrtor3d__
#define __hope__Vecrtor3d__

#include <iostream>
#include <cmath>


class vector3d{
public:
    float x,y,z;
    vector3d();
    //vector3d(float a, float b);
    vector3d(float a, float b, float c);
    
    float dotProduct(const vector3d& vec2);
    vector3d crossProduct(const vector3d& vec2);
    float length();
    void normalize();
    float magnitude() const;
    float magnitudeSquared() const;
    
    void change(float a, float b,float c);
    void change(vector3d& vec2);
    void change(vector3d vec2);
    void changeX(float a);
    void changeY(float a);
    void changeZ(float a);

vector3d operator=(const vector3d vec2);
vector3d operator+(const vector3d vec2);
vector3d operator-(const vector3d vec2);
vector3d operator*(float vec2);
vector3d operator/(float vec2);

vector3d& operator+=(const vector3d vec2);
vector3d& operator-=(const vector3d vec2);
vector3d& operator*=(float vec2);
vector3d& operator/=(float vec2);

bool operator==(const vector3d vec2);
bool operator!=(const vector3d vec2);

friend std::ostream& operator<<(std::ostream& out, const vector3d& vec);
             
    
};


#endif /* defined(__hope__Vecrtor3d__) */
