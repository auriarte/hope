#ifndef __hope__Actor__
#define __hope__Actor__

#include <iostream>
#include "vector2.h"
#include "CollisionBody.h"

enum actorType_t {
    ACTOR_PLAYER,
    ACTOR_ENEMY
};

class Actor{
public:
    Actor();
    Actor(actorType_t type);
    ~Actor();
    actorType_t type;
    aiVector2D pos;
    aiVector2D dir_vec;
    float dir_rot;
    CollisionBody * collisionBody;
};

#endif /* defined(__hope__Actor__) */
