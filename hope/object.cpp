#include "object.h"

Object::Object()
{
	translation.set(0.0,0.0,0.0);
	scaling.set(1.0,1.0,1.0);
	rotation.set(0.0,0.0,0.0);

	glListId = glGenLists(1);
	model = NULL;	
}

Object::Object(Model *_model, int _type)
{
	translation.set(0.0,0.0,0.0);
	scaling.set(1.0,1.0,1.0);
	rotation.set(0.0,0.0,0.0);

	glListId = glGenLists(1);
	model = _model;	

	type = _type;
}

Object::Object(Model *_model, int _type, myVector &_translation, myVector &_rotation, myVector &_scaling)
{
	translation.set(_translation);
	scaling.set(_scaling);
	rotation.set(_rotation);

	glListId = glGenLists(1);
	model = _model;	

	type = _type;
}

void Object::transformObject()
{
	//glLoadIdentity();

	glTranslatef(translation.x,translation.y,translation.z);
	glRotatef(rotation.x, 1.0f, 0.0f, 0.0f);
	glRotatef(rotation.y, 0.0f, 1.0f, 0.0f);
	glRotatef(rotation.z, 0.0f, 0.0f, 1.0f);
	glScalef(scaling.x,scaling.y,scaling.z);
}

void Object::reset()
{
	translation.set(0.0,0.0,-20.0);
	scaling.set(1.0,1.0,1.0);
	rotation.set(0.0,0.0,0.0);
}