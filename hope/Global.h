#ifndef __GLOBAL_H_
#define __GLOBAL_H_

#include <list>

#include "SDL.h"
#include "SDL_opengl.h"
#include "SDL_image.h"

#include "Singelton.h"

#include "GameState.h"

//Screen attributes
const int SCREEN_WIDTH = 1024;
const int SCREEN_HEIGHT = 780;
const int SCREEN_BPP = 32;

//The frame rate
const int FRAMES_PER_SECOND = 60;

// TODO we should change the working directory...
const char FONT_PATH[] = "../../../../Resources/DroidSans.ttf";

typedef Singleton<GameState> GameStateS;   // Global declaration

//Actions
enum actionType_t {
    NONE,
    PRIMARY,
    SECONDARY,
    LEFT,
    RIGHT,
    UP,
    DOWN,
    AIM,
    TOGGLE,
    TOGGLE_GUI,
    SPECIAL_ESCAPE,
    SPECIAL_TOGGLE_FULLSCREEN,
    WHEEL_UP,
    WHEEL_DOWN,
    TRANSLATION
};
class action_t {
public:
    actionType_t action;
    int x;
    int y;
    action_t(){ x=0; y=0; action=NONE; };
    action_t( actionType_t action ){ x=0; y=0; this->action=action; };
    ~action_t(){};
};

#endif