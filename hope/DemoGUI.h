#ifndef hope_DemoGUI_h
#define hope_DemoGUI_h

#include "GameFlow.h"

class DemoGUI : public GameFlow {
private:
    DemoGUI();
    DemoGUI(const DemoGUI&);
    DemoGUI& operator=(const DemoGUI&);
    
    void draw1();
    void draw2();
    
    float cameraDistance;
    float cameraYaw;
    float cameraPitch;
public:
    static DemoGUI *    Instance();
    virtual void        HandleInput( std::list<action_t*> actions );
    virtual void        HandleCollisions( std::list<collision_t*> collisions );
    virtual void        Update();
    virtual void        Render();
    virtual void        RenderGUI( GUI* gui );    
};

#endif
