#ifndef __hope__GameFlow__
#define __hope__GameFlow__

#include "Global.h"
#include "GUI.h"

class GameFlow {
public:   
    virtual         ~GameFlow(){}
    
    virtual void    HandleInput( std::list<action_t*> actions )=0;
    virtual void    HandleCollisions( std::list<collision_t*> collisions )=0;
    virtual void    Update()=0;
    virtual void    Render()=0;
    virtual void    RenderGUI( GUI* gui )=0;    

};

#endif /* defined(__hope__GameFlow__) */
