#ifndef hope_DemoNavigation_h
#define hope_DemoNavigation_h

#include "GameFlow.h"
#include "SearchFiles.h"
//#include "model_obj.h"
#include "ObjDraw.h"
#include "Navigation.h"

class DemoNavigation : public GameFlow {
private:
                    DemoNavigation();
                    DemoNavigation(const DemoNavigation&);
                    DemoNavigation& operator=(const DemoNavigation&);
    
    Sint16          _mainScroll;
    Sint16          _meshScroll;
    bool            _showMeshes;
    fileNameList_t  _files;
    //ModelOBJ        _map;
    ObjDraw        _map;    
    
    // camera position
    float cameraYaw;
    float cameraPitch;
    float _cameraPos[3];
    float _targetPos[3];
    
    float _scaleFactor;
    
    // movement
    float _moveSpeed;
    float _zoomSpeed;
    
    // navigation mesh
    float _cellSize;
    void buildNavMesh();
    bool _drawMesh;
    bool _drawGrid;
    bool _drawVoxel;
    bool _drawDistanceTransform;
    bool _drawRegions;
    bool _drawContours;
    bool _drawNavMesh;
    
    // pathfinding
    bool _startPosSet;
    float _startPos[3];
    bool _endPosSet;
    float _endPos[3];
    

    unsigned char* _triareas;
    heightfield_t* _solid;
	compactHeightfield_t* _compactHeightfield;
	contourSet_t* _contourSet;
	polyMesh_t* _polyMesh;
	polyMeshDetail_t* _polyMeshDetail;
    
    // TODO this method must be generic for render any object!!!!
    void DrawObj();
    void DrawGroundGrid() const;
    void DrawBoxWire(float minx, float miny, float minz,
                     float maxx, float maxy, float maxz) const;
    void DrawWalkableVoxels();
    void DistanceTransform(const compactHeightfield_t& chf);
    void DrawRegions();
    void DrawContours(const contourSet_t& cset, const float alpha = 1.0f);
    void DrawPolyMesh(const struct polyMesh_t& mesh);
    
    void DrawAgent(const float* pos, float r, float h, float c, const unsigned int col);

    
public:
    static DemoNavigation * Instance();
    virtual void            HandleInput( std::list<action_t*> actions );
    virtual void            HandleCollisions( std::list<collision_t*> collisions );
    virtual void            Update();
    virtual void            Render();
    virtual void            RenderGUI( GUI* gui );    
};

#endif
