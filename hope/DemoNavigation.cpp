
#include "DemoNavigation.h"

#define _USE_MATH_DEFINES

DemoNavigation* DemoNavigation::Instance()
{
    static DemoNavigation instance;
    return &instance;
}

DemoNavigation::DemoNavigation() {
    _mainScroll = 0;
    _meshScroll = 0;
    _showMeshes = false;
    // camera position
    _cameraPos[0] = 0.0f;
    _cameraPos[1] = 0.0f;
    _cameraPos[2] = 10.0f;
    cameraPitch = 45.0f;
    cameraYaw = -45.0f;
    
    _moveSpeed = 1.2f;
    _zoomSpeed = 12.00f;
    
    //gui vars
    _drawMesh = true;
    _drawGrid = false;
    _drawVoxel = false;
    _drawDistanceTransform = false;
    _drawRegions = false;
    _drawContours = false;
    _drawNavMesh = false;
    
    // rasterization
    _cellSize = 1.0f;
    
    // pathfinding
    _startPosSet = false;
    _endPosSet = false;
}

void DemoNavigation::HandleInput( std::list<action_t*> actions ) {
    GameStateS::Instance()->speed.x = 0;
    GameStateS::Instance()->speed.y = 0;
    while ( !actions.empty() ) {
        action_t * action = actions.front();
        actions.pop_front();
        switch( action->action ){
            case TOGGLE_GUI:
                GameStateS::Instance()->showGUI = !GameStateS::Instance()->showGUI;
                break;
            case SPECIAL_ESCAPE:
                GameStateS::Instance()->state = GS_FSM_RENDER;
                break;
            case WHEEL_UP:
                if (!GameStateS::Instance()->ui.mouseOverMenu) _cameraPos[2] -= _zoomSpeed;
                break;
            case WHEEL_DOWN:
                if (!GameStateS::Instance()->ui.mouseOverMenu) _cameraPos[2] += _zoomSpeed;
                break;
            case LEFT:
                GameStateS::Instance()->speed.x = -4;
                break;
            case RIGHT:
                GameStateS::Instance()->speed.x = +4;
                break;
            case UP:
                GameStateS::Instance()->speed.y = -4;
                break;
            case DOWN:
                GameStateS::Instance()->speed.y = +4;
                break;
            case AIM:
                cameraYaw += action->x;
                cameraPitch += action->y;
                if (cameraPitch < -90) cameraPitch = -90;
                if (cameraPitch > 90) cameraPitch = 90;
                break;
            case TRANSLATION:
                GameStateS::Instance()->speed.x = action->x;
                GameStateS::Instance()->speed.y = action->y;
                break;
            default:
                break;
        }
        delete action;
    }
}

void DemoNavigation::HandleCollisions( std::list<collision_t*> collisions ) {
    
}

void DemoNavigation::Update() {   
    _cameraPos[0] += GameStateS::Instance()->speed.x * _moveSpeed;
    _cameraPos[1] -= GameStateS::Instance()->speed.y * _moveSpeed;
    
    _targetPos[0] += GameStateS::Instance()->speed.x * _moveSpeed;
    _targetPos[1] -= GameStateS::Instance()->speed.y * _moveSpeed;
}

void DemoNavigation::Render() {
    
    // Update and render
    glViewport(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
    glClearColor(0.3f, 0.3f, 0.32f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glDisable(GL_TEXTURE_2D);
    
    // set perspective viewing frustum
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(60.0f, (float)SCREEN_WIDTH/(float)SCREEN_HEIGHT, 0.1f, 10000.0f); // FOV, AspectRatio, NearClip, FarClip
    
    // switch to modelview matrix in order to set scene
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    
    // transform camera
    gluLookAt(_cameraPos[0], _cameraPos[1], _cameraPos[2],
              _targetPos[0], _targetPos[1], _targetPos[2],
              0.0f, 1.0f, 0.0f);
    
    glRotatef(cameraPitch,  1.0f, 0.0f, 0.0f);
    glRotatef(cameraYaw,    0.0f, 1.0f, 0.0f);
    
    
    
    if (_drawMesh) {
        //DrawObj();
        _map.Draw();
    }
    
    // remember lighting state and disable lighting
    bool isLightingEnabled = glIsEnabled(GL_LIGHTING);
	bool isTextureEnabled = glIsEnabled(GL_TEXTURE_2D);
	if (isLightingEnabled)
		glDisable(GL_LIGHTING);
	if (isTextureEnabled)
		glDisable(GL_TEXTURE_2D);
    
    if (_drawGrid) {
        DrawGroundGrid();
        
        // Draw bounds
        const float* bmin = _map.g_model.getMeshBoundsMin();
        const float* bmax = _map.g_model.getMeshBoundsMax();
        DrawBoxWire(bmin[0],bmin[1],bmin[2], bmax[0],bmax[1],bmax[2]);
    }
    
    if (_drawVoxel && _solid)
        DrawWalkableVoxels();
    
    if (_drawDistanceTransform && _compactHeightfield)
        DistanceTransform(*_compactHeightfield);
    
    if (_drawRegions && _compactHeightfield)
        DrawRegions();
    
    if (_drawContours && _contourSet)
        DrawContours(*_contourSet);
    
    if (_drawNavMesh && _polyMesh)
        DrawPolyMesh(*_polyMesh);
    
    glDepthMask(GL_FALSE);
	if (_startPosSet)
		DrawAgent(_startPos, 0.6f, 2.0f, 0.9f, duRGBA(128,25,0,192));
	if (_endPosSet)
		DrawAgent(_endPos, 0.6f, 2.0f, 0.9f, duRGBA(51,102,0,129));
    glDepthMask(GL_TRUE);
    

    
    //	restore previous lighting state
	if (isLightingEnabled)
		glEnable(GL_LIGHTING);
	if (isTextureEnabled)
		glEnable(GL_TEXTURE_2D);

}

void DemoNavigation::RenderGUI(GUI* gui) {
    
    char text[64];
    snprintf(text, 64, "Camera at (%.2f, %.2f, %.2f)",_cameraPos[0], _cameraPos[1], _cameraPos[2]);
    gui->DrawText( 280, SCREEN_HEIGHT-32-20, GUI::ALIGN_LEFT, text, GUI::RGBA(255,255,255,128) );
    
    char text2[64];
    snprintf(text2, 64, "  looking at (%.2f, %.2f, %.2f)",_targetPos[0], _targetPos[1], _targetPos[2]);
    gui->DrawText( 280, SCREEN_HEIGHT-32-20-20, GUI::ALIGN_LEFT, text2, GUI::RGBA(255,255,255,128) );
    
    gui->BeginScrollArea( NULL, 10, SCREEN_HEIGHT-32-10-500, 200, 500, &_mainScroll );
    if (gui->Button("Load Map")) {
        if (_showMeshes) {
            _showMeshes = false;
        } else {
            _showMeshes = true;
            _files = SearchFiles("meshes", ".obj");
        }
    }
    
    if (gui->Slider("Zoom speed", &_zoomSpeed, 0.0f, 25.0f, 0.01f)) {
        // slider value was updated
    }
    if (gui->Slider("Move speed", &_moveSpeed, 0.0f, 2.5f, 0.01f)) {
        // slider value was updated
    }
    gui->SeparatorLine();
    
    gui->Label("Rasterization");
	gui->Slider("Voxel Size", &_cellSize, 0.1f, 2.0f, 0.01f);
//	gui->Slider("Cell Height", &_cellHeight, 0.1f, 1.0f, 0.01f);
	
	if (_map.g_model.exists()) {
		const float* bmin = _map.g_model.getMeshBoundsMin();
		const float* bmax = _map.g_model.getMeshBoundsMax();
        // calc grid size
        int gw = (int)((bmax[0] - bmin[0])/_cellSize+0.5f);
        int gh = (int)((bmax[2] - bmin[2])/_cellSize+0.5f);
		char text[64];
		snprintf(text, 64, "Voxels  %d x %d", gw, gh);
		gui->Value(text);
	}
    
//    imguiSeparator();
//	imguiLabel("Agent");
//	imguiSlider("Height", &m_agentHeight, 0.1f, 5.0f, 0.1f);
//	imguiSlider("Radius", &m_agentRadius, 0.0f, 5.0f, 0.1f);
//	imguiSlider("Max Climb", &m_agentMaxClimb, 0.1f, 5.0f, 0.1f);
//	imguiSlider("Max Slope", &m_agentMaxSlope, 0.0f, 90.0f, 1.0f);
//	
//	imguiSeparator();
//	imguiLabel("Region");
//	imguiSlider("Min Region Size", &m_regionMinSize, 0.0f, 150.0f, 1.0f);
//	imguiSlider("Merged Region Size", &m_regionMergeSize, 0.0f, 150.0f, 1.0f);
//	if (imguiCheck("Monotore Partitioning", m_monotonePartitioning))
//		m_monotonePartitioning = !m_monotonePartitioning;
//	
//	imguiSeparator();
//	imguiLabel("Polygonization");
//	imguiSlider("Max Edge Length", &m_edgeMaxLen, 0.0f, 50.0f, 1.0f);
//	imguiSlider("Max Edge Error", &m_edgeMaxError, 0.1f, 3.0f, 0.1f);
//	imguiSlider("Verts Per Poly", &m_vertsPerPoly, 3.0f, 12.0f, 1.0f);		
//    
//	imguiSeparator();
//	imguiLabel("Detail Mesh");
//	imguiSlider("Sample Distance", &m_detailSampleDist, 0.0f, 16.0f, 1.0f);
//	imguiSlider("Max Sample Error", &m_detailSampleMaxError, 0.0f, 16.0f, 1.0f);
    
    if (gui->Button("Generate NavMesh")) {
        buildNavMesh();
    }
    
    gui->SeparatorLine();
    gui->Label("Draw");
    if (gui->Check("Mesh", _drawMesh)) _drawMesh = !_drawMesh;
    if (gui->Check("Grid", _drawGrid)) _drawGrid = !_drawGrid;
    if (gui->Check("Voxels", _drawVoxel, _solid)) _drawVoxel = !_drawVoxel;
    if (gui->Check("Distance Transform", _drawDistanceTransform, _compactHeightfield)) _drawDistanceTransform = !_drawDistanceTransform;
    if (gui->Check("Regions", _drawRegions, _compactHeightfield)) _drawRegions = !_drawRegions;
    if (gui->Check("Contours", _drawContours, _contourSet)) _drawContours = !_drawContours;
    if (gui->Check("NavMesh", _drawNavMesh, _polyMesh)) _drawNavMesh = !_drawNavMesh;
    
    if (gui->Button("Start Position")) {
        _startPosSet = true;
        _startPos[0] = 6.0f;
        _startPos[1] = 0.0f;
        _startPos[2] = 294.0f;
    }
    
    if (gui->Button("End Position")) {
        _endPosSet = true;
        _endPos[0] = 145.0f;
        _endPos[1] = 0.0f;
        _endPos[2] = 145.0f;
    }
        
    gui->EndScrollArea();
    
    
    
    
    
    
    
    
    
    
    
    // Show List of mehses window
    if (_showMeshes) {
        gui->BeginScrollArea("Choose Mesh", 10+200+10, SCREEN_HEIGHT-32-10-300, 200, 300, &_meshScroll);

        int meshToLoad = -1;
        for (int i = 0; i < _files.size(); ++i) {
            if (gui->Item(_files[i].c_str()))
                meshToLoad = i;
        }
        
        if (meshToLoad != -1) {
            _showMeshes = false;
            _map.g_model.destroy();
            std::string meshPath = "meshes/"+_files[meshToLoad];
            _map.LoadModel(meshPath.c_str());
//            if (!_map.import(meshPath.c_str())) {
//                std::cout << "Failed to load model";
//            } else {
                //_map.normalize();
                
                //	evaluate the scale of the object
                _scaleFactor = ceilf(log10f(_map.g_model.getRadius()));
                
                // Reset camera to match the mesh bounds.
                _map.g_model.getCenter(_targetPos[0], _targetPos[1], _targetPos[2]);
                
                _cameraPos[0] = _targetPos[0];
                _cameraPos[1] = _targetPos[1];
                _cameraPos[2] = _targetPos[2] + _map.g_model.getRadius() + 0.5f;
                
                cameraPitch = 0.0f;
                cameraYaw = 0.0f;
//            }
        }
        gui->EndScrollArea();
    }
}

// TODO this method must be generic for render any object!!!!
void DemoNavigation::DrawObj() {
    const ModelOBJ::Mesh *pMesh = 0;
    const ModelOBJ::Vertex *pVertices = 0;
    
    //glClear(GL_COLOR_BUFFER_BIT);
    glColor3ub(192,128,0);
    
    for (int i = 0; i < _map.g_model.getNumberOfMeshes(); ++i)  {
        
        pMesh = &_map.g_model.getMesh(i);
        pVertices = _map.g_model.getVertexBuffer();
        
        if (_map.g_model.hasPositions()) {
            glEnableClientState(GL_VERTEX_ARRAY);
            glVertexPointer(3, GL_FLOAT, _map.g_model.getVertexSize(), _map.g_model.getVertexBuffer()->position);
        }
        
        if (_map.g_model.hasNormals()) {
            glEnableClientState(GL_NORMAL_ARRAY);
            glNormalPointer(GL_FLOAT, _map.g_model.getVertexSize(), _map.g_model.getVertexBuffer()->normal);
        }
        
        glDrawElements(GL_TRIANGLES, pMesh->triangleCount * 3, GL_UNSIGNED_INT, _map.g_model.getIndexBuffer() + pMesh->startIndex);
        
        if (_map.g_model.hasNormals())
            glDisableClientState(GL_NORMAL_ARRAY);
        
        if (_map.g_model.hasPositions())
            glDisableClientState(GL_VERTEX_ARRAY);
    }
}

void DemoNavigation::DrawGroundGrid() const
{
	const float range = powf(10, _scaleFactor);
//	const float step = range/30;
	const float step = _cellSize*10;    

    
	//	some lines (draw last if antialiased)
	glBegin(GL_LINES);
	glColor3f(1, 1, 1);
	for (float d = -range ; d <= range; d += step) {
		glVertex3f(range, 0, d);  glVertex3f(-range, 0, d);
		glVertex3f(d, 0, range);  glVertex3f(d, 0, -range);
	}
	glEnd();
}

void DemoNavigation::DrawBoxWire(float minx, float miny, float minz,
                                 float maxx, float maxy, float maxz) const
{
    glBegin(GL_LINES);
	glColor3f(1, 1, 0);
    
    // Top
	glVertex3f(minx, miny, minz);
	glVertex3f(maxx, miny, minz);
	glVertex3f(maxx, miny, minz);
	glVertex3f(maxx, miny, maxz);
	glVertex3f(maxx, miny, maxz);
	glVertex3f(minx, miny, maxz);
	glVertex3f(minx, miny, maxz);
	glVertex3f(minx, miny, minz);
	
	// bottom
	glVertex3f(minx, maxy, minz);
	glVertex3f(maxx, maxy, minz);
	glVertex3f(maxx, maxy, minz);
	glVertex3f(maxx, maxy, maxz);
	glVertex3f(maxx, maxy, maxz);
	glVertex3f(minx, maxy, maxz);
	glVertex3f(minx, maxy, maxz);
	glVertex3f(minx, maxy, minz);
	
	// Sides
	glVertex3f(minx, miny, minz);
	glVertex3f(minx, maxy, minz);
	glVertex3f(maxx, miny, minz);
	glVertex3f(maxx, maxy, minz);
	glVertex3f(maxx, miny, maxz);
	glVertex3f(maxx, maxy, maxz);
	glVertex3f(minx, miny, maxz);
	glVertex3f(minx, maxy, maxz);
    
	glEnd();
}

void DemoNavigation::buildNavMesh() {
    
    // clean up TODO there are memory leaks here!!!!
    delete [] _triareas;
	_triareas = 0;
	_solid = 0;
	_compactHeightfield = 0;
	_contourSet = 0;
	_polyMesh = 0;
	_polyMeshDetail = 0;
	
	const float* bmin = _map.g_model.getMeshBoundsMin();
	const float* bmax = _map.g_model.getMeshBoundsMax();
	const float* verts = _map.g_model.m_verts;
	const int nverts = _map.g_model.m_vertCount;
	const int* tris = _map.g_model.m_tris;
	const int ntris = _map.g_model.m_triCount;
	
	//
	// Step 1. Initialize build config.
	// *********************************
    /// The maximum slope that is considered walkable. [Limits: 0 <= value < 90] [Units: Degrees]
	float cellHeight = 0.2f;
    
    float walkableSlopeAngle = 45.0f;
    float agentHeight = 2.0f;
    float agentMaxClimb = 0.9f;
    //float agentRadius = 0.6f;
    /// Minimum floor to 'ceiling' height that will still allow the floor area to be considered walkable. [Limit: >= 3] [Units: vx]
	int walkableHeight = (int)ceilf(agentHeight / cellHeight);
	/// Maximum ledge height that is considered to still be traversable. [Limit: >=0] [Units: vx] 
	int walkableClimb = (int)floorf(agentMaxClimb / cellHeight);
	/// The distance to erode/shrink the walkable area of the heightfield away from obstructions.  [Limit: >=0] [Units: vx] 
	//int walkableRadius = (int)ceilf(agentRadius / _cellSize);
    
    int regionMinSize = 60;
    int regionMergeSize = 20;
    /// The minimum number of cells allowed to form isolated island areas. [Limit: >=0] [Units: vx] 
	int minRegionArea = regionMinSize * regionMinSize;		// Note: area = size*size
	/// Any regions with a span count smaller than this value will, if possible, 
	/// be merged with larger regions. [Limit: >=0] [Units: vx] 
	int mergeRegionArea = regionMergeSize * regionMergeSize;	// Note: area = size*size
    
    float edgeMaxLen = 50.0f;
    float edgeMaxError = 1.3f;
    int vertsPerPoly = 6;
	/// The maximum allowed length for contour edges along the border of the mesh. [Limit: >=0] [Units: vx] 
	int maxEdgeLen = (int)(edgeMaxLen / _cellSize);
	/// The maximum distance a simplfied contour's border edges should deviate 
	/// the original raw contour. [Limit: >=0] [Units: wu]
	float maxSimplificationError = edgeMaxError;
	/// The maximum number of vertices allowed for polygons generated during the 
	/// contour to polygon conversion process. [Limit: >= 3] 
	int maxVertsPerPoly = vertsPerPoly;
    
	/// Sets the sampling distance to use when generating the detail mesh.
	/// (For height detail only.) [Limits: 0 or >= 0.9] [Units: wu] 
//	float detailSampleDist = _cellSize * 6;
	/// The maximum distance the detail mesh surface should deviate from heightfield
	/// data. (For height detail only.) [Limit: >=0] [Units: wu] 
//	float detailSampleMaxError = cellHeight * 1;
	
    // calc grid size
    int width = (int)((bmax[0] - bmin[0])/_cellSize+0.5f);
    int height = (int)((bmax[2] - bmin[2])/_cellSize+0.5f);
    
	
//    std::cout << "Building navigation:" << std::endl;
//	std::cout << " - " << width << " x " << height << " cells" << std::endl;
//	std::cout << " - " << nverts/1000.0f << " verts, " << ntris/1000.0f << " tris" << std::endl;
	
	//
	// Step 2. Voxelize input polygon soup.
	// *********************************
	
	// Allocate voxel heightfield where we rasterize our input data to.
    heightfield_t* hf = (heightfield_t*)malloc(sizeof(heightfield_t));
	memset(hf, 0, sizeof(heightfield_t));
	_solid = hf;
	if (!_solid) {
		std::cout << "buildNavigation: Out of memory 'solid'.";
        return;
	}
    _solid->width = width;
	_solid->height = height;
	rcVcopy(_solid->bmin, bmin);
	rcVcopy(_solid->bmax, bmax);
	_solid->cs = _cellSize;
	_solid->ch = cellHeight;
	_solid->spans = (span_t**)malloc(sizeof(span_t*)*_solid->width*_solid->height);
	if (!_solid->spans) {
		std::cout << "buildNavigation: Could not create solid heightfield.";
		return;
	}
	memset(_solid->spans, 0, sizeof(span_t*)*_solid->width*_solid->height);
	
	// Allocate array that can hold triangle area types.
	_triareas = new unsigned char[ntris];
	if (!_triareas) {
		std::cout << "buildNavigation: Out of memory 'm_triareas'";
		return;
	}
    memset(_triareas, 0, ntris*sizeof(unsigned char));
	
	// Find triangles which are walkable based on their slope
	const float walkableThr = cosf(walkableSlopeAngle/180.0f*M_PI);
	
	float norm[3];
	
	for (int i = 0; i < ntris; ++i) {
		const int* tri = &tris[i*3];
		calcTriNormal(&verts[tri[0]*3], &verts[tri[1]*3], &verts[tri[2]*3], norm);
		// Check if the face is walkable.
		if (norm[1] > walkableThr)
			_triareas[i] = WALKABLE_AREA;
	}

    // Rasterize walkable triangles
	rcRasterizeTriangles(verts, nverts, tris, _triareas, ntris, *_solid, walkableClimb);
    
	
	//
	// Step 3. Filter walkables surfaces.
	// *********************************
	
	// Once all geoemtry is rasterized, we do initial pass of filtering to
	// remove unwanted overhangs caused by the conservative rasterization
	// as well as filter spans where the character cannot possibly stand.
	rcFilterLowHangingWalkableObstacles(walkableClimb, *_solid);
	rcFilterLedgeSpans(walkableHeight, walkableClimb, *_solid);
	rcFilterWalkableLowHeightSpans(walkableHeight, *_solid);
    
    
	//
	// Step 4. Partition walkable surface to simple regions.
	// *********************************
    
	// Compact the heightfield so that it is faster to handle from now on.
	// This will result more cache coherent data as well as the neighbours
	// between walkable cells will be calculated.
    _compactHeightfield = (compactHeightfield_t*)malloc(sizeof(compactHeightfield_t));
	memset(_compactHeightfield, 0, sizeof(compactHeightfield_t));

	if (!_compactHeightfield) {
		std::cout << "buildNavigation: Out of memory '_compactHeightfield'.";
		return;
	}
	if (!rcBuildCompactHeightfield(walkableHeight, walkableClimb, *_solid, *_compactHeightfield)) {
		std::cout << "buildNavigation: Could not build compact data.";
		return;
	}
    
	// Erode the walkable area by agent radius.
//	if (!rcErodeWalkableArea(walkableRadius, *_compactHeightfield)) {
//		std::cout << "buildNavigation: Could not erode.";
//		return;
//	}
    
    // Prepare for region partitioning, by calculating distance field along the walkable surface.
    if (!rcBuildDistanceField(*_compactHeightfield)) {
        std::cout << "buildNavigation: Could not build distance field.";
        return;
    }
    
    // Partition the walkable surface into simple regions without holes.
    if (!rcBuildRegions(*_compactHeightfield, 0, minRegionArea, mergeRegionArea)) {
        std::cout << "buildNavigation: Could not build regions.";
        return;
    }
    
    
	//
	// Step 5. Trace and simplify region contours.
	// *********************************
	
	// Create contours.
    _contourSet = (contourSet_t*)malloc(sizeof(contourSet_t));
	memset(_contourSet, 0, sizeof(contourSet_t));

	if (!_contourSet) {
		std::cout << "buildNavigation: Out of memory '_contourSet'.";
		return;
	}
	if (!rcBuildContours(*_compactHeightfield, maxSimplificationError, maxEdgeLen, *_contourSet)) {
		std::cout << "buildNavigation: Could not create contours.";
		return;
	}
	
	//
	// Step 6. Build polygons mesh from contours.
	// *********************************
	
	// Build polygon navmesh from the contours.
    _polyMesh = (polyMesh_t*)malloc(sizeof(polyMesh_t));
	memset(_polyMesh, 0, sizeof(polyMesh_t));
    
	if (!_polyMesh) {
		std::cout << "buildNavigation: Out of memory 'pmesh'.";
		return;
	}
	if (!rcBuildPolyMesh(*_contourSet, maxVertsPerPoly, *_polyMesh)) {
		std::cout << "buildNavigation: Could not triangulate contours.";
		return;
	}
	
	//
	// Step 7. Create detail mesh which allows to access approximate height on each polygon.
	// *********************************
	
//    _polyMeshDetail = (polyMeshDetail_t*)malloc(sizeof(polyMeshDetail_t));
//	memset(_polyMeshDetail, 0, sizeof(polyMeshDetail_t));
//    
//	if (!_polyMeshDetail) {
//		std::cout << "buildNavigation: Out of memory 'pmdtl'.";
//		return;
//	}
//    
//	if (!rcBuildPolyMeshDetail(*_polyMesh, *_compactHeightfield, detailSampleDist, 
//                               detailSampleMaxError, *_polyMeshDetail)) {
//		std::cout << "buildNavigation: Could not build detail mesh.";
//		return;
//	}

    
    
}


void DemoNavigation::DrawWalkableVoxels() 
{
	const float* orig = _solid->bmin;
	const float cs = _solid->cs;
	const float ch = _solid->ch;
	const int w = _solid->width;
	const int h = _solid->height;
	
	unsigned int fcol[6];
	duCalcBoxColors(fcol, duRGBA(255,255,255,255), duRGBA(217,217,217,255));
    
    glBegin(GL_QUADS);
	
	for (int y = 0; y < h; ++y)
	{
		for (int x = 0; x < w; ++x)
		{
			float fx = orig[0] + x*cs;
			float fz = orig[2] + y*cs;
			const span_t* s = _solid->spans[x + y*w];
			while (s)
			{
				if (s->area == WALKABLE_AREA) {
					fcol[0] = duRGBA(64,128,160,255);
				} else if (s->area == RC_NULL_AREA) {
					fcol[0] = duRGBA(64,64,64,255);
				} else {
					fcol[0] = duMultCol(duIntToCol(s->area, 255), 200);
                }
				
				duAppendBox(fx, orig[1]+s->smin*ch, fz, fx+cs, orig[1] + s->smax*ch, fz+cs, fcol);
				s = s->next;
			}
		}
	}
	
	glEnd();
}

void DemoNavigation::DrawRegions() 
{
    const float cs = _compactHeightfield->cs;
	const float ch = _compactHeightfield->ch;
    
	glBegin(GL_QUADS);
    
	for (int y = 0; y < _compactHeightfield->height; ++y)
	{
		for (int x = 0; x < _compactHeightfield->width; ++x)
		{
			const float fx = _compactHeightfield->bmin[0] + x*cs;
			const float fz = _compactHeightfield->bmin[2] + y*cs;
			const compactCell_t& c = _compactHeightfield->cells[x+y*_compactHeightfield->width];
			
			for (unsigned i = c.index, ni = c.index+c.count; i < ni; ++i)
			{
				const compactSpan_t& s = _compactHeightfield->spans[i];
				const float fy = _compactHeightfield->bmin[1] + (s.y)*ch;
				unsigned int color;
				if (s.reg)
					color = duIntToCol(s.reg, 192);
				else
					color = duRGBA(0,0,0,64);
                
                glColor4ubv((GLubyte*)&color);
                glVertex3f(fx, fy, fz);
				glVertex3f(fx, fy, fz+cs);
				glVertex3f(fx+cs, fy, fz+cs);
				glVertex3f(fx+cs, fy, fz);
			}
		}
	}
	
    glEnd();
}

void DemoNavigation::DistanceTransform(const compactHeightfield_t& chf)
{
	if (!chf.dist) return;
    
	const float cs = chf.cs;
	const float ch = chf.ch;
    
	float maxd = chf.maxDistance;
	if (maxd < 1.0f) maxd = 1;
	const float dscale = 255.0f / maxd;
	
	glBegin(GL_QUADS);
	
	for (int y = 0; y < chf.height; ++y) {
		for (int x = 0; x < chf.width; ++x) {
			const float fx = chf.bmin[0] + x*cs;
			const float fz = chf.bmin[2] + y*cs;
			const compactCell_t& c = chf.cells[x+y*chf.width];
			
			for (unsigned i = c.index, ni = c.index+c.count; i < ni; ++i) {
				const compactSpan_t& s = chf.spans[i];
				const float fy = chf.bmin[1] + (s.y+1)*ch;
				const unsigned char cd = (unsigned char)(chf.dist[i] * dscale);
				const unsigned int color = duRGBA(cd,cd,cd,255);
                glColor4ubv((GLubyte*)&color);
                glVertex3f(fx, fy, fz);
				glVertex3f(fx, fy, fz+cs);
				glVertex3f(fx+cs, fy, fz+cs);
				glVertex3f(fx+cs, fy, fz);
			}
		}
	}
    glEnd();
}

void DemoNavigation::DrawContours(const contourSet_t& cset, const float alpha)
{
	const float* orig = cset.bmin;
	const float cs = cset.cs;
	const float ch = cset.ch;
	
	const unsigned char a = (unsigned char)(alpha*255.0f);
	
    glLineWidth(2.5f);
    glBegin(GL_LINES);
	
	for (int i = 0; i < cset.nconts; ++i) {
		const contour_t& c = cset.conts[i];
		if (!c.nverts)
			continue;
		const unsigned int color = duIntToCol(c.reg, a);
		const unsigned int bcolor = duLerpCol(color,duRGBA(255,255,255,a),128);
		for (int j = 0, k = c.nverts-1; j < c.nverts; k=j++) {
			const int* va = &c.verts[k*4];
			const int* vb = &c.verts[j*4];
			unsigned int col = (va[3] & RC_AREA_BORDER) ? bcolor : color; 
            glColor4ubv((GLubyte*)&col);
			float fx,fy,fz;
			fx = orig[0] + va[0]*cs;
			fy = orig[1] + (va[1]+1+(i&1))*ch;
			fz = orig[2] + va[2]*cs;
            glVertex3f(fx,fy,fz);
			fx = orig[0] + vb[0]*cs;
			fy = orig[1] + (vb[1]+1+(i&1))*ch;
			fz = orig[2] + vb[2]*cs;
            glVertex3f(fx,fy,fz);

		}
	}
	glEnd();
    
    glPointSize(3.0f);
    glBegin(GL_POINTS);
	
	for (int i = 0; i < cset.nconts; ++i) {
		const contour_t& c = cset.conts[i];
		unsigned int color = duDarkenCol(duIntToCol(c.reg, a));
		for (int j = 0; j < c.nverts; ++j)
		{
			const int* v = &c.verts[j*4];
			float off = 0;
			unsigned int colv = color;
			if (v[3] & RC_BORDER_VERTEX)
			{
				colv = duRGBA(255,255,255,a);
				off = ch*2;
			}
            
			float fx = orig[0] + v[0]*cs;
			float fy = orig[1] + (v[1]+1+(i&1))*ch + off;
			float fz = orig[2] + v[2]*cs;
            glColor4ubv((GLubyte*)&colv);
            glVertex3f(fx,fy,fz);
		}
	}
	glEnd();
}

void DemoNavigation::DrawPolyMesh(const struct polyMesh_t& mesh)
{
	const int nvp = mesh.nvp;
	const float cs = mesh.cs;
	const float ch = mesh.ch;
	const float* orig = mesh.bmin;
	
	glBegin(GL_TRIANGLES);
	
	for (int i = 0; i < mesh.npolys; ++i) {
		const unsigned short* p = &mesh.polys[i*nvp*2];
		
		unsigned int color;
		if (mesh.areas[i] == WALKABLE_AREA)
			color = duRGBA(0,192,255,64);
		else if (mesh.areas[i] == RC_NULL_AREA)
			color = duRGBA(0,0,0,64);
		else
			color = duIntToCol(mesh.areas[i], 255);
        
        glColor4ubv((GLubyte*)&color);
		
		unsigned short vi[3];
		for (int j = 2; j < nvp; ++j) {
			if (p[j] == RC_MESH_NULL_IDX) break;
			vi[0] = p[0];
			vi[1] = p[j-1];
			vi[2] = p[j];
			for (int k = 0; k < 3; ++k) {
				const unsigned short* v = &mesh.verts[vi[k]*3];
				const float x = orig[0] + v[0]*cs;
				const float y = orig[1] + (v[1]+1)*ch;
				const float z = orig[2] + v[2]*cs;
                glVertex3f(x,y,z);
			}
		}
	}
	glEnd();
    
	// Draw neighbours edges
	const unsigned int coln = duRGBA(0,48,64,32);
    glLineWidth(1.5f);
    glBegin(GL_LINES);
    glColor4ubv((GLubyte*)&coln);
	for (int i = 0; i < mesh.npolys; ++i) {
		const unsigned short* p = &mesh.polys[i*nvp*2];
		for (int j = 0; j < nvp; ++j) {
			if (p[j] == RC_MESH_NULL_IDX) break;
			if (p[nvp+j] & 0x8000) continue;
			const int nj = (j+1 >= nvp || p[j+1] == RC_MESH_NULL_IDX) ? 0 : j+1; 
			const int vi[2] = {p[j], p[nj]};
			
			for (int k = 0; k < 2; ++k) {
				const unsigned short* v = &mesh.verts[vi[k]*3];
				const float x = orig[0] + v[0]*cs;
				const float y = orig[1] + (v[1]+1)*ch + 0.1f;
				const float z = orig[2] + v[2]*cs;
                glVertex3f(x,y,z);
			}
		}
	}
	glEnd();
	
	// Draw boundary edges
	const unsigned int colb = duRGBA(0,48,64,220);
    glLineWidth(2.5f);
    glBegin(GL_LINES);
	for (int i = 0; i < mesh.npolys; ++i)
	{
		const unsigned short* p = &mesh.polys[i*nvp*2];
		for (int j = 0; j < nvp; ++j)
		{
			if (p[j] == RC_MESH_NULL_IDX) break;
			if ((p[nvp+j] & 0x8000) == 0) continue;
			const int nj = (j+1 >= nvp || p[j+1] == RC_MESH_NULL_IDX) ? 0 : j+1; 
			const int vi[2] = {p[j], p[nj]};
			
			unsigned int col = colb;
			if ((p[nvp+j] & 0xf) != 0xf)
				col = duRGBA(255,255,255,128);
            glColor4ubv((GLubyte*)&col);
			for (int k = 0; k < 2; ++k)
			{
				const unsigned short* v = &mesh.verts[vi[k]*3];
				const float x = orig[0] + v[0]*cs;
				const float y = orig[1] + (v[1]+1)*ch + 0.1f;
				const float z = orig[2] + v[2]*cs;
                glVertex3f(x,y,z);
			}
		}
	}
	glEnd();
	
    glPointSize(3.0f);
    glBegin(GL_POINTS);
	const unsigned int colv = duRGBA(0,0,0,220);
    glColor4ubv((GLubyte*)&colv);
	for (int i = 0; i < mesh.nverts; ++i) {
		const unsigned short* v = &mesh.verts[i*3];
		const float x = orig[0] + v[0]*cs;
		const float y = orig[1] + (v[1]+1)*ch + 0.1f;
		const float z = orig[2] + v[2]*cs;
		glVertex3f(x,y,z);
	}
    glEnd();

}

void duDebugDrawCylinderWire(float minx, float miny, float minz,
							 float maxx, float maxy, float maxz, unsigned int col, const float lineWidth)
{
    glLineWidth(lineWidth);
    glBegin(GL_LINES);
    
    glColor4ubv((GLubyte*)&col);

    static const int NUM_SEG = 16;
	static float dir[NUM_SEG*2];
	static bool init = false;
	if (!init) {
		init = true;
		for (int i = 0; i < NUM_SEG; ++i) {
			const float a = (float)i/(float)NUM_SEG*M_PI*2;
			dir[i*2] = cosf(a);
			dir[i*2+1] = sinf(a);
		}
	}
	
	const float cx = (maxx + minx)/2;
	const float cz = (maxz + minz)/2;
	const float rx = (maxx - minx)/2;
	const float rz = (maxz - minz)/2;
	
	for (int i = 0, j = NUM_SEG-1; i < NUM_SEG; j = i++) {
		glVertex3f(cx+dir[j*2+0]*rx, miny, cz+dir[j*2+1]*rz);
		glVertex3f(cx+dir[i*2+0]*rx, miny, cz+dir[i*2+1]*rz);
		glVertex3f(cx+dir[j*2+0]*rx, maxy, cz+dir[j*2+1]*rz);
		glVertex3f(cx+dir[i*2+0]*rx, maxy, cz+dir[i*2+1]*rz);
	}
	for (int i = 0; i < NUM_SEG; i += NUM_SEG/4) {
		glVertex3f(cx+dir[i*2+0]*rx, miny, cz+dir[i*2+1]*rz);
		glVertex3f(cx+dir[i*2+0]*rx, maxy, cz+dir[i*2+1]*rz);
	}
    
    glEnd();
}

void duDebugDrawCircle(const float x, const float y, const float z,
					   const float r, unsigned int col, const float lineWidth)
{
	glLineWidth(lineWidth);
    glBegin(GL_LINES);
    
    glColor4ubv((GLubyte*)&col);
    
    static const int NUM_SEG = 40;
	static float dir[40*2];
	static bool init = false;
	if (!init) {
		init = true;
		for (int i = 0; i < NUM_SEG; ++i) {
			const float a = (float)i/(float)NUM_SEG*M_PI*2;
			dir[i*2] = cosf(a);
			dir[i*2+1] = sinf(a);
		}
	}
	
	for (int i = 0, j = NUM_SEG-1; i < NUM_SEG; j = i++) {
		glVertex3f(x+dir[j*2+0]*r, y, z+dir[j*2+1]*r);
		glVertex3f(x+dir[i*2+0]*r, y, z+dir[i*2+1]*r);
	}
    
	glEnd();
}

void DemoNavigation::DrawAgent(const float* pos, float r, float h, float c, const unsigned int col)
{
	// Agent dimensions.	
	duDebugDrawCylinderWire(pos[0]-r, pos[1]+0.02f, pos[2]-r, pos[0]+r, pos[1]+h, pos[2]+r, col, 2.0f);
    
	duDebugDrawCircle(pos[0],pos[1]+c,pos[2],r,duRGBA(0,0,0,64),1.0f);
    
	unsigned int colb = duRGBA(0,0,0,196);
    glLineWidth(1.0f);
    glBegin(GL_LINES);
    glColor4ubv((GLubyte*)&colb);
	glVertex3f(pos[0], pos[1]-c, pos[2]);
	glVertex3f(pos[0], pos[1]+c, pos[2]);
	glVertex3f(pos[0]-r/2, pos[1]+0.02f, pos[2]);
	glVertex3f(pos[0]+r/2, pos[1]+0.02f, pos[2]);
	glVertex3f(pos[0], pos[1]+0.02f, pos[2]-r/2);
	glVertex3f(pos[0], pos[1]+0.02f, pos[2]+r/2);
	glEnd();
	
}
