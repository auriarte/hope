//
//  main.cpp
//  testSDL
//
//  Created by Ehsan Khosroshahi on 1/17/13.
//  Copyright (c) 2013 Ehsan Khosroshahi. All rights reserved.
//

#include <iostream>
#include <string>
#include <SDL/SDL.h>
//#include "SDLMain.h"
#include <SDL/SDL_opengl.h>
//#include <OpenGL/OpenGL.h>
//#include <OpenGL/gl.h>
//#include <OpenGL/glu.h>
//#include <SDL/SDL_video.h>


//Screen attributes
const int SCREEN_WIDTH = 800;
const int SCREEN_HEIGHT = 600;
const int SCREEN_BPP = 32;


void BuildWall ( float X1, float Y1 ,float X2 , float Y2 ,float thickness )
{
    glColor4ub(150, 0, 0, 255);
    
    glLineWidth(thickness);
    glBegin(GL_LINES); // GL_POINT, GL_LINE, GL_LINE_STRIP, GL_QUAD, GL_TRIANGLE, GL_POLYGON
    
    glVertex2f(X1, Y1);
    glVertex2f(X2, Y2);

    
    glEnd();
    
    glLineWidth(1);
}

int main(int argc,char* argv[])
{
    SDL_Init(SDL_INIT_EVERYTHING);
    
    SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_BUFFER_SIZE, 32);
    SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 16);
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
    
    // the title of the window
    SDL_WM_SetCaption("starting from the scratch !!", NULL);
    
    // window's size
    if( SDL_SetVideoMode( SCREEN_WIDTH, SCREEN_HEIGHT, SCREEN_BPP, SDL_OPENGL ) == NULL )
        {
        return false;
        }
    
    // color of the  screen     red,green,blue,alpha
   glClearColor(1, 1, 1, 1);   // range is 0 to 256
    
    // what portion we want to view
    glViewport(0, 0, 800, 600);
    
    // changing the color in a gradual way
    glShadeModel(GL_SMOOTH);
    
    // on what it is been draw for 2D
    glMatrixMode(GL_PROJECTION); // GL_MODEL for 3d
    glLoadIdentity(); //
    
    // for 3d you have to inable it
    glDisable(GL_DEPTH_TEST);

    
    std::cout << "it is running" ;
    
    //Quit flag
    bool quit = false;
    
    SDL_Event event;
    
    // player position in the screen
    float player_positionX=5, player_positionY=595;
    
    std::string move;
    
    while( quit == false )
        {
            //EVENTS
            while( SDL_PollEvent(&event))
            {
                if(event.type == SDL_QUIT)
                    quit=true;
                
                if (event.type == SDL_KEYDOWNMASK && event.key.keysym.sym == SDLK_r)
                {
                    glClearColor(1, 0, 0, 1);
                }
                
                if (event.type == SDL_KEYDOWN)
                {
                    if ( event.key.keysym.sym == SDLK_UP)
                        move = "up";
                    if ( event.key.keysym.sym == SDLK_DOWN)
                        move = "down";
                    if ( event.key.keysym.sym == SDLK_RIGHT)
                        move = "right";
                    if ( event.key.keysym.sym == SDLK_LEFT)
                        move = "left";

                }
                
                // defining the event where you pull you fingure from thr key and the player should stop
                if (event.type == SDL_KEYUP)
                {
                    move = "stop";
                    
                }

            }
            
            ////////////LOGIC/////////////
            
            // for the movement
            if(move == "up")
                player_positionY -= 4;
            else if (move =="down")
                player_positionY += 4;
            else if (move == "right")
                player_positionX += 4;
            else if (move == "left")
                player_positionX -= 4;
            
            
            // Checking to see that player does not go out of the page
            if (player_positionX < 5 )
                player_positionX = 5;
            if (player_positionX > 795 )
                player_positionX = 795;
            if (player_positionY < 5 )
                player_positionY = 5;
            if (player_positionY > 595 )
                player_positionY = 595 ;
                
            
            
            ///////////// RENDERING/////////////////
            glClear(GL_COLOR_BUFFER_BIT);
            
            glPushMatrix();
            
            // matrix ortogonal
            glOrtho(0, 800, 600, 0, -1, 1);
            
            // building some walls
            BuildWall(400, 300, 500, 300, 40);
            BuildWall(600, 200, 600, 280, 40);
            BuildWall(200, 450, 350, 450, 100);
            BuildWall(350, 450, 350, 550, 1);
            
            glColor4ub(150, 0, 0, 255);
            
            glBegin(GL_QUADS); // GL_POINT, GL_LINE, GL_LINE_STRIP, GL_QUAD, GL_TRIANGLE, GL_POLYGON
            
            glVertex2f(player_positionX -5 , player_positionY -5);
            glVertex2f(player_positionX + 5 , player_positionY -5);
            glVertex2f(player_positionX + 5, player_positionY +5);
            glVertex2f(player_positionX -5 , player_positionY +5);
            
            glEnd();
            
            glColor4ub(0, 255, 0, 255);
            
            glBegin(GL_QUADS); // GL_POINT, GL_LINE, GL_LINE_STRIP, GL_QUAD, GL_TRIANGLE, GL_POLYGON
            
            glVertex2f(player_positionX -5 , player_positionY -5);
            glVertex2f(player_positionX + 5 , player_positionY -5);
            glVertex2f(player_positionX + 5, player_positionY +5);
            glVertex2f(player_positionX -5 , player_positionY +5);
            
            glEnd();
            
            glBegin(GL_LINES); // GL_POINTS, GL_LINES, GL_LINE_STRIPS, GL_QUADS, GL_TRIANGLES, GL_POLYGONS
            
            glVertex2f(10 , 10);
            glVertex2f(400, 400);
            glVertex2f(10 , 400);
            glVertex2f(400, 10);
            
            glEnd();
            
            glPopMatrix();
            
            SDL_GL_SwapBuffers();
        
        }

    //std::cin.get();
    
    // this function will force to quit the wndow after 5 sec
    SDL_Delay(1);
    
    SDL_Quit();
    

    return 0;
}

////Screen attributes
//const int SCREEN_WIDTH = 640;
//const int SCREEN_HEIGHT = 480;
//const int SCREEN_BPP = 32;
//
////The frame rate
//const int FRAMES_PER_SECOND = 60;
//
////Event handler
//SDL_Event event;
//
////Rendering flag
//bool renderQuad = true;
//
////The timer
//class Timer
//{
//private:
//    //The clock time when the timer started
//    int startTicks;
//    
//    //The ticks stored when the timer was paused
//    int pausedTicks;
//    
//    //The timer status
//    bool paused;
//    bool started;
//    
//public:
//    //Initializes variables
//    Timer();
//    
//    //The various clock actions
//    void start();
//    void stop();
//    void pause();
//    void unpause();
//    
//    //Gets the timer's time
//    int get_ticks();
//    
//    //Checks the status of the timer
//    bool is_started();
//    bool is_paused();
//};
//
//bool initGL()
//{
//    //Initialize Projection Matrix
//    glMatrixMode( GL_PROJECTION );
//    glLoadIdentity();
//    
//    //Initialize Modelview Matrix
//    glMatrixMode( GL_MODELVIEW );
//    glLoadIdentity();
//    
//    //Initialize clear color
//    glClearColor( 0.f, 0.f, 0.f, 1.f );
//    
//    //Check for error
//    GLenum error = glGetError();
//    if( error != GL_NO_ERROR )
//    {
//        printf( "Error initializing OpenGL! %s\n", gluErrorString( error ) );
//        return false;
//    }
//    
//    return true;
//}
//
//bool init()
//{
//    //Initialize SDL
//    if( SDL_Init( SDL_INIT_EVERYTHING ) < 0 )
//    {
//        return false;
//    }
//    
//    //Create Window
//    if( SDL_SetVideoMode( SCREEN_WIDTH, SCREEN_HEIGHT, SCREEN_BPP, SDL_OPENGL ) == NULL )
//    {
//        return false;
//    }
//    
//    //Enable unicode
//    SDL_EnableUNICODE( SDL_TRUE );
//    
//    //Initialize OpenGL
//    if( initGL() == false )
//    {
//        return false;
//    }
//    
//    //Set caption
//    SDL_WM_SetCaption( "OpenGL Test", NULL );
//    
//    return true;
//}
//
//void handleKeys( unsigned char key, int x, int y )
//{
//    //Toggle quad
//    if( key == 'q' )
//    {
//        renderQuad = !renderQuad;
//    }
//}
//
//void update()
//{
//    
//}
//
//void render()
//{
//    //Clear color buffer
//    glClear( GL_COLOR_BUFFER_BIT );
//    
//    //Render quad
//    if( renderQuad == true )
//    {
//        glBegin( GL_QUADS );
//        glVertex2f( -0.5f, -0.5f );
//        glVertex2f(  0.5f, -0.5f );
//        glVertex2f(  0.5f,  0.5f );
//        glVertex2f( -0.5f,  0.5f );
//        glEnd();
//    }
//    
//    //Update screen
//    SDL_GL_SwapBuffers();
//}
//
//void clean_up()
//{
//    //Quit SDL
//    SDL_Quit();
//}
//
//Timer::Timer()
//{
//    //Initialize the variables
//    startTicks = 0;
//    pausedTicks = 0;
//    paused = false;
//    started = false;
//}
//
//void Timer::start()
//{
//    //Start the timer
//    started = true;
//    
//    //Unpause the timer
//    paused = false;
//    
//    //Get the current clock time
//    startTicks = SDL_GetTicks();
//}
//
//void Timer::stop()
//{
//    //Stop the timer
//    started = false;
//    
//    //Unpause the timer
//    paused = false;
//}
//
//void Timer::pause()
//{
//    //If the timer is running and isn't already paused
//    if( ( started == true ) && ( paused == false ) )
//    {
//        //Pause the timer
//        paused = true;
//        
//        //Calculate the paused ticks
//        pausedTicks = SDL_GetTicks() - startTicks;
//    }
//}
//
//void Timer::unpause()
//{
//    //If the timer is paused
//    if( paused == true )
//    {
//        //Unpause the timer
//        paused = false;
//        
//        //Reset the starting ticks
//        startTicks = SDL_GetTicks() - pausedTicks;
//        
//        //Reset the paused ticks
//        pausedTicks = 0;
//    }
//}
//
//int Timer::get_ticks()
//{
//    //If the timer is running
//    if( started == true )
//    {
//        //If the timer is paused
//        if( paused == true )
//        {
//            //Return the number of ticks when the timer was paused
//            return pausedTicks;
//        }
//        else
//        {
//            //Return the current time minus the start time
//            return SDL_GetTicks() - startTicks;
//        }
//    }
//    
//    //If the timer isn't running
//    return 0;
//}
//
//bool Timer::is_started()
//{
//    return started;
//}
//
//bool Timer::is_paused()
//{
//    return paused;
//}
//
//int main( int argc, char *argv[] )
//{
//    //Quit flag
//    bool quit = false;
//    
//    //Initialize
//    if( init() == false )
//    {
//        return 1;
//    }
//    
//    //The frame rate regulator
//    Timer fps;
//    
//	//Wait for user exit
//	while( quit == false )
//	{
//        //Start the frame timer
//        fps.start();
//        
//        //While there are events to handle
//		while( SDL_PollEvent( &event ) )
//		{
//			if( event.type == SDL_QUIT )
//			{
//                quit = true;
//            }
//            else if( event.type == SDL_KEYDOWN )
//            {
//                //Handle keypress with current mouse position
//                int x = 0, y = 0;
//                SDL_GetMouseState( &x, &y );
//                handleKeys( event.key.keysym.unicode, x, y );
//            }
//		}
//        
//        //Run frame update
//        update();
//        
//        //Render frame
//        render();
//        
//        //Cap the frame rate
//        if( fps.get_ticks() < 1000 / FRAMES_PER_SECOND )
//        {
//            SDL_Delay( ( 1000 / FRAMES_PER_SECOND ) - fps.get_ticks() );
//        }
//	}
//    
//	//Clean up
//	clean_up();
//    
//	return 0;
//}
//
