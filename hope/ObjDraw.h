//
//  ObjDraw.h
//  hope
//
//  Created by Ehsan Khosroshahi on 3/12/13.
//
//

#ifndef __hope__ObjDraw__
#define __hope__ObjDraw__

#include "GameFlow.h"
#include <iostream>
#include <cassert>
#include <cmath>
#include <map>
#include <sstream>
#include <stdexcept>
#include <string>
#include <vector>



//#include "bitmap.h"
//#include "gl2.h"
#include "model_obj.h"
//#include "resource.h"
//#include "WGL_ARB_multisample.h"

class ObjDraw {
private:
    
    //-----------------------------------------------------------------------------
    // Type definitions.
    //-----------------------------------------------------------------------------
    
    typedef std::map<std::string, GLuint> ModelTextures;
    
    
    //-----------------------------------------------------------------------------
    // Globals.
    //-----------------------------------------------------------------------------
    
    GLuint              g_nullTexture;
//    GLuint              g_blinnPhongShader;
//    GLuint              g_normalMappingShader;
//    float               g_maxAnisotrophy;
//    float               g_heading;
//    float               g_pitch;
//    float               g_cameraPos[3];
    float               g_targetPos[3];
    bool                g_isFullScreen;
    bool                g_hasFocus;
    bool                g_enableWireframe;
    bool                g_enableTextures ;
    bool                g_supportsProgrammablePipeline;
    bool                g_cullBackFaces = true;
    
    ModelTextures       g_modelTextures;
    
    char                *pszFilename;

    //ObjDraw(const char *pszFilename);
    
    void                Cleanup();
    void                CleanupApp();
    GLuint              CreateNullTexture(int width, int height);
    void                DrawFrame();
    

    void                InitApp(const char *pszFilename);
    
      GLuint              LoadTexture(const char *pszFilename);
    //void              Log(const char *pszMessage);
    //void              ReadTextFileFromResource(const char *pResouceId, std::string &buffer);
    
    void                UnloadModel();
  
    
public:
    ObjDraw();
    //ObjDraw(const char *pszFilename);
    ModelOBJ            g_model;
    void                Draw();
    void                LoadModel(const char *pszFilename);
    
    
};






#endif /* defined(__hope__ObjDraw__) */
