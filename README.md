# Hope engine

Game engine for CS 680 - Game Engine course at Drexel.

![Architecture.png](https://bitbucket.org/repo/6zXK9q/images/2728608012-Architecture.png)

Some examples of the Game Engine running

![example1.png](https://bitbucket.org/repo/6zXK9q/images/2458711974-example1.png)
![example2.png](https://bitbucket.org/repo/6zXK9q/images/1610214484-example2.png)

## Authors
* Ehsan Khosroshahi
* Alberto Uriarte
* Josep Valls

## Dependencies

* [SDL 1.12.15][1]
* [SDL_image 1.2.12][2]
* OpenGL

## Includes

* [TrueType Font Rendering][3]. An improvement could be [Font-Stash][4]

## References

* GUI based on [Immediate Mode GUIs][5]

[1]: http://www.libsdl.org/download-1.2.php
[2]: http://www.libsdl.org/projects/SDL_image/
[3]: http://mollyrocket.com/forums/viewtopic.php?t=736
[4]: https://github.com/akrinke/Font-Stash
[5]: http://sol.gfxile.net/imgui/ch02.html