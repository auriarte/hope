#ifndef _HELPER_H
#define _HELPER_H

#define PI 3.1415926535897932384626433832795
#define RADIAN_TO_DEGREE 180/PI
#define DEGREE_TO_RADIAN PI/180

//#include "basecode/window.h"
#include "SDL.h"
#include "SDL_opengl.h"
#include <iostream>
#include <sstream>
#include <string>
#include <cmath>
#include <time.h>
#include <stdio.h>

typedef struct myVector
{
	float x, y, z;

	myVector(){};
	myVector(float _x, float _y, float _z)
	{
		x = _x;
		y = _y;
		z = _z;
	}
	inline void set(float _x, float _y, float _z)
	{
		x = _x;
		y = _y;
		z = _z;
	}
	inline void set(myVector vec)
	{
		x = vec.x;
		y = vec.y;
		z = vec.z;
	}
	inline void addAndSet(myVector vec1, myVector vec2)
	{
		x = vec1.x + vec2.x;
		y = vec1.y + vec2.y;
		z = vec1.z + vec2.z;
	}
	inline void add(float _x, float _y, float _z)
	{
		x += _x;
		y += _y;
		z += _z;
	}
	inline void add(myVector vec)
	{
		x += vec.x;
		y += vec.y;
		z += vec.z;
	}
	inline void subtractAndSet(myVector vec1, myVector vec2)
	{
		x = vec1.x - vec2.x;
		y = vec1.y - vec2.y;
		z = vec1.z - vec2.z;
	}
	inline void subtract(myVector vec)
	{
		x -= vec.x;
		y -= vec.y;
		z -= vec.z;
	}
	inline void multiply(float ratio)
	{
		x *= ratio;
		y *= ratio;
		z *= ratio;
	}
	inline void multiply(myVector ratio)
	{
		x *= ratio.x;
		y *= ratio.y;
		z *= ratio.z;
	}
	inline myVector multiplyAndReturn(float ratio)
	{
		myVector temp;
		temp.x = x * ratio;
		temp.y = y * ratio;
		temp.z = z * ratio;
		return temp;
	}
	inline void calculateVector(myVector P1, myVector P2)
	{//calculates vector that passes through P1,P2 points
		x = P2.x - P1.x;
		y = P2.y - P1.y;
		z = P2.z - P1.z;
	}
	inline void calculateUnitVector()
	{
		float magnitude = this->calculateMagnitude();
		this->multiply(1/magnitude);
	}
	inline float calculateMagnitude()
	{
		float magnitude;
		magnitude = sqrt(x*x + y*y + z*z);
		return magnitude;
	}
	inline void averageVectors(myVector *vectorGroup, int num)
	{
		set(0,0,0);
		for(int i=0;i<num;i++)
		{
			x += vectorGroup[i].x;
			y += vectorGroup[i].y;
			z += vectorGroup[i].z;
		}
		this->multiply(1/(float)num);
	}
	inline void averageVectors(myVector vec1, myVector vec2)
	{
		x = (vec1.x + vec2.x) / 2;		
		y = (vec1.y + vec2.y) / 2;
		z = (vec1.z + vec2.z) / 2;
	}
	inline void calculateCrossProduct(myVector v1, myVector v2)
	{
		x = v1.y * v2.z - v1.z * v2.y;  
		y = v1.z * v2.x - v1.x * v2.z;
		z = v1.x * v2.y - v1.y * v2.x;
	}
	inline void calculateAngles(myVector v1)
	{//calculate angles of one vertex wrt origin
		x = atan2(v1.z,v1.y);
		y = atan2(v1.x,v1.z);
		z = atan2(v1.y,v1.x);
	}
	inline void calculateAngles(myVector v1, myVector v2)
	{//calculate angles of one vertex wrt another
		x = atan2(v1.z-v2.z,v1.y-v2.y);
		y = atan2(v1.x-v2.x,v1.z-v2.z);
		z = atan2(v1.y-v2.y,v1.x-v2.x);
	}
	static inline float calculateDotProduct(myVector v1, myVector v2)
	{
		float distance = (v1.x * v2.x) + (v1.y * v2.y) + (v1.z * v2.z);
		return distance;
	}
	static inline float calculateDistance(myVector v1, myVector v2)
	{//for calculating distance btw two vertices
		float distance = sqrt((v1.x - v2.x)*(v1.x - v2.x)+(v1.y - v2.y)*(v1.y - v2.y)+(v1.z - v2.z)*(v1.z - v2.z));
		return distance;
	}
	static inline float calculateDistance(myVector v1)
	{//for calculating distance to origin
		float distance = sqrt(v1.x * v1.x + v1.y * v1.y + v1.z * v1.z);
		return distance;
	}
	static inline myVector calculateReflectionVector(myVector vector, myVector normal)
	{
		//R = V - ( 2 * (V [dot] N) * N )
		float dotProduct;
		myVector reflection;
		
		dotProduct = calculateDotProduct(vector, normal);//dotProduct = (v [dot] N)
		normal.multiply(2*dotProduct);
		reflection.subtractAndSet(vector, normal);

		return reflection;
	}
	
	/*static inline struct myVector calculateVectoralDistance(Vertex v1, Vertex v2)
	{
		myVector distance;
		distance.x = sqrt(((v1.y - v2.y)*(v1.y - v2.y))+((v1.z - v2.z)*(v1.z - v2.z)));
		distance.y = sqrt(((v1.x - v2.x)*(v1.x - v2.x))+((v1.z - v2.z)*(v1.z - v2.z)));
		distance.z = sqrt(((v1.x - v2.x)*(v1.x - v2.x))+((v1.y - v2.y)*(v1.y - v2.y)));
		return distance;
	}*/
} Vertex,Force;

struct Triangle
{
	unsigned int corners[3];

	Triangle(){}

	Triangle(unsigned int a, unsigned int b, unsigned int c)
	{
		this->corners[0] = a;
		this->corners[1] = b;
		this->corners[2] = c;
	}
};

class Matrix
{
	public:
		Matrix(){};
		Matrix(myVector v1, myVector v2, myVector v3)
		{
			cell[0][0] = v1.x;
			cell[0][1] = v1.y;
			cell[0][2] = v1.z;
			cell[1][0] = v2.x;
			cell[1][1] = v2.y;
			cell[1][2] = v2.z;
			cell[2][0] = v3.x;
			cell[2][1] = v3.y;
			cell[2][2] = v3.z;			
		}
		~Matrix(){};

		inline void set(myVector v1, myVector v2, myVector v3)
		{
			cell[0][0] = v1.x;
			cell[0][1] = v1.y;
			cell[0][2] = v1.z;
			cell[1][0] = v2.x;
			cell[1][1] = v2.y;
			cell[1][2] = v2.z;
			cell[2][0] = v3.x;
			cell[2][1] = v3.y;
			cell[2][2] = v3.z;
		}
		inline void set(Matrix mat)
		{
			for(int i=0;i<3;i++)
				for(int j=0;j<3;j++)
					cell[i][j] = mat.cell[i][j];
		}
		inline float calculateDeterminant()
		{
			return   ((cell[0][0]*cell[1][1]*cell[2][2]) + (cell[2][0]*cell[0][1]*cell[1][2])
				   + (cell[1][0]*cell[2][1]*cell[0][2]) - (cell[0][2]*cell[1][1]*cell[2][0]) 
				   - (cell[1][2]*cell[2][1]*cell[0][0]) - (cell[2][2]*cell[0][1]*cell[1][0]));
		}
		inline void multiplyMatricesAndSet(Matrix m1, Matrix m2)
		{//multiplies two 3x3 matrices and sets result to the matrix
			for(int i=0;i<3;i++)
				for(int j=0;j<3;j++)
				{
					cell[i][j] = m1.cell[i][0]*m2.cell[0][j] + m1.cell[i][1]*m2.cell[1][j] + m1.cell[i][2]*m2.cell[2][j];
				}
		}
		inline void multiplyWithVectorAndSetVector(Vertex& position)
		{
			Vertex newPosition;
			newPosition.x = position.x*cell[0][0] + position.y*cell[1][0] + position.z*cell[2][0];
			newPosition.y = position.x*cell[0][1] + position.y*cell[1][1] + position.z*cell[2][1];
			newPosition.z = position.x*cell[0][2] + position.y*cell[1][2] + position.z*cell[2][2];
			position.set(newPosition);
		}
		inline void calculateInverseMatrixAndSet(Matrix mat)
		{
			float determinant;

			determinant = mat.calculateDeterminant();

			cell[0][0] = (mat.cell[1][1]*mat.cell[2][2] - mat.cell[1][2]*mat.cell[2][1]) / determinant;
			cell[0][1] = (mat.cell[0][2]*mat.cell[2][1] - mat.cell[0][1]*mat.cell[2][2]) / determinant;
			cell[0][2] = (mat.cell[0][1]*mat.cell[1][2] - mat.cell[0][2]*mat.cell[1][1]) / determinant;
			cell[1][0] = (mat.cell[1][2]*mat.cell[2][0] - mat.cell[1][0]*mat.cell[2][2]) / determinant;
			cell[1][1] = (mat.cell[0][0]*mat.cell[2][2] - mat.cell[0][2]*mat.cell[2][0]) / determinant;
			cell[1][2] = (mat.cell[0][2]*mat.cell[1][0] - mat.cell[0][0]*mat.cell[1][2]) / determinant;
			cell[2][0] = (mat.cell[1][0]*mat.cell[2][1] - mat.cell[1][1]*mat.cell[2][0]) / determinant;
			cell[2][1] = (mat.cell[0][1]*mat.cell[2][0] - mat.cell[0][0]*mat.cell[2][1]) / determinant;
			cell[2][2] = (mat.cell[0][0]*mat.cell[1][1] - mat.cell[0][1]*mat.cell[1][0]) / determinant;
		}

	private:
		float cell[3][3];

};

class Helper
{
	public:
		Helper();
		~Helper();

		static inline std::string stringify(double x)
		{
			std::ostringstream o;
			o << x;
			return o.str();
		}

		static inline std::string stringify(float x)
		{
			std::ostringstream o;
			o << x;
			return o.str();
		}

		static inline std::string stringify(int x)
		{
			std::ostringstream o;
			o << x;
			return o.str();
		}

		static inline int round(float num)
		{
			int temp;
			temp = (int)(num*10);
			temp %= 10;
			temp >= 5 ? num++ : num;
			return (int)num;
		}

		static inline float square(float num)
		{
			return num*num;
		}

		static inline float minimizeAngle(float val)
		{
			while(val >= 180.0)
				val -= 360.0;
			while(val <= -180.0)
				val += 360.0;
			return val;
		}
		static inline void drawRect(float size, float x, float y, float z)
		{
			//float size = 0.15;

			glBegin(GL_QUADS);
				glVertex3f(x-size, y+size, z);
				glVertex3f(x+size, y+size, z);
				glVertex3f(x+size, y-size, z);
				glVertex3f(x-size, y-size, z);
			glEnd();
		}
		static inline void drawRect(float size, myVector v1)
		{
			//float size = 0.15;

			glBegin(GL_QUADS);
				glVertex3f(v1.x-size, v1.y+size, v1.z);
				glVertex3f(v1.x+size, v1.y+size, v1.z);
				glVertex3f(v1.x+size, v1.y-size, v1.z);
				glVertex3f(v1.x-size, v1.y-size, v1.z);
			glEnd();
		}

		static inline void drawArrow()
		{
			glBegin(GL_TRIANGLES);
				glVertex3f(0.0, 0.0, 0.0);
				glVertex3f(0.0, 10.0, 0.0);
				glVertex3f(10.0, 0.0, 0.0);
			glEnd();
		}
		static inline float absoluteValue(float num)
		{
			return (num < 0 ? -num : num);
		}

		static inline void glTranslatefv(myVector vec)
		{
			glTranslatef(vec.x, vec.y, vec.z);
		}

		static inline float generateRandomNumber(int seed, int interval)
		{
			float temp;
			int halfInterval = interval/2;

			srand ( seed );
			while(1)
			{
				temp = rand() % interval;
				temp -= halfInterval;
				if(temp!=0)
					break;
				else 
					continue;
			}
			return temp;
		}

		static inline myVector generateRandomVector(int seed, int interval)
		{
			float temp[3];
			myVector returnVal;
			int halfInterval = interval/2;

			srand ( seed );
			for(int i=0;i<3;i++)
			{
				while(1)
				{
					temp[i] = rand() % interval;
					temp[i] -= halfInterval;
					if(temp[i]!=0)
						break;
					else 
						continue;
				}	
				
			}
			returnVal.set(temp[0]*0.001,temp[1]*0.001,temp[2]*0.001);
			return returnVal;
		}

		/*static inline int absoluteValue(int num)
		{
			return (num < 0.0 ? -num : num);
		}*/
};


#endif