from housebuilder import *
import pprint

m_w = 521288629
m_z = 36243606;
def resetRandom(seed):
    global m_w,m_z
    m_w = 521288629+seed
    m_z = 36243606+seed

def rand():
    global m_w,m_z
    m_z = 36969 * (m_z & 65535) + (m_z >> 16)
    m_w = 18000 * (m_w & 65535) + (m_w >> 16)
    return (((m_z << 16) + m_w) % 36243606 / 36243606.0)
def rand_choice(seq):
    return seq[int(rand() * len(seq))]



from Tkinter import *
import itertools
import math
def s(x):
    return math.tanh(x*2)

styleColors = ["#%02x%02x%02x" % (0, 255, 0),
               "#%02x%02x%02x" % (128, 128, 128),
               "#%02x%02x%02x" % (255, 0, 0),
               ]
styleColors = ["#%02x%02x%02x" % i for i in itertools.permutations([0,255,128],3)]

class Vis:
    def __init__(self,root,width,height):
        self.enable = False
        frame = Frame(root)
        frame2 = Frame(root)
        self.width = width
        self.height = height
        self.canvas = Canvas(frame2,bg="black",width=width,height=height)
        self.canvas.pack()
        
        self.size = 0
        self.seed = 0
        self.population = 0
        self.roads = 0
        self.public = 0
        self.cluster = 0
        self.residential = 0
        self.comercial = 0
        self.industrial = 0
        Label(frame2, text="Random Seed").pack()
        s = Scale(frame2, from_=0, to=100, resolution=1, orient=HORIZONTAL, length=width, command=self.reseed )
        s.set(55)
        s.pack()
        
        Button(frame2, command=self.draw,text="Redraw" ).pack()
        Button(frame2, command=self.save,text="Save file" ).pack()
        
        textfr=Frame(frame)
        self.text=Text(textfr,height=30,width=50)
        
        # put a scroll bar in the frame
        scroll=Scrollbar(textfr,command=self.text.yview)
        self.text.configure(yscrollcommand=scroll.set)
        
        #pack everything
        self.text.pack(side=LEFT)
        scroll.pack(side=RIGHT,fill=Y)
        textfr.pack(side=TOP)
        
        frame2.grid(row=0,column=0)
        frame.grid(row=0,column=1)
        frame2.update()
        frame.update()
        self.enable = True
    def resize(self,i):
        self.size = int(i)
        self.draw()
    def reseed(self,i):
        self.seed = int(i)
        self.draw()
    def repopulation(self,i):
        self.population = float(i)
        self.draw()
    def republic(self,i):
        self.public = float(i)
        self.draw()
    def reroads(self,i):
        self.roads = float(i)
        self.draw()
    def recluster(self,i):
        self.cluster = float(i)
        self.draw()
    def reresidential(self,i):
        self.residential = float(i)
        self.draw()
    def recomercial(self,i):
        self.comercial = float(i)
        self.draw()
    def reindustrial(self,i):
        self.industrial = float(i)
        self.draw()
    def rearrange(self):
        print self.size
    def rearrange2(self,i):
        print i
    def draw(self):
        draw_grid = False
        draw_tile_value = True
        if self.enable:
            self.b = Building()
            try:
                new_prod = eval(self.text.get(1.0, END))
                if isinstance(new_prod,dict):
                    self.b.productions = new_prod
                else:
                    print "Error, production rules not proper dict"
            except:
                print "Error, couldn't parse production rules"
                self.text.delete(1.0,END)
                self.text.insert(END, pprint.pformat(self.b.productions))

            self.b.plan(self.seed,self.size,self.population,self.public,self.roads,self.cluster)
            self.canvas.delete(ALL)
            if None: shape = Shape()
            for idx,shape in enumerate(self.b.shapes):
                prev = shape.points[0]
                #Plane
                for pt in shape.points[1:]+[prev]:
                    self.draw_line([prev[0]*self.width/2+self.width/2,prev[1]*self.height/2+self.height/2],[pt[0]*self.width/2+self.width/2,pt[1]*self.height/2+self.height/2],styleColors[idx%len(styleColors)])
                    prev = pt
                #Height
                w = max([i[0] for i in shape.points])-min([i[0] for i in shape.points])
                h = self.height - (shape.height*self.height/10)
                self.draw_line([0,h], [self.width/10*w,h], styleColors[idx%len(styleColors)])
                
    def save(self):
        if self.b and self.b.builder:
            self.b.builder.save('new_building.obj')      
    def draw_tile(self,x,y,w,color):  
        self.canvas.create_rectangle(x,y,x+w,y+w,fill=color) 
    def draw_line(self,p1,p2,color):
        self.canvas.create_line(p1,p2,fill=color)
    def draw_point(self,p):
        self.canvas.create_oval(p[0]-2,p[1]-2,p[0]+3,p[1]+3,fill='white',state=DISABLED)
    def draw_label(self,p,w,s):
        if isinstance(s,float):
            s = "-"#s = "%.2f"%s
        if isinstance(s,int):
            s = "%d"%s
        self.canvas.create_text(p[0]+w/2,p[1]+w/2,text = s,font=("Sans", 8),fill = "white")
        

# show it
root = Tk()

vis = Vis(root,400,400)
vis.draw()
root.mainloop()
