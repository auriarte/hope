

#include "DemoGUI.h"
#include <sstream>

//using namespace std;

DemoGUI* DemoGUI::Instance()
{
    static DemoGUI instance;
    return &instance;
}

DemoGUI::DemoGUI() {
    cameraDistance = 10.0f;
    cameraPitch = 0.0f;
    cameraYaw = 0.0f;
}

void DemoGUI::HandleInput( std::list<action_t*> actions ) {
    while ( !actions.empty() ) {
        action_t * action = actions.front();
        actions.pop_front();
        switch( action->action ){
            case TOGGLE_GUI:
                GameStateS::Instance()->showGUI = !GameStateS::Instance()->showGUI;
                break;
            case SPECIAL_ESCAPE:
                GameStateS::Instance()->state = GS_FSM_RENDER;
                break;
            case WHEEL_UP:
                if (!GameStateS::Instance()->ui.mouseOverMenu) cameraDistance += 0.1f;
                break;
            case WHEEL_DOWN:
                if (!GameStateS::Instance()->ui.mouseOverMenu) cameraDistance -= 0.1f;
                break;
            case AIM:
                cameraYaw += action->x;
                cameraPitch += action->y;
                if (cameraPitch < -90) cameraPitch = -90;
                if (cameraPitch > 90) cameraPitch = 90;
                break;
            case TRANSLATION:
                GameStateS::Instance()->speed.x = action->x;
                GameStateS::Instance()->speed.y = action->y;
                break;

            default:
                break;
                
        }
        delete action;
    }
    
}

void DemoGUI::HandleCollisions( std::list<collision_t*> collisions ) {
    
}

void DemoGUI::Update() {   

}

void DemoGUI::Render() {

    // Update and render
    glViewport(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
    glClearColor(0.3f, 0.3f, 0.32f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glDisable(GL_TEXTURE_2D);
    
    // set perspective viewing frustum
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(60.0f, (float)SCREEN_WIDTH/(float)SCREEN_HEIGHT, 1.0f, 1000.0f); // FOV, AspectRatio, NearClip, FarClip
    
    // switch to modelview matrix in order to set scene
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    
    // tramsform camera
    glTranslatef(0, 0, -cameraDistance);
    glRotatef(cameraPitch, 1, 0, 0);
    glRotatef(cameraYaw, 0, 1, 0);
    
    
    draw1();
    draw2();
}

void DemoGUI::RenderGUI(GUI* gui) {
    static const char* samples[] = { "Test 1", "Test 2 bla", "Test 3"};
    std::string text = "Camera Distance: ";// + static_cast<std::ostringstream*>( &(std::ostringstream() << cameraDistance) )->str();;
    gui->DrawText( 280, SCREEN_HEIGHT-32-20, GUI::ALIGN_LEFT, text.c_str(), GUI::RGBA(255,255,255,128) );
    
    gui->BeginScrollArea( NULL, 10, SCREEN_HEIGHT-32-10-250, 250, 250, 
                          &GameStateS::Instance()->sampleScroll1 );
    
    if (gui->Check("Show Area 2", GameStateS::Instance()->showArea2))
        GameStateS::Instance()->showArea2 = !GameStateS::Instance()->showArea2;
    if (gui->Button("Add Label"))
        GameStateS::Instance()->numLabels++;
    for (int i = 0; i < GameStateS::Instance()->numLabels; ++i) { 
        gui->Label(samples[i%3]); 
    }
    
    gui->SeparatorLine();
    
    if (gui->Collapse("Options Sample", 0, GameStateS::Instance()->optionsExpanded))
        GameStateS::Instance()->optionsExpanded = !GameStateS::Instance()->optionsExpanded;
    
    if (GameStateS::Instance()->optionsExpanded) {
        gui->Indent();
        
        gui->Label("Sample");
        gui->Label("Sample");
        gui->Label("Sample");
        
        gui->Unindent();
    }
    
    gui->SeparatorLine();
    
    if (gui->Slider("Slider sample", &GameStateS::Instance()->sliderValue, 0.0f, 100.0f, 1.0f)) {
        // slider value was updated
    }
    
    gui->EndScrollArea();
    
    if (GameStateS::Instance()->showArea2) {
        gui->BeginScrollArea( "Sample Area 2", 10, SCREEN_HEIGHT-32-10-250-10-250, 250, 250, 
                              &GameStateS::Instance()->sampleScroll2 );
        gui->Label("Sample");
        gui->Label("Sample");                
        gui->Label("Sample");                                
        gui->Label("Sample");                                                
        gui->Label("Sample");                                                                
        gui->Label("Sample");                                                                                
        gui->Label("Sample");                                                                                                
        gui->Label("Sample");                                                                                                                
        gui->Label("Sample");                                                                                                                                
        gui->Label("Sample");                                                                                                                                                
        gui->Label("Sample");                                                                                                                                                                
        gui->Label("Sample");                                                                                                                                                                                
        gui->Label("Sample");                                                                                                                                                                                                
        gui->Label("Sample");                                                                                                                                                                                                                
        gui->Label("Sample");                                                                                                                                                                                                                                
        gui->EndScrollArea();
    }
}

// cube ///////////////////////////////////////////////////////////////////////
//    v6----- v5
//   /|      /|
//  v1------v0|
//  | |     | |
//  | |v7---|-|v4
//  |/      |/
//  v2------v3

// vertex array for glDrawElements() and glDrawRangeElement() =================
// Notice that the sizes of these arrays become samller than the arrays for
// glDrawArrays() because glDrawElements() uses an additional index array to
// choose designated vertices with the indices. The size of vertex array is now
// 24 instead of 36, but the index array size is 36, same as the number of
// vertices required to draw a cube.
GLfloat vertices2[] = { 1, 1, 1,  -1, 1, 1,  -1,-1, 1,   1,-1, 1,   // v0,v1,v2,v3 (front)
                        1, 1, 1,   1,-1, 1,   1,-1,-1,   1, 1,-1,   // v0,v3,v4,v5 (right)
                        1, 1, 1,   1, 1,-1,  -1, 1,-1,  -1, 1, 1,   // v0,v5,v6,v1 (top)
                        -1, 1, 1,  -1, 1,-1,  -1,-1,-1,  -1,-1, 1,   // v1,v6,v7,v2 (left)
                        -1,-1,-1,   1,-1,-1,   1,-1, 1,  -1,-1, 1,   // v7,v4,v3,v2 (bottom)
                        1,-1,-1,  -1,-1,-1,  -1, 1,-1,   1, 1,-1 }; // v4,v7,v6,v5 (back)

// normal array
GLfloat normals2[]  = { 0, 0, 1,   0, 0, 1,   0, 0, 1,   0, 0, 1,   // v0,v1,v2,v3 (front)
                        1, 0, 0,   1, 0, 0,   1, 0, 0,   1, 0, 0,   // v0,v3,v4,v5 (right)
                        0, 1, 0,   0, 1, 0,   0, 1, 0,   0, 1, 0,   // v0,v5,v6,v1 (top)
                        -1, 0, 0,  -1, 0, 0,  -1, 0, 0,  -1, 0, 0,   // v1,v6,v7,v2 (left)
                        0,-1, 0,   0,-1, 0,   0,-1, 0,   0,-1, 0,   // v7,v4,v3,v2 (bottom)
                        0, 0,-1,   0, 0,-1,   0, 0,-1,   0, 0,-1 }; // v4,v7,v6,v5 (back)

// color array
GLfloat colors2[]   = { 1, 1, 1,   1, 1, 0,   1, 0, 0,   1, 0, 1,   // v0,v1,v2,v3 (front)
                        1, 1, 1,   1, 0, 1,   0, 0, 1,   0, 1, 1,   // v0,v3,v4,v5 (right)
                        1, 1, 1,   0, 1, 1,   0, 1, 0,   1, 1, 0,   // v0,v5,v6,v1 (top)
                        1, 1, 0,   0, 1, 0,   0, 0, 0,   1, 0, 0,   // v1,v6,v7,v2 (left)
                        0, 0, 0,   0, 0, 1,   1, 0, 1,   1, 0, 0,   // v7,v4,v3,v2 (bottom)
                        0, 0, 1,   0, 0, 0,   0, 1, 0,   0, 1, 1 }; // v4,v7,v6,v5 (back)

// index array of vertex array for glDrawElements() & glDrawRangeElement()
GLubyte indices[]  = { 0, 1, 2,   2, 3, 0,      // front
                        4, 5, 6,   6, 7, 4,      // right
                        8, 9,10,  10,11, 8,      // top
                        12,13,14,  14,15,12,      // left
                        16,17,18,  18,19,16,      // bottom
                        20,21,22,  22,23,20 };    // back

///////////////////////////////////////////////////////////////////////////////
// draw 1: immediate mode
// 78 calls = 36 glVertex*() calls + 36 glColor*() calls + 6 glNormal*() calls
///////////////////////////////////////////////////////////////////////////////
void DemoGUI::draw1()
{
    glPushMatrix();
    glTranslatef(-2, -2, 0); // move to bottom-left corner
    glBegin(GL_TRIANGLES);
    // front faces
    glNormal3f(0,0,1);
    // face v0-v1-v2
    glColor3f(1,1,1);
    glVertex3f(1,1,1);
    glColor3f(1,1,0);
    glVertex3f(-1,1,1);
    glColor3f(1,0,0);
    glVertex3f(-1,-1,1);
    // face v2-v3-v0
    glColor3f(1,0,0);
    glVertex3f(-1,-1,1);
    glColor3f(1,0,1);
    glVertex3f(1,-1,1);
    glColor3f(1,1,1);
    glVertex3f(1,1,1);
    
    // right faces
    glNormal3f(1,0,0);
    // face v0-v3-v4
    glColor3f(1,1,1);
    glVertex3f(1,1,1);
    glColor3f(1,0,1);
    glVertex3f(1,-1,1);
    glColor3f(0,0,1);
    glVertex3f(1,-1,-1);
    // face v4-v5-v0
    glColor3f(0,0,1);
    glVertex3f(1,-1,-1);
    glColor3f(0,1,1);
    glVertex3f(1,1,-1);
    glColor3f(1,1,1);
    glVertex3f(1,1,1);
    
    // top faces
    glNormal3f(0,1,0);
    // face v0-v5-v6
    glColor3f(1,1,1);
    glVertex3f(1,1,1);
    glColor3f(0,1,1);
    glVertex3f(1,1,-1);
    glColor3f(0,1,0);
    glVertex3f(-1,1,-1);
    // face v6-v1-v0
    glColor3f(0,1,0);
    glVertex3f(-1,1,-1);
    glColor3f(1,1,0);
    glVertex3f(-1,1,1);
    glColor3f(1,1,1);
    glVertex3f(1,1,1);
    
    // left faces
    glNormal3f(-1,0,0);
    // face  v1-v6-v7
    glColor3f(1,1,0);
    glVertex3f(-1,1,1);
    glColor3f(0,1,0);
    glVertex3f(-1,1,-1);
    glColor3f(0,0,0);
    glVertex3f(-1,-1,-1);
    // face v7-v2-v1
    glColor3f(0,0,0);
    glVertex3f(-1,-1,-1);
    glColor3f(1,0,0);
    glVertex3f(-1,-1,1);
    glColor3f(1,1,0);
    glVertex3f(-1,1,1);
    
    // bottom faces
    glNormal3f(0,-1,0);
    // face v7-v4-v3
    glColor3f(0,0,0);
    glVertex3f(-1,-1,-1);
    glColor3f(0,0,1);
    glVertex3f(1,-1,-1);
    glColor3f(1,0,1);
    glVertex3f(1,-1,1);
    // face v3-v2-v7
    glColor3f(1,0,1);
    glVertex3f(1,-1,1);
    glColor3f(1,0,0);
    glVertex3f(-1,-1,1);
    glColor3f(0,0,0);
    glVertex3f(-1,-1,-1);
    
    // back faces
    glNormal3f(0,0,-1);
    // face v4-v7-v6
    glColor3f(0,0,1);
    glVertex3f(1,-1,-1);
    glColor3f(0,0,0);
    glVertex3f(-1,-1,-1);
    glColor3f(0,1,0);
    glVertex3f(-1,1,-1);
    // face v6-v5-v4
    glColor3f(0,1,0);
    glVertex3f(-1,1,-1);
    glColor3f(0,1,1);
    glVertex3f(1,1,-1);
    glColor3f(0,0,1);
    glVertex3f(1,-1,-1);
    glEnd();
    
    glPopMatrix();
}


///////////////////////////////////////////////////////////////////////////////
// draw cube at bottom-left corner with glDrawElements
// The main advantage of glDrawElements() over glDrawArray() is that
// glDrawElements() allows hopping around the vertex array with the associated
// index values.
// In a cube, the number of vertex data in the vertex array can be reduced to
// 24 vertices for glDrawElements().
// Note that you need an additional array (index array) to store how to traverse
// the vertext data. For a cube, we need 36 entries in the index array.
///////////////////////////////////////////////////////////////////////////////
void DemoGUI::draw2()
{
    // enable and specify pointers to vertex arrays
    glEnableClientState(GL_NORMAL_ARRAY);
//    glEnableClientState(GL_COLOR_ARRAY);
    glEnableClientState(GL_VERTEX_ARRAY);
    glNormalPointer(GL_FLOAT, 0, normals2);
//    glColorPointer(3, GL_FLOAT, 0, colors2);
    glVertexPointer(3, GL_FLOAT, 0, vertices2);
    
    glPushMatrix();
    glTranslatef(2, -2, 0);                 // move to bottom-right
    
    glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_BYTE, indices);
    
    glPopMatrix();
    
    glDisableClientState(GL_VERTEX_ARRAY);  // disable vertex arrays
//    glDisableClientState(GL_COLOR_ARRAY);
    glDisableClientState(GL_NORMAL_ARRAY);
}