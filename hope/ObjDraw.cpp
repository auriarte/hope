//
//  ObjDraw.cpp
//  hope
//
//  Created by Ehsan Khosroshahi on 3/12/13.
//
//

#include "ObjDraw.h"



//-----------------------------------------------------------------------------
// Constants.
//-----------------------------------------------------------------------------

#define APP_TITLE "OpenGL OBJ Viewer"

// Windows Vista compositing support.
#if !defined(PFD_SUPPORT_COMPOSITION)
#define PFD_SUPPORT_COMPOSITION 0x00008000
#endif

// GL_EXT_texture_filter_anisotropic
#define GL_TEXTURE_MAX_ANISOTROPY_EXT     0x84FE
#define GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT 0x84FF





//  TODO we dont have any shader yet




ObjDraw::ObjDraw()
{
    
}

void ObjDraw::Draw()
{
    
    
      //  SetProcessorAffinity();
        
    
            
        DrawFrame();
        
    
        
       // Cleanup();

}



void ObjDraw::Cleanup()
{
    UnloadModel();
    
    if (g_nullTexture)
    {
        glDeleteTextures(1, &g_nullTexture);
        g_nullTexture = 0;
    }
    
}



GLuint ObjDraw::CreateNullTexture(int width, int height)
{
    // Create an empty white texture. This texture is applied to OBJ models
    // that don't have any texture maps. This trick allows the same shader to
    // be used to draw the OBJ model with and without textures applied.
    
    int pitch = ((width * 32 + 31) & ~31) >> 3; // align to 4-byte boundaries
    std::vector<GLubyte> pixels(pitch * height, 255);
    GLuint texture = 0;
    
    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_2D, texture);
    
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, width, height, 0, GL_BGRA,
                 GL_UNSIGNED_BYTE, &pixels[0]);
    
    return texture;
}

void ObjDraw::DrawFrame()
{

    const ModelOBJ::Mesh *pMesh = 0;
    const ModelOBJ::Material *pMaterial = 0;
    const ModelOBJ::Vertex *pVertices = 0;
    ModelTextures::const_iterator iter;
    
    for (int i = 0; i < g_model.getNumberOfMeshes(); ++i)
    {
        pMesh = &g_model.getMesh(i);
        pMaterial = pMesh->pMaterial;
        pVertices = g_model.getVertexBuffer();
        
        // Disable color materials, so that glMaterial calls work
        // it should be enabled at the end
        glDisable(GL_COLOR_MATERIAL);
        
        
        glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, pMaterial->ambient);
        glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, pMaterial->diffuse);
        glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, pMaterial->specular);
        glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, pMaterial->shininess * 128.0f);
        
        if (g_enableTextures)
        {
            iter = g_modelTextures.find(pMaterial->colorMapFilename);
            
            if (iter == g_modelTextures.end())
            {
                glDisable(GL_TEXTURE_2D);
            }
            else
            {
                glEnable(GL_TEXTURE_2D);
                glBindTexture(GL_TEXTURE_2D, iter->second);
            }
        }
        else
        {
            glDisable(GL_TEXTURE_2D);
        }
        
        if (g_model.hasPositions())
        {
            glEnableClientState(GL_VERTEX_ARRAY);
            glVertexPointer(3, GL_FLOAT, g_model.getVertexSize(),
                            g_model.getVertexBuffer()->position);
        }
        
        if (g_model.hasTextureCoords())
        {
            glEnableClientState(GL_TEXTURE_COORD_ARRAY);
            glTexCoordPointer(2, GL_FLOAT, g_model.getVertexSize(),
                              g_model.getVertexBuffer()->texCoord);
        }
        
        if (g_model.hasNormals())
        {
            glEnableClientState(GL_NORMAL_ARRAY);
            glNormalPointer(GL_FLOAT, g_model.getVertexSize(),
                            g_model.getVertexBuffer()->normal);
        }
        
        
        
        glDrawElements(GL_TRIANGLES, pMesh->triangleCount * 3, GL_UNSIGNED_INT,
                       g_model.getIndexBuffer() + pMesh->startIndex);
        
        glDisableClientState(GL_COLOR_ARRAY);
        if (g_model.hasNormals())
            glDisableClientState(GL_NORMAL_ARRAY);
        
        if (g_model.hasTextureCoords())
            glDisableClientState(GL_TEXTURE_COORD_ARRAY);
        
        if (g_model.hasPositions())
            glDisableClientState(GL_VERTEX_ARRAY);
    }
    
    glEnable(GL_COLOR_MATERIAL);

}


void ObjDraw::InitApp(const char *pszFilename)
{
    glEnable(GL_TEXTURE_2D);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    
    glActiveTexture(GL_TEXTURE1);
    glEnable(GL_TEXTURE_2D);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    
    glActiveTexture(GL_TEXTURE0);
    glEnable(GL_TEXTURE_2D);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    
    
    
    
        LoadModel(pszFilename);
    
}




void ObjDraw::LoadModel(const char *pszFilename)
{
    
    if (!g_model.import(pszFilename))
    {
        throw std::runtime_error("Failed to load model.");
    }
    
    //g_model.normalize();
    
    
    // these are for testing the data
    
    //    std::cout<<g_model.getNumberOfMaterials();
    //    std::cout<<g_model.getcolorfile()<< std::endl;
    //    std::cout<<"number of meshes : "<<g_model.getNumberOfMeshes()<< std::endl;
    //    std::cout<<"has a texture coordinate : "<<g_model.hasTextureCoords()<< std::endl;
    //    std::cout<<"number of the attributes : "<<g_model.getNumberOfAttributes()<< std::endl;
    //
    //    std::cout<<"vertex bufer : " << g_model.getVertexBuffer() << std::endl;
    
    // Load any associated textures.
    // Note the path where the textures are assumed to be located.
    
    const ModelOBJ::Material *pMaterial = 0;
 //   GLuint textureId = 0;
 //   std::string::size_type offset = 0;
    std::string filename;
    
    for (int i = 0; i < g_model.getNumberOfMaterials(); ++i)
    {
        pMaterial = &g_model.getMaterial(i);
        
        // Look for and load any diffuse color map textures.
        
        if (pMaterial->colorMapFilename.empty())
            continue;
        
    // TODO if there is file for colormap or the texture as bitmap
        
        
    }
    
    
 
    

}


//GLuint ObjDraw::LoadTexture(const char *pszFilename)
//{
//    GLuint id = 0;
//    Bitmap bitmap;
//    
//    if (bitmap.loadPicture(pszFilename))
//    {
//        // The Bitmap class loads images and orients them top-down.
//        // OpenGL expects bitmap images to be oriented bottom-up.
//        bitmap.flipVertical();
//        
//        glGenTextures(1, &id);
//        glBindTexture(GL_TEXTURE_2D, id);
//        
//        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
//        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
//        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
//        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
//        
//        if (g_maxAnisotrophy > 1.0f)
//        {
//            glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT,
//                            g_maxAnisotrophy);
//        }
//        
//        gluBuild2DMipmaps(GL_TEXTURE_2D, 4, bitmap.width, bitmap.height,
//                          GL_BGRA_EXT, GL_UNSIGNED_BYTE, bitmap.getPixels());
//    }
//    
//    return id;
//}






void ObjDraw::UnloadModel()
{
 
    ModelTextures::iterator i = g_modelTextures.begin();
    
    while (i != g_modelTextures.end())
    {
        glDeleteTextures(1, &i->second);
        ++i;
    }
    
    g_modelTextures.clear();
    g_model.destroy();
    
 
}



