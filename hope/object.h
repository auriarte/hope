#ifndef _OBJECT_H
#define _OBJECT_H

#include "model.h"
#include "helper.h"

class Object
{
	private:
		Model *model;
		int type;
		myVector translation, scaling, rotation;
		int glListId;
		

	public:
		Object();
		Object(Model *_model, int type);
		Object(Model *_model, int type, myVector &_translation, myVector &_rotation, myVector &_scaling);

		void translate(float x, float y, float z){translation.add(x,y,z);};
		void rotate(float x, float y, float z){rotation.add(x,y,z);};
		void scale(float x, float y, float z){scaling.add(x,y,z);};
		void setModel(Model *_model){model = _model;};
		void reset();
		inline void draw(){	model->draw();};
    void transformObject();

	friend class Core;

};


#endif 