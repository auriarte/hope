#ifndef __hope__CollisionWorld__
#define __hope__CollisionWorld__

#include <iostream>
#include "Actor.h"
#include "vector2.h"
#include "CollisionBody.h"
#include <list>


struct collision_t {
    Actor *         actor;
    CollisionBody * body;
};

class CollisionWorld {
public:
    CollisionWorld();
    ~CollisionWorld();
    
    std::list<collision_t*>     GetCollisions();
    void                        AddBody( CollisionBody body );
    void                        RemoveBody( CollisionBody body );
    std::list<CollisionBody*>   bodies;
};

#endif /* defined(__hope__CollisionWorld__) */
