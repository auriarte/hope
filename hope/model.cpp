#include "model.h"

Model::Model()
{
	textureFlag = true;
	lightingFlag = true;

	numberOfVertices = 0;
	numberOfTriangles = 0;
	numberOfTexels = 0;
	numberOfNormals = 0;
}

Model::Model(std::string modelName)
{
	textureFlag = false;
	lightingFlag = false;

	numberOfVertices = 0;
	numberOfTriangles = 0;
	numberOfTexels = 0;
	numberOfNormals = 0;

	loadModel(modelName);
}

Model::~Model()
{}

void Model::clear()
{
	vertices.clear();
	normals.clear();
	vertexTextures.clear();
	faces.clear();
}

bool Model::loadModel(std::string modelName)
{
	size_t	location;
	bool	returnVal = false;
	std::string texturePath;
	
	location = modelName.find_last_of(".");
	
	if(modelName.substr(location+1).compare("obj") == 0)
		returnVal = load(modelName);

	/*for(int i=0; i<textureFileName.size(); i++)
	{
		//extract texturePath from modelPath
		size_t pos;
		pos = modelName.find_last_of("/\\");
		texturePath.assign(modelName.substr(0,pos+1));

		texturePath.append(textureFileName[i]);

		initializeTexture(texturePath, i);
	}*/

	return returnVal;//load failed or succeeded
}

bool Model::load(std::string modelName)
{
	std::ifstream file;  
	
	float			x,y,z;
	int				a,b,c;
	string			str;
//	size_t			pos;

	file.open(modelName.c_str());
	
	if(file.fail())
		return false;
	
	std::string line; 
	std::istringstream is;

	//std::getline(file, line);
	//is.str(line);
	//is >> textureCount;

	/*for(int i=0; i<textureCount; i++)
	{
		is.clear();
		std::getline(file, line);
		is.str(line);
		is >> temp_textureFile;
		textureFileName.push_back(temp_textureFile);
	}*/


	while(1)
	{
		std::getline(file, line);
		if(line.empty())
			break;

		is.clear();
		is.str(line);		

		is >> str;		

		if(str.compare("#") == 0)
			continue;
		else if(str.compare("v") == 0 || str.compare("vt") == 0 || str.compare("n") == 0 )
		{
			is >> x;  
			is >> y;
			is >> z;

			if(str.compare("v") == 0 )//vertex
			{
				vertices.push_back(Vertex(x,y,z));
				numberOfVertices++;
			}
			else if(str.compare("vt") == 0 )
			{
				vertexTextures.push_back(TxtCoordinate(x,y));
				numberOfTexels++;
			}

			else if(str.compare("n") == 0)//normal
			{
				normals.push_back(Vertex(x,y,z));
				numberOfNormals++;
			}
		}
		else if(str.compare("f") == 0)//face
		{
			is >> a;
			is >> b;
			is >> c;

			faces.push_back(Triangle(a-1,b-1,c-1));
			numberOfTriangles++;
		}
		else
			continue;
	}

	file.close();

	return true;//load completed	
}




void Model::draw()
{
	glEnableClientState(GL_VERTEX_ARRAY);
	if(numberOfNormals>0)
		glEnableClientState(GL_NORMAL_ARRAY);//enable when obj file is initialized
	//glEnableClientState(GL_TEXTURE_COORD_ARRAY);

	glVertexPointer(3, GL_FLOAT, 0, &vertices[0]);
	if(numberOfNormals>0)
		glNormalPointer(GL_FLOAT, 0, &normals[0]);
	//glTexCoordPointer(2, GL_FLOAT, 0, &vertexTextures);

	if(lightingFlag)//we can turn on the lights after that point
		glEnable(GL_LIGHTING);

	glColor3f(0.0,1.0,1.0);
	glPointSize(1.0);	
	//Bu kisim kafa modelini ciziyor
	if(textureFlag == true)
	{
		glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D, textureId);
	}
	/*for(int i=0;i<headModel.numberOfTriangles;i++)//glDrawElements yerine bu kisim kullanilinca frame rate 110 dan 50 lere dusuyor
	{//burayi kesinlikle acma, ibret olsun diye dursun
		glBegin(GL_TRIANGLES);
		glArrayElement(headModel.indices[i].corners[0]);
		glArrayElement(headModel.indices[i].corners[1]);
		glArrayElement(headModel.indices[i].corners[2]);
		glEnd();
	}*/
	glDrawElements(GL_TRIANGLES, numberOfTriangles * 3, GL_UNSIGNED_INT, &faces[0]);
	glFlush();
	if(textureFlag == true)
		glDisable(GL_TEXTURE_2D);

	if(numberOfNormals>0)
		glDisableClientState(GL_NORMAL_ARRAY);
	glDisableClientState(GL_VERTEX_ARRAY);
	//glDisableClientState(GL_TEXTURE_COORD_ARRAY);
}

void Model::draw2()
{//this is going to be fixed
	glEnableClientState(GL_VERTEX_ARRAY);
	glEnableClientState(GL_NORMAL_ARRAY);//enable when obj file is initialized
	//glEnableClientState(GL_TEXTURE_COORD_ARRAY);

	glVertexPointer(3, GL_FLOAT, 0, &vertices[0]);
	glNormalPointer(GL_FLOAT, 0, &normals[0]);
	//glTexCoordPointer(2, GL_FLOAT, 0, &vertexTextures[0]);

	if(lightingFlag)//we can turn on the lights after that point
		glEnable(GL_LIGHTING);

	glColor3f(1.0,1.0,1.0);
	glPointSize(1.0);	
	//Bu kisim kafa modelini ciziyor
	if(textureFlag == true)
	{
		glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D, textureId);
	}
	for(int i=0;i<numberOfTriangles;i++)//glDrawElements yerine bu kisim kullanilinca frame rate 110 dan 50 lere dusuyor
	{
		glBegin(GL_TRIANGLES);
		glTexCoord2f(vertexTextures[textureIndices[i].corners[0]].coord[0], vertexTextures[textureIndices[i].corners[0]].coord[1]);
		//glNormal3f(normals[normalIndices[i].corners[0]]);
		glArrayElement(faces[i].corners[0]);
		glTexCoord2f(vertexTextures[textureIndices[i].corners[1]].coord[0], vertexTextures[textureIndices[i].corners[1]].coord[1]);
		glArrayElement(faces[i].corners[1]);
		glTexCoord2f(vertexTextures[textureIndices[i].corners[2]].coord[0], vertexTextures[textureIndices[i].corners[2]].coord[1]);
		glArrayElement(faces[i].corners[2]);
		glEnd();
	}
	//glDrawElements(GL_TRIANGLES, numberOfTriangles * 3, GL_UNSIGNED_INT, &indices[0]);
	glFlush();
	if(textureFlag == true)
		glDisable(GL_TEXTURE_2D);

	glDisableClientState(GL_NORMAL_ARRAY);
	glDisableClientState(GL_VERTEX_ARRAY);
	//glDisableClientState(GL_TEXTURE_COORD_ARRAY);
}

void Model::initializeTexture(std::string path, int id)
{
	GLuint temp_textureId;
	glGenTextures(1,&temp_textureId);
	
	textureId = temp_textureId;
	
//	int a = loadTGA(path.c_str(), temp_textureId);
}