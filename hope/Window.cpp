#include "Window.h"
#include "model_obj.h"

Window::Window( int screenWidth, int screenHeight, int screenBpp ) {
    
    // initialize SDL
    // TODO: check why SDL_INIT_EVERYTHING instead of SDL_INIT_VIDEO
    if( SDL_Init( SDL_INIT_EVERYTHING ) < 0 ) {
        printf( "Failed to initialize SDL. [%s]\n", SDL_GetError() );
        exit(1);
    }
    
    // set desired OpenGL attributes prior to selecting a video mode
	SDL_GL_SetAttribute( SDL_GL_DEPTH_SIZE, 16 );  // use 16 bits for depth buffer
	SDL_GL_SetAttribute( SDL_GL_DOUBLEBUFFER, 1 ); // enable double buffering
    SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_BUFFER_SIZE, 32);
    
    // create an OpenGL surface/window
    if( SDL_SetVideoMode( screenWidth, screenHeight, screenBpp, SDL_OPENGL ) == NULL ) {
        printf( "Failed to set video mode. [%s]\n", SDL_GetError() );
        exit(1);
    }
    
    //Enable unicode
    SDL_EnableUNICODE( SDL_TRUE );
    
    //Initialize OpenGL
    initGL();
    
    //Set caption
    SDL_WM_SetCaption( "Hope Engine 0.1", NULL );
    _screenWidth = screenWidth;
    _screenHeight = screenHeight;
    
//    const GLubyte *renderer = glGetString( GL_RENDERER );
//    const GLubyte *vendor = glGetString( GL_VENDOR );
//    const GLubyte *version = glGetString( GL_VERSION );
//    const GLubyte *glslVersion = glGetString( GL_SHADING_LANGUAGE_VERSION );
    
//    printf("GL Vendor : %s\n", vendor);
//    printf("GL Renderer : %s\n", renderer);
//    printf("GL Version (string) : %s\n", version);
//    printf("GLSL Version : %s\n", glslVersion);

}

void Window::initGL() {
    glShadeModel(GL_SMOOTH);                    // shading mathod: GL_SMOOTH or GL_FLAT
    glPixelStorei(GL_UNPACK_ALIGNMENT, 4);      // 4-byte pixel alignment
    
    // enable /disable features
    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
    //glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);
    //glHint(GL_POLYGON_SMOOTH_HINT, GL_NICEST);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_LIGHTING);
    glEnable(GL_TEXTURE_2D);
    glEnable(GL_CULL_FACE);
    
    // track material ambient and diffuse from surface color, call it before glEnable(GL_COLOR_MATERIAL)
    glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE);
    glEnable(GL_COLOR_MATERIAL);
    
    glClearColor(0, 0, 0, 0);                   // background color
    glClearStencil(0);                          // clear stencil buffer
    glClearDepth(1.0f);                         // 0 is near, 1 is far
    glDepthFunc(GL_LEQUAL);
    
    // init lights ************
    // set up light colors (ambient, diffuse, specular)
    GLfloat lightKa[] = {.2f, .2f, .2f, 1.0f};  // ambient light
    GLfloat lightKd[] = {.7f, .7f, .7f, 1.0f};  // diffuse light
    GLfloat lightKs[] = {1, 1, 1, 1};           // specular light
    glLightfv(GL_LIGHT0, GL_AMBIENT, lightKa);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, lightKd);
    glLightfv(GL_LIGHT0, GL_SPECULAR, lightKs);
    
    // position the light
    float lightPos[4] = {0, 0, 20, 1}; // positional light
    glLightfv(GL_LIGHT0, GL_POSITION, lightPos);
    
    glEnable(GL_LIGHT0);                        // MUST enable each light source after configuration
    
    // check for error
    GLenum error = glGetError();
    if( error != GL_NO_ERROR ) {
        printf( "Failed to initialize OpenGL. [%s]\n", gluErrorString( error ) );
        exit(1);
    }
}

Window::~Window() {
    SDL_Quit();
}

void Window::DrawBox( float x, float y, float w, float h, mapTileStyle_t color ) {
    switch (color) {
        case MAP_BLOCK_COMERCIAL:
            glColor4ub(150, 0, 0, 255);
            break;
        case MAP_BLOCK_INDUSTRIAL:
            glColor4ub(0, 150, 0, 255);
            break;
        case MAP_BLOCK_RESIDENTIAL:
            glColor4ub(0, 0, 150, 255);
        default:
            break;
    }
    glBegin( GL_QUADS );
    glVertex2f( x, y );
    glVertex2f( x + w, y );
    glVertex2f( x + w, y + h );
    glVertex2f( x , y + h );
    glEnd();
}

void Window::RenderGameMap2() {
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    
    glTranslatef(0.0, 0.0, -6.0);
    int w = GameStateS::Instance()->map->w;
    int h = GameStateS::Instance()->map->h;
    MapTile ** tile = GameStateS::Instance()->map->tiles;
    float tile_w = 1.0f/(float)w;
    float tile_h = 1.0f/(float)h;
    for(int y=0;y<h;y++){
        for(int x=0;x<w;x++){
            switch((*tile)->type){
                case MAP_BLOCK:
                    this->DrawBox( (float)x*tile_w-0.5f, (float)y*tile_h-0.5f, tile_w, tile_h, (*tile)->style);
                    break;
                case MAP_EMPTY:
                default:
                    break;
            }
            tile++;
        }
    }
}






