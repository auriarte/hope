#include "MapTile.h"
#include "SDL/SDL.h"
#include "SDL/SDL_opengl.h"
#include "SDL_image.h"
#include "scene.h"
#include "model_obj.h"

#ifndef __MAP_H_
#define __MAP_H_

class Map{
public:
            Map();
            ~Map();
    void    parseTilesFromFile();
    void    debugTiles();
    int     w,h;
    MapTile **tiles;
    void    appendChildToParentNode(aiNode *pParent, aiNode *pChild);
    bool    ai_assert(bool assert);
    float * vertexarray;
    
    ModelOBJ            g_model;
    float               g_heading;
    float               g_pitch;
    float               g_cameraPos[3];
    float               g_targetPos[3];
    
    
};
#endif