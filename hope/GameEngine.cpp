#include "GameEngine.h"

GameEngine::GameEngine() {
    _window = new Window( SCREEN_WIDTH, SCREEN_HEIGHT, SCREEN_BPP );
    _gui = new GUI( FONT_PATH );
    collisionWorld = new CollisionWorld();
    switch( GameStateS::Instance()->state ){
        case GS_FSM_GUI: _gameFlow = DemoGUI::Instance(); break;
        case GS_FSM_RENDER: _gameFlow = DemoRender::Instance(); break;
        case GS_FSM_NAVIGATION: _gameFlow = DemoNavigation::Instance(); break;            
        default: _gameFlow = DemoGUI::Instance(); break;
    }
    
}

GameEngine::~GameEngine() {
    delete _window;
    delete _gui;
}
 
void GameEngine::Update() {
    // check game flow
    switch( GameStateS::Instance()->state ){
        case GS_FSM_GUI: _gameFlow = DemoGUI::Instance(); break;
        case GS_FSM_RENDER: _gameFlow = DemoRender::Instance(); break;
        case GS_FSM_NAVIGATION: _gameFlow = DemoNavigation::Instance(); break;            
        default: _gameFlow = DemoGUI::Instance(); break;
    }
    
    _gameFlow->Update();
}

void GameEngine::HandleInput( std::list<action_t*> actions ) {
    _gameFlow->HandleInput(actions);
}

void GameEngine::HandleCollisions( std::list<collision_t*> collisions ) {
    _gameFlow->HandleCollisions(collisions);
}

void GameEngine::Render() {
    _gameFlow->Render();
    
    if(GameStateS::Instance()->showGUI) {
        _gui->BeginRender();
        // game flow GUI
        _gameFlow->RenderGUI(_gui);
        
        // global GUI
        _gui->BeginScrollArea( NULL, 5, SCREEN_HEIGHT-5-32, SCREEN_WIDTH-10, 32,
                             &GameStateS::Instance()->emptyScroll, false );
        if (_gui->Button("GUI Demo"))
            GameStateS::Instance()->state = GS_FSM_GUI;
        if (_gui->Button("Render Demo"))
            GameStateS::Instance()->state = GS_FSM_RENDER;
        if (_gui->Button("Navigation Demo"))
            GameStateS::Instance()->state = GS_FSM_NAVIGATION;
        _gui->EndScrollArea();
        
        _gui->EndRender();
    }
    
    SDL_GL_SwapBuffers();
}

