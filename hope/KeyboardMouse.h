#ifndef __KEYBOARDMOUSE_H_
#define __KEYBOARDMOUSE_H_

#include "Global.h"

class KeyboardMouse {
public:
                            KeyboardMouse(){};
                            ~KeyboardMouse(){};
    std::list<action_t*>    HandleEvents( SDL_Event &event, bool &quit );
    
};

#endif

