#ifndef hope_Imgui_h
#define hope_Imgui_h

#include "Global.h"

const Uint16    GFXCMD_QUEUE_SIZE = 5000;
// font constants
const float     FONT_HEIGHT = 15.0f;
const Uint16    TEXT_POOL_SIZE = 8000;
const float     TAB_STOPS[4] = {150, 210, 270, 330};
const int       TEXT_HEIGHT = 8;
// widget constants
const int       AREA_SCROLL_PADDING = 6;
const int       AREA_HEADER = 28;
const int       BUTTON_HEIGHT = 20;
const int       BUTTON_WIDTH = 150;
const int       CHECK_SIZE = 8;
const int       DEFAULT_SPACING = 4;
const int       INDENT_SIZE = 16;
const int       SLIDER_HEIGHT = 20;
const int       SLIDER_MARKER_WIDTH = 10;

const Uint8     TEMP_COORD_COUNT = 100; // max polygon coordiantes of a GUI widget
const int       CIRCLE_VERTS = 8*4; // 8 vertxes for corner

class GUI {
public:
    enum textAlign_t {
        ALIGN_LEFT,
        ALIGN_CENTER,
        ALIGN_RIGHT
    };
    static unsigned int RGBA( unsigned char r, unsigned char g, unsigned char b, unsigned char a=255 );

         GUI( const char* fontpath );
         ~GUI();
    void BeginRender();
    void EndRender();    
    void DrawText( int x, int y, int align, const char* text, unsigned int color );
    void BeginScrollArea( const char* name, int x, int y, int w, int h, Sint16* scroll, bool isVertical = true );
    void EndScrollArea();
    void BeginVerticalScrollArea( const char* name, int x, int y, int w, int h, Sint16* scroll );
    void EndVerticalScrollArea();
    void Label(const char* text);
    void Value(const char* text);
    bool Check(const char* text, bool checked, bool enabled = true);
    bool Button(const char* text, bool enabled = true);
    bool Item(const char* text, bool enabled = true);
    void SeparatorLine();
    bool Collapse(const char* text, const char* subtext, bool checked, bool enabled = true);
    void Indent();
    void Unindent();
    bool Slider(const char* text, float* val, float vmin, float vmax, float vinc, bool enabled = true);
    
private:
    enum gfxCmdType_t {
        GFXCMD_RECT,
        GFXCMD_TRIANGLE,
        GFXCMD_LINE,
        GFXCMD_TEXT,
        GFXCMD_SCISSOR
    };
    
    struct gfxRect_t {
        short x,y,w,h,r;
    };
    
    struct gfxText_t {
        short x,y,align;
        const char* text;
    };
    
    struct gfxLine_t {
        short x0,y0,x1,y1,r;
    };
    
    struct gfxCmd_t {
        char    type;
        char    flags;
        char    pad[2];
        Uint32  col;
        union {
            gfxLine_t line;
            gfxRect_t rect;
            gfxText_t text;
        };
    };
    
    gfxCmd_t     _gfxCmdQueue[GFXCMD_QUEUE_SIZE];
    unsigned     _gfxCmdQueueSize;
    char         _textPool[TEXT_POOL_SIZE];
    unsigned     _textPoolSize;
    
    // drag and drop state
    bool        _wentActive = false;
    int         _dragX = 0;
    int         _dragY = 0;
    float       _dragOrig = 0;
    
    // area and widget info
    Uint32      _areaId = 0;
    Uint32      _widgetId = 0;
    int         _widgetX = 0;
    int         _widgetY = 0;
    int         _widgetW = 0;
    
    int         _scrollTop = 0;
    int         _scrollBottom = 0;
    int         _scrollRight = 0;
    int         _scrollAreaTop = 0;
    Sint16 *    _scrollVal = 0;
    int         _focusTop = 0;
    int         _focusBottom = 0;
    Uint32      _scrollId = 0;
    bool        _insideScrollArea = false;
    bool        _verticalScroll = true;
    
    float       _tempCoords[TEMP_COORD_COUNT*2];
    float       _tempNormals[TEMP_COORD_COUNT*2];
    float       _circleVerts[CIRCLE_VERTS*2];
    
    bool        AnyActive() { return GameStateS::Instance()->ui.activeItem != 0; }
    bool        IsActive(Uint32 id) { return GameStateS::Instance()->ui.activeItem == id; }
    bool        IsHot(Uint32 id) { return GameStateS::Instance()->ui.hotItem == id; }
    bool        ButtonLogic(unsigned int id, bool over);
    
    bool         RegionHit( int x, int y, int w, int h );
    const char*  AllocText( const char* text );
    void         AddGfxCmdText( int x, int y, int align, const char* text, unsigned int color );
    void         AddGfxCmdScissor( int x, int y, int w, int h );
    void         AddGfxCmdRoundedRect( float x, float y, float w, float h, float r, unsigned int color );
    void         AddGfxCmdRect(float x, float y, float w, float h, unsigned int color);
    void         AddGfxCmdTriangle(int x, int y, int w, int h, int flags, unsigned int color);
    void         Render();
    // OpenGL draw methods
    void         GlDrawText( float x, float y, const char *text, int align, unsigned int col );
    void         GlDrawPolygon( const float* coords, unsigned numCoords, float r, unsigned int col );
    void         GlDrawRect( float x, float y, float w, float h, float fth, unsigned int col );
    void         GlDrawRoundedRect( float x, float y, float w, float h, float r, float fth, unsigned int col );
       
};

#endif




