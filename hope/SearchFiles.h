#ifndef hope_SearchFiles_h
#define hope_SearchFiles_h

#include <vector>
#include <string>
#ifdef WIN32
#	include <io.h>
#else
#	include <dirent.h>
#endif

typedef std::vector<std::string> fileNameList_t;

inline fileNameList_t SearchFiles(const std::string &path, const std::string &ext) {
    fileNameList_t files;
#ifdef WIN32
    _finddata_t dir;
    char pathWithExt[260];
    long fh;
    strcpy(pathWithExt, path.c_str());
    strcat(pathWithExt, "/*");
    strcat(pathWithExt, ext.c_str());
    fh = _findfirst(pathWithExt, &dir);
    if (fh == -1L)
        return;
    do {
        files.push_back(dir.name);
    } while (_findnext(fh, &dir) == 0);
    _findclose(fh);
#else
    DIR* dirFile = opendir( path.c_str() );

    if ( dirFile ) {
        dirent* hFile;
        while ( (hFile = readdir( dirFile )) != NULL )  {
            // ignore hidden files
            if ( hFile->d_name[0] == '.' ) continue;
            
            if ( strstr( hFile->d_name, ext.c_str() ) ) {
                files.push_back(hFile->d_name);
            }
        } 
        closedir( dirFile );
    }
#endif
    //files.sort();
    std::sort(files.begin(), files.end());
    return files;
}


#endif
