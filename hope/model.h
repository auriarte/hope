#ifndef _MODEL_H
#define _MODEL_H

//#include "basecode/window.h"
#include <cstring>
#include <vector>
#include <fstream>
#include <iostream>
#include "helper.h"
#include "tga.h"


using std::string;

class Model
{
	private:
		struct Triangle
		{
			unsigned int corners[3];

			Triangle(unsigned int a, unsigned int b, unsigned int c)
			{
				this->corners[0] = a;
				this->corners[1] = b;
				this->corners[2] = c;
			}
		};
		struct TxtCoordinate
		{
			float coord[2];

			TxtCoordinate(float a, float b)
			{
				this->coord[0] = a;
				this->coord[1] = b;
			}
		};

		//model data
		std::vector<Vertex>		vertices, normals;//vectors that store vertices and normals in vertex structure
		std::vector<TxtCoordinate>  vertexTextures;//vector that stores texture mapping coordinates for each vertex
		std::vector<Triangle>	faces,normalIndices,textureIndices;  //vector that stores indices of the vertices that form triangles

		//texture related variables
		std::string  textureFileName;// stores the name of the texture of the obj file
		GLuint		 textureId;

		//# of vertices and triangles
		int numberOfVertices, numberOfTriangles, numberOfTexels, numberOfNormals;

		//flags used for program phases
		bool textureFlag, lightingFlag;

		bool load(std::string modelName);//model loader, called after file extension check, by the loadModel() func.
		void initializeTexture(std::string texturePath, int id); //initialize texture, and make OpenGL related settings here

	public:
		Model();						//Default Constructor
		Model(std::string modelName);
		~Model();						//Desturctor

		void clear();

		void draw();
		void draw2();

		bool loadModel(std::string modelName);//actual file loader function
		void resetToInitialModel();

		inline int  getNumberOfVertices(){return numberOfVertices;};
		inline void invertTextureFlag(){textureFlag = !textureFlag;};
		inline void invertLightingFlag(){lightingFlag = !lightingFlag;};
};

#endif