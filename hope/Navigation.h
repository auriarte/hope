#ifndef hope_Navigation_h
#define hope_Navigation_h
#include "Global.h"

static const int SPANS_PER_POOL = 2048;
static const unsigned char RC_NULL_AREA = 0;
static const unsigned char WALKABLE_AREA = 63;
/// Defines the number of bits allocated to rcSpan::smin and rcSpan::smax.
static const int RC_SPAN_HEIGHT_BITS = 13;
/// Defines the maximum value for rcSpan::smin and rcSpan::smax.
static const int RC_SPAN_MAX_HEIGHT = (1<<RC_SPAN_HEIGHT_BITS)-1;

/// The number of spans allocated per span spool.
static const int RC_SPANS_PER_POOL = 2048;

static const int RC_NOT_CONNECTED = 0x3f;

static const unsigned short RC_BORDER_REG = 0x8000;

static const int RC_BORDER_VERTEX = 0x10000;
static const int RC_AREA_BORDER = 0x20000;
static const int RC_CONTOUR_REG_MASK = 0xffff;

static const unsigned short RC_MESH_NULL_IDX = 0xffff;

enum rcBuildContoursFlags {
	RC_CONTOUR_TESS_WALL_EDGES = 0x01,	///< Tessellate solid (impassable) edges during contour simplification.
	RC_CONTOUR_TESS_AREA_EDGES = 0x02,	///< Tessellate edges between areas during contour simplification.
};

struct span_t {
    unsigned int smin : 13;			///< The lower limit of the span. [Limit: < #smax]
    unsigned int smax : 13;			///< The upper limit of the span. [Limit: <= #RC_SPAN_MAX_HEIGHT]
    unsigned int area : 6;			///< The area id assigned to the span.
    span_t* next;					///< The next span higher up in column.
};

struct spanPool_t {
    spanPool_t* next;				///< The next span pool.
    span_t items[SPANS_PER_POOL];	///< Array of spans in the pool.
};

struct heightfield_t {
    int width;			///< The width of the heightfield. (Along the x-axis in cell units.)
    int height;			///< The height of the heightfield. (Along the z-axis in cell units.)
    float bmin[3];  	///< The minimum bounds in world space. [(x, y, z)]
    float bmax[3];		///< The maximum bounds in world space. [(x, y, z)]
    float cs;			///< The size of each cell. (On the xz-plane.)
    float ch;			///< The height of each cell. (The minimum increment along the y-axis.)
    span_t** spans;		///< Heightfield of spans (width*height).
    spanPool_t* pools;	///< Linked list of span pools.
    span_t* freelist;	///< The next free span.
};

struct compactCell_t
{
    unsigned int index : 24;	///< Index to the first span in the column.
    unsigned int count : 8;		///< Number of spans in the column.
};

struct compactSpan_t
{
    unsigned short y;			///< The lower extent of the span. (Measured from the heightfield's base.)
    unsigned short reg;			///< The id of the region the span belongs to. (Or zero if not in a region.)
    unsigned int con : 24;		///< Packed neighbor connection data.
    unsigned int h : 8;			///< The height of the span.  (Measured from #y.)
};

/// A compact, static heightfield representing unobstructed space.
struct compactHeightfield_t
{
    int width;					///< The width of the heightfield. (Along the x-axis in cell units.)
    int height;					///< The height of the heightfield. (Along the z-axis in cell units.)
    int spanCount;				///< The number of spans in the heightfield.
    int walkableHeight;			///< The walkable height used during the build of the field.  (See: rcConfig::walkableHeight)
    int walkableClimb;			///< The walkable climb used during the build of the field. (See: rcConfig::walkableClimb)
    int borderSize;				///< The AABB border size used during the build of the field. (See: rcConfig::borderSize)
    unsigned short maxDistance;	///< The maximum distance value of any span within the field. 
    unsigned short maxRegions;	///< The maximum region id of any span within the field. 
    float bmin[3];				///< The minimum bounds in world space. [(x, y, z)]
    float bmax[3];				///< The maximum bounds in world space. [(x, y, z)]
    float cs;					///< The size of each cell. (On the xz-plane.)
    float ch;					///< The height of each cell. (The minimum increment along the y-axis.)
    compactCell_t* cells;		///< Array of cells. [Size: #width*#height]
    compactSpan_t* spans;		///< Array of spans. [Size: #spanCount]
    unsigned short* dist;		///< Array containing border distance data. [Size: #spanCount]
    unsigned char* areas;		///< Array containing area id data. [Size: #spanCount]
};

/// Represents a simple, non-overlapping contour in field space.
struct contour_t
{
    int* verts;			///< Simplified contour vertex and connection data. [Size: 4 * #nverts]
    int nverts;			///< The number of vertices in the simplified contour. 
    int* rverts;		///< Raw contour vertex and connection data. [Size: 4 * #nrverts]
    int nrverts;		///< The number of vertices in the raw contour. 
    unsigned short reg;	///< The region id of the contour.
    unsigned char area;	///< The area id of the contour.
};

/// Represents a group of related contours.
struct contourSet_t
{
    contour_t* conts;	///< An array of the contours in the set. [Size: #nconts]
    int nconts;			///< The number of contours in the set.
    float bmin[3];  	///< The minimum bounds in world space. [(x, y, z)]
    float bmax[3];		///< The maximum bounds in world space. [(x, y, z)]
    float cs;			///< The size of each cell. (On the xz-plane.)
    float ch;			///< The height of each cell. (The minimum increment along the y-axis.)
    int width;			///< The width of the set. (Along the x-axis in cell units.) 
    int height;			///< The height of the set. (Along the z-axis in cell units.) 
    int borderSize;		///< The AABB border size used to generate the source data from which the contours were derived.
};

/// Represents a polygon mesh suitable for use in building a navigation mesh. 
struct polyMesh_t
{
    unsigned short* verts;	///< The mesh vertices. [Form: (x, y, z) * #nverts]
    unsigned short* polys;	///< Polygon and neighbor data. [Length: #maxpolys * 2 * #nvp]
    unsigned short* regs;	///< The region id assigned to each polygon. [Length: #maxpolys]
    unsigned short* flags;	///< The user defined flags for each polygon. [Length: #maxpolys]
    unsigned char* areas;	///< The area id assigned to each polygon. [Length: #maxpolys]
    int nverts;				///< The number of vertices.
    int npolys;				///< The number of polygons.
    int maxpolys;			///< The number of allocated polygons.
    int nvp;				///< The maximum number of vertices per polygon.
    float bmin[3];			///< The minimum bounds in world space. [(x, y, z)]
    float bmax[3];			///< The maximum bounds in world space. [(x, y, z)]
    float cs;				///< The size of each cell. (On the xz-plane.)
    float ch;				///< The height of each cell. (The minimum increment along the y-axis.)
    int borderSize;			///< The AABB border size used to generate the source data from which the mesh was derived.
};

/// Contains triangle meshes that represent detailed height data associated 
/// with the polygons in its associated polygon mesh object.
struct polyMeshDetail_t
{
    unsigned int* meshes;	///< The sub-mesh data. [Size: 4*#nmeshes] 
    float* verts;			///< The mesh vertices. [Size: 3*#nverts] 
    unsigned char* tris;	///< The mesh triangles. [Size: 4*#ntris] 
    int nmeshes;			///< The number of sub-meshes defined by #meshes.
    int nverts;				///< The number of vertices in #verts.
    int ntris;				///< The number of triangles in #tris.
};






// Function prototypes
void rcRasterizeTriangles(const float* verts, const int nv,
						  const int* tris, const unsigned char* areas, const int nt,
						  heightfield_t& solid, const int flagMergeThr = 1);

void rcFilterLowHangingWalkableObstacles(const int walkableClimb, heightfield_t& solid);
void rcFilterLedgeSpans(const int walkableHeight, const int walkableClimb, heightfield_t& solid);
void rcFilterWalkableLowHeightSpans(int walkableHeight, heightfield_t& solid);
bool rcBuildCompactHeightfield(const int walkableHeight, const int walkableClimb,
							   heightfield_t& hf, compactHeightfield_t& chf);
bool rcBuildDistanceField(compactHeightfield_t& chf);
bool rcBuildRegions(compactHeightfield_t& chf, const int borderSize, const int minRegionArea, const int mergeRegionArea);
bool rcBuildContours(compactHeightfield_t& chf, const float maxError, const int maxEdgeLen,
					 contourSet_t& cset, const int flags = RC_CONTOUR_TESS_WALL_EDGES);
bool rcBuildPolyMesh(contourSet_t& cset, const int nvp, polyMesh_t& mesh);








inline void rcVcopy(float* dest, const float* v)
{
	dest[0] = v[0];
	dest[1] = v[1];
	dest[2] = v[2];
}

inline void rcVsub(float* dest, const float* v1, const float* v2)
{
	dest[0] = v1[0]-v2[0];
	dest[1] = v1[1]-v2[1];
	dest[2] = v1[2]-v2[2];
}

inline void rcVcross(float* dest, const float* v1, const float* v2)
{
	dest[0] = v1[1]*v2[2] - v1[2]*v2[1];
	dest[1] = v1[2]*v2[0] - v1[0]*v2[2];
	dest[2] = v1[0]*v2[1] - v1[1]*v2[0];
}

inline void rcVnormalize(float* v)
{
	float d = 1.0f / sqrtf((v[0]*v[0]) + (v[1]*v[1]) + (v[2]*v[2]));
	v[0] *= d;
	v[1] *= d;
	v[2] *= d;
}

static void calcTriNormal(const float* v0, const float* v1, const float* v2, float* norm)
{
	float e0[3], e1[3];
	rcVsub(e0, v1, v0);
	rcVsub(e1, v2, v0);
	rcVcross(norm, e0, e1);
    //    std::cout << " cross: ("<<norm[0]<<","<<norm[1]<<","<<norm[2]<<")";
	rcVnormalize(norm);
    //    std::cout << " norm: ("<<norm[0]<<","<<norm[1]<<","<<norm[2]<<")" << std::endl;
}

inline void rcVmin(float* mn, const float* v)
{
	mn[0] = std::min(mn[0], v[0]);
	mn[1] = std::min(mn[1], v[1]);
	mn[2] = std::min(mn[2], v[2]);
}

inline void rcVmax(float* mx, const float* v)
{
	mx[0] = std::max(mx[0], v[0]);
	mx[1] = std::max(mx[1], v[1]);
	mx[2] = std::max(mx[2], v[2]);
}

template<class T> inline T rcClamp(T v, T mn, T mx) { return v < mn ? mn : (v > mx ? mx : v); }

inline int rcGetDirOffsetX(int dir)
{
	const int offset[4] = { -1, 0, 1, 0, };
	return offset[dir&0x03];
}
inline int rcGetDirOffsetY(int dir)
{
	const int offset[4] = { 0, 1, 0, -1 };
	return offset[dir&0x03];
}

inline int rcGetCon(const compactSpan_t& s, int dir)
{
	const unsigned int shift = (unsigned int)dir*6;
	return (s.con >> shift) & 0x3f;
}

// debug opengl functions--------------------------------
inline unsigned int duRGBA(int r, int g, int b, int a)
{
	return ((unsigned int)r) | ((unsigned int)g << 8) | ((unsigned int)b << 16) | ((unsigned int)a << 24);
}

inline int bit(int a, int b)
{
	return (a & (1 << b)) >> b;
}

inline unsigned int duIntToCol(int i, int a)
{
	int	r = bit(i, 1) + bit(i, 3) * 2 + 1;
	int	g = bit(i, 2) + bit(i, 4) * 2 + 1;
	int	b = bit(i, 0) + bit(i, 5) * 2 + 1;
	return duRGBA(r*63,g*63,b*63,a);
}
inline unsigned int duMultCol(const unsigned int col, const unsigned int d)
{
	const unsigned int r = col & 0xff;
	const unsigned int g = (col >> 8) & 0xff;
	const unsigned int b = (col >> 16) & 0xff;
	const unsigned int a = (col >> 24) & 0xff;
	return duRGBA((r*d) >> 8, (g*d) >> 8, (b*d) >> 8, a);
}
inline void duCalcBoxColors(unsigned int* colors, unsigned int colTop, unsigned int colSide)
{
	if (!colors) return;
	
	colors[0] = duMultCol(colTop, 250);
	colors[1] = duMultCol(colSide, 140);
	colors[2] = duMultCol(colSide, 165);
	colors[3] = duMultCol(colSide, 217);
	colors[4] = duMultCol(colSide, 165);
	colors[5] = duMultCol(colSide, 217);
}
inline unsigned int duDarkenCol(unsigned int col)
{
	return ((col >> 1) & 0x007f7f7f) | (col & 0xff000000);
}

inline unsigned int duLerpCol(unsigned int ca, unsigned int cb, unsigned int u)
{
	const unsigned int ra = ca & 0xff;
	const unsigned int ga = (ca >> 8) & 0xff;
	const unsigned int ba = (ca >> 16) & 0xff;
	const unsigned int aa = (ca >> 24) & 0xff;
	const unsigned int rb = cb & 0xff;
	const unsigned int gb = (cb >> 8) & 0xff;
	const unsigned int bb = (cb >> 16) & 0xff;
	const unsigned int ab = (cb >> 24) & 0xff;
	
	unsigned int r = (ra*(255-u) + rb*u)/255;
	unsigned int g = (ga*(255-u) + gb*u)/255;
	unsigned int b = (ba*(255-u) + bb*u)/255;
	unsigned int a = (aa*(255-u) + ab*u)/255;
	return duRGBA(r,g,b,a);
}

inline void duAppendBox(float minx, float miny, float minz,
                        float maxx, float maxy, float maxz, const unsigned int* fcol)
{
	const float verts[8*3] =
	{
		minx, miny, minz,
		maxx, miny, minz,
		maxx, miny, maxz,
		minx, miny, maxz,
		minx, maxy, minz,
		maxx, maxy, minz,
		maxx, maxy, maxz,
		minx, maxy, maxz,
	};
	static const unsigned char inds[6*4] =
	{
		7, 6, 5, 4,
		0, 1, 2, 3,
		1, 5, 6, 2,
		3, 7, 4, 0,
		2, 6, 7, 3,
		0, 4, 5, 1,
	};
	
	const unsigned char* in = inds;
	for (int i = 0; i < 6; ++i) {
        glColor4ubv((GLubyte*)&fcol[i]);
        glVertex3fv(&verts[*in*3]); in++;
        glVertex3fv(&verts[*in*3]); in++;        
        glVertex3fv(&verts[*in*3]); in++;                
        glVertex3fv(&verts[*in*3]); in++;
	}
}

/// A simple dynamic array of integers.
class rcIntArray
{
	int* m_data;
	int m_size, m_cap;
	inline rcIntArray(const rcIntArray&);
	inline rcIntArray& operator=(const rcIntArray&);
public:
    
	/// Constructs an instance with an initial array size of zero.
	inline rcIntArray() : m_data(0), m_size(0), m_cap(0) {}
    
	/// Constructs an instance initialized to the specified size.
	///  @param[in]		n	The initial size of the integer array.
	inline rcIntArray(int n) : m_data(0), m_size(0), m_cap(0) { resize(n); }
	inline ~rcIntArray() { free(m_data); }
    
	/// Specifies the new size of the integer array.
	///  @param[in]		n	The new size of the integer array.
	void resize(int n);
    
	/// Push the specified integer onto the end of the array and increases the size by one.
	///  @param[in]		item	The new value.
	inline void push(int item) { resize(m_size+1); m_data[m_size-1] = item; }
    
	/// Returns the value at the end of the array and reduces the size by one.
	///  @return The value at the end of the array.
	inline int pop() { if (m_size > 0) m_size--; return m_data[m_size]; }
    
	/// The value at the specified array index.
	/// @warning Does not provide overflow protection.
	///  @param[in]		i	The index of the value.
	inline const int& operator[](int i) const { return m_data[i]; }
    
	/// The value at the specified array index.
	/// @warning Does not provide overflow protection.
	///  @param[in]		i	The index of the value.
	inline int& operator[](int i) { return m_data[i]; }
    
	/// The current size of the integer array.
	inline int size() const { return m_size; }
};

inline void rcIntArray::resize(int n)
{
	if (n > m_cap)
	{
		if (!m_cap) m_cap = n;
		while (m_cap < n) m_cap *= 2;
		int* newData = (int*)malloc(m_cap*sizeof(int));
		if (m_size && newData) memcpy(newData, m_data, m_size*sizeof(int));
		free(m_data);
		m_data = newData;
	}
	m_size = n;
}

struct rcRegion
{
	inline rcRegion(unsigned short i) :
    spanCount(0),
    id(i),
    areaType(0),
    remap(false),
    visited(false)
	{}
	
	int spanCount;					// Number of spans belonging to this region
	unsigned short id;				// ID of the region
	unsigned char areaType;			// Are type.
	bool remap;
	bool visited;
	rcIntArray connections;
	rcIntArray floors;
};

/// A simple helper class used to delete an array when it goes out of scope.
template<class T> class rcScopedDelete
{
	T* ptr;
	inline T* operator=(T* p);
public:
    
	/// Constructs an instance with a null pointer.
	inline rcScopedDelete() : ptr(0) {}
    
	/// Constructs an instance with the specified pointer.
	///  @param[in]		p	An pointer to an allocated array.
	inline rcScopedDelete(T* p) : ptr(p) {}
	inline ~rcScopedDelete() { free(ptr); }
    
	/// The root array pointer.
	///  @return The root array pointer.
	inline operator T*() { return ptr; }
};

#endif
