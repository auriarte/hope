//
//  Cam.cpp
//  hope
//
//  Created by Ehsan Khosroshahi on 2/13/13.
//
//

#include "Cam.h"


Camera::Camera()
{
	//Init with standard OGL values:
	setPosition(0.0, 0.0,0.0);
	setViewDir( 0.0, 0.0,-1.0);
	setRightVector(0.0, 0.0,-1.0);
	//UpVector = vector3d(0.0, 1.0, 0.0);
    setUpVector(0.0 , 1.0, 0.0);
	//Only to be sure:
	RotatedX = RotatedY = RotatedZ = 0.0;
}

void Camera::Reset()
{
	//Init with standard OGL values:
	Position = vector3d (0.0,0.0,0.0);
	ViewDir = vector3d(0.0,0.0,-1.0);
	RightVector = vector3d(1.0,0.0,0.0);
	UpVector = vector3d(0.0,1.0,0.0);
    
	//Only to be sure:
	RotatedX = RotatedY = RotatedZ = 0.0;
}

void Camera::Move (vector3d Direction)
{
	Position = Position + Direction;
}

void Camera::RotateX (GLfloat Angle)
{
	RotatedX += Angle;
	
	//Rotate viewdir around the right vector:
	ViewDir = (ViewDir*cos(Angle*PIdiv180)
								+ UpVector*sin(Angle*PIdiv180));
    ViewDir.normalize();
    
	//now compute the new UpVector (by cross product)
	UpVector = ViewDir.crossProduct(RightVector)*-1;
    
	
}

void Camera::RotateY (GLfloat Angle)
{
	RotatedY += Angle;
	
	//Rotate viewdir around the up vector:
	ViewDir = (ViewDir*cos(Angle*PIdiv180)
								- RightVector*sin(Angle*PIdiv180));
    ViewDir.normalize();
    
    
	//now compute the new RightVector (by cross product)
	RightVector = ViewDir.crossProduct(UpVector);
}

void Camera::RotateZ (GLfloat Angle)
{
	RotatedZ += Angle;
	
	//Rotate viewdir around the right vector:
	RightVector = (RightVector*cos(Angle*PIdiv180)
                                    + UpVector*sin(Angle*PIdiv180));
    RightVector.normalize();

    
	//now compute the new UpVector (by cross product)
	UpVector = ViewDir.crossProduct(RightVector)*-1;
}

void Camera::Render( void )
{
    
	//The point at which the camera looks:
	vector3d ViewPoint = Position+ViewDir;
    
  //  setUpVector(0.0 , 1.0, 0.0);
    
	//as we know the up vector, we can easily use gluLookAt:
	gluLookAt(Position.x,Position.y,Position.z,
              ViewPoint.x,ViewPoint.y,ViewPoint.z,
              UpVector.x,UpVector.y,UpVector.z);

}

void Camera::MoveForward( GLfloat Distance )
{
	Position = Position + (ViewDir*-Distance);
}

void Camera::StrafeRight ( GLfloat Distance )
{
	Position = Position + (RightVector*Distance);
}

void Camera::MoveUpward( GLfloat Distance )
{
	Position = Position + (UpVector*Distance);
}