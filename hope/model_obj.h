//-----------------------------------------------------------------------------
// Copyright (c) 2007 dhpoware. All Rights Reserved.
//
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense,
// and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.
//-----------------------------------------------------------------------------

#if !defined(MODEL_OBJ_H)
#define MODEL_OBJ_H

#include <cstdio>
#include <map>
#include <string>
#include <vector>

//-----------------------------------------------------------------------------
// Alias|Wavefront OBJ file loader.
//
// This OBJ file loader contains the following restrictions:
// 1. Group information is ignored. Faces are grouped based on the material
//    that each face uses.
// 2. Object information is ignored. This loader will merge everything into a
//    single object.
// 3. The MTL file must be located in the same directory as the OBJ file. If
//    it isn't then the MTL file will fail to load and a default material is
//    used instead.
// 4. This loader triangulates all polygonal faces during importing.
//-----------------------------------------------------------------------------

class ModelOBJ
{
public:
    struct Material
    {
        float ambient[4];
        float diffuse[4];
        float specular[4];
        float shininess;        // [0 = min shininess, 1 = max shininess]
        float alpha;            // [0 = fully transparent, 1 = fully opaque]

        std::string name;
        std::string colorMapFilename;
        std::string bumpMapFilename;
    };

    struct Vertex
    {
        float position[3];
        float texCoord[2];
        float normal[3];
        float tangent[4];
        float bitangent[3];
    };

    struct Mesh
    {
        int startIndex;
        int triangleCount;
        const Material *pMaterial;
    };

    ModelOBJ();
    ~ModelOBJ();

    void destroy();
    bool import(const char *pszFilename, bool rebuildNormals = false);
    void normalize(float scaleTo = 1.0f, bool center = true);
    void reverseWinding();

    inline bool exists() const { return m_hasPositions; }
    // Getter methods.

    inline void getCenter(float &x, float &y, float &z) const { x = m_center[0]; y = m_center[1]; z = m_center[2]; }
    inline float getWidth() const { return m_width; }
    inline float getHeight() const { return m_height; }
    inline float getLength() const { return m_length; }
    inline float getRadius() const { return m_radius; }
    
    inline const int *getIndexBuffer() const { return &m_indexBuffer[0]; }
    inline const int *getAttributeBuffer() const { return &m_attributeBuffer[0]; }
    
    inline int getIndexSize() const { return static_cast<int>(sizeof(int)); }
    
    inline const Material &getMaterial(int i) const { return m_materials[i]; }
    inline const Mesh &getMesh(int i) const { return m_meshes[i]; }
    
    inline int getNumberOfIndices() const { return m_numberOfTriangles * 3; }
    inline int getNumberOfMaterials() const { return m_numberOfMaterials; }
    inline int getNumberOfMeshes() const { return m_numberOfMeshes; }
    inline int getNumberOfTriangles() const { return m_numberOfTriangles; }
    inline int getNumberOfVertices() const { return static_cast<int>(m_vertexBuffer.size()); }
    inline int getNumberOfAttributes() const { return (int)m_attributeBuffer.size(); }
    
    inline const std::string &getPath() const { return m_directoryPath; }
    
    inline const std::string &getcolorfile() const { return m_materials[0].colorMapFilename; }
//    inline const std::string &getPath() const { return m_directoryPath; }
//    inline const std::string &getPath() const { return m_directoryPath; }
    
    inline const Vertex &getVertex(int i) const { return m_vertexBuffer[i]; }
    inline const Vertex *getVertexBuffer() const { return &m_vertexBuffer[0]; }
    
    inline int getVertexSize() const { return static_cast<int>(sizeof(Vertex)); }
    
    inline bool hasNormals() const { return m_hasNormals; }
    inline bool hasPositions() const { return m_hasPositions; }
    inline bool hasTangents() const { return m_hasTangents; }
    inline bool hasTextureCoords() const { return m_hasTextureCoords; }
    
    inline const float* getMeshBoundsMin() const { return m_meshBMin; }
	inline const float* getMeshBoundsMax() const { return m_meshBMax; }
    
    void addVertex(float x, float y, float z, int& cap);
	void addTriangle(int a, int b, int c, int& cap);
	
	float m_scale;	
	float* m_verts;
	int* m_tris;
//	float* m_normals0;
	int m_vertCount;
	int m_triCount;

private:
    void addTrianglePos(int index, int material,
        int v0, int v1, int v2);
    void addTrianglePosNormal(int index, int material,
        int v0, int v1, int v2,
        int vn0, int vn1, int vn2);
    void addTrianglePosTexCoord(int index, int material,
        int v0, int v1, int v2,
        int vt0, int vt1, int vt2);
    void addTrianglePosTexCoordNormal(int index, int material,
        int v0, int v1, int v2,
        int vt0, int vt1, int vt2,
        int vn0, int vn1, int vn2);
    int addVertex(int hash, const Vertex *pVertex);
    void bounds(float center[3], float &width, float &height,
        float &length, float &radius);
    void buildMeshes();
    void generateNormals();
    void generateTangents();
    void importGeometryFirstPass(FILE *pFile);
    void importGeometrySecondPass(FILE *pFile);
    bool importMaterials(const char *pszFilename);
    void scale(float scaleFactor, float offset[3]);

    bool m_hasPositions;
    bool m_hasTextureCoords;
    bool m_hasNormals;
    bool m_hasTangents;

    int m_numberOfVertexCoords;
    int m_numberOfTextureCoords;
    int m_numberOfNormals;
    int m_numberOfTriangles;
    int m_numberOfMaterials;
    int m_numberOfMeshes;

    float m_center[3];
    float m_width;
    float m_height;
    float m_length;
    float m_radius;
    float m_meshBMin[3];
    float m_meshBMax[3];

    std::string m_directoryPath;

    std::vector<Mesh> m_meshes;
    std::vector<Material> m_materials;
    std::vector<Vertex> m_vertexBuffer;
    std::vector<int> m_indexBuffer;
    std::vector<int> m_attributeBuffer;
    std::vector<float> m_vertexCoords;
    std::vector<float> m_textureCoords;
    std::vector<float> m_normals;

    std::map<std::string, int> m_materialCache;
    std::map<int, std::vector<int> > m_vertexCache;
};

//-----------------------------------------------------------------------------
#endif
