//
//  Cam.h
//  hope
//
//  Created by Ehsan Khosroshahi on 2/13/13.
//
//

#ifndef __hope__Cam__
#define __hope__Cam__

#include "Vector3d.h"
#include <iostream>
#include <cmath>
#include "SDL.h"
#include "SDL_opengl.h"
#include "SDL_image.h"
#include "helper.h"
#define PI 3.1415926535897932384626433832795
#define PIdiv180 (PI/180.0)



class Camera{
private:
	
	vector3d ViewDir;
	vector3d RightVector;
	vector3d UpVector;
	vector3d Position;
    
	GLfloat RotatedX, RotatedY, RotatedZ;
	
public:
	Camera();				//inits the values (Position: (0|0|0) Target: (0|0|-1) )
	void Render ( void );	//executes some glRotates and a glTranslate command
    //Note: You should call glLoadIdentity before using Render
    
	void Move ( vector3d Direction );
	void RotateX ( GLfloat Angle );
	void RotateY ( GLfloat Angle );
	void RotateZ ( GLfloat Angle );
    
	void MoveForward ( GLfloat Distance );
	void MoveUpward ( GLfloat Distance );
	void StrafeRight ( GLfloat Distance );
    
	void Reset();
    
	vector3d getPosition(){return Position;};
	vector3d getUpVector(){return UpVector;};
	vector3d getViewDir(){return ViewDir;};
    
	void setPosition(float x, float y, float z){Position.x = x;Position.y = y;Position.z = z;};
	void setUpVector(float x, float y, float z){UpVector.x = x;UpVector.y = y;UpVector.z = z;};
	void setViewDir(float x, float y, float z){ViewDir.x = x;ViewDir.y = y;ViewDir.z = z;};
    void setRightVector(float x, float y, float z){RightVector.x = x;RightVector.y = y;RightVector.z = z;};

};


#endif /* defined(__hope__Cam__) */
