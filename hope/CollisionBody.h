#ifndef __hope__CollisionBody__
#define __hope__CollisionBody__

#include <iostream>
#include "vector2.h"

class CollisionBody{
public:
    CollisionBody();
    ~CollisionBody();
};

enum CollisionBodyType_t{
    COLLISION_BODY_BOX_BLOCK,
    COLLISION_BODY_ACTOR,
    COLLISION_BODY_BOX_EMPTY //For regions
};

class CollisionBody2dBox : public CollisionBody {
public:
    CollisionBody2dBox(aiVector2D pos, aiVector2D size, CollisionBodyType_t type);
    ~CollisionBody2dBox();
    aiVector2D pos;
    aiVector2D size;
    CollisionBodyType_t type;
};

#endif /* defined(__hope__CollisionBody__) */
