#ifndef __GAMEENGINE_H_
#define __GAMEENGUNE_H_

#include "Global.h"
#include "Window.h"
#include "GUI.h"
#include "CollisionWorld.h"
// include all Game Flows
#include "DemoGUI.h"
#include "DemoRender.h"
#include "DemoNavigation.h"

class GameEngine {
public:
                GameEngine();
                ~GameEngine();
    void        Update();
    void        HandleInput( std::list<action_t*> actions );
    void        HandleCollisions( std::list<collision_t*> collisions );
    void        Render();
    CollisionWorld * collisionWorld;
    
private:
    Window  *   _window;
    GUI     *   _gui;
    GameFlow *  _gameFlow;
};


#endif