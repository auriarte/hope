//
//  DrawTerrain.h
//  hope
//
//  Created by Ehsan Khosroshahi on 3/13/13.
//
//

#ifndef __hope__DrawTerrain__
#define __hope__DrawTerrain__

#include <iostream>
#include <stdlib.h>
#include "GameFlow.h"
#include "imageloader.h"
#include "vector3d.h"

class Terrain {
private:
    int             w; //Width
    int             l; //Length
    float**         hs; //Heights
    vector3d**      normals;
    bool            computedNormals; //Whether normals is up-to-date
public:
    Terrain();
    ~Terrain();
    
    void loadTerrain(const char* filename, float height);
    void cleanup();
    void draw();
    
    
    inline int width(){ return w; };
    inline int lenght(){return l; };
    
    inline void setHeight(int x, int z, float y);
    float getHeight(int x, int z);
    void computeNormals();
    vector3d getNormal(int x, int z);
    
    
    
    
};

inline int max(int i, int j)
{
    if (i>j)
        return i;
    else return j;
    
}

#endif /* defined(__hope__DrawTerrain__) */
