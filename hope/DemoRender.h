#ifndef hope_DemoRender_h
#define hope_DemoRender_h

#include "GameFlow.h"
#include <string>
#include "model_obj.h"
#include "Cam.h"
#include "imageloader.h"
//#include "bitmap.h"
#include "DrawTerrain.h"

#include <stdio.h>

#include "ObjDraw.h"

using namespace std;

typedef             std::map<std::string, GLuint> ModelTextures;

class DemoRender : public GameFlow {
private:
    DemoRender();
    DemoRender(const DemoRender&);
    DemoRender& operator=(const DemoRender&);
    
    void                DrawCube(float posx, float posy , float size ,float hieght);
    void                RenderGameMap();
    void                RenderGameActors();
    void                UpdateCamera();
    void                DrawObj(std::string obj);
    
    GLuint              loadTexture(Image* image);
    Camera              _cam;
        
    ObjDraw             _test1;
    ObjDraw             _test2;
    
    Terrain             _land;
    
    bool                g_enableTextures = true;
    ModelTextures       _mapTextures;
    
    GLuint              _textureId; //The id of the texture

 //   GLuint              LoadTexture(const char *pszFilename);
    
    
    float               _scaleFactor;
    
    typedef             std::map<std::string, GLuint> ModelTextures;

public:
    static DemoRender * Instance();
    virtual void        HandleInput( std::list<action_t*> actions );
    virtual void        HandleCollisions( std::list<collision_t*> collisions );
    virtual void        Update();
    virtual void        Render();
    virtual void        RenderGUI( GUI* gui );    
};

#endif
