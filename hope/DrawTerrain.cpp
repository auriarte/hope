//
//  DrawTerrain.cpp
//  hope
//
//  Created by Ehsan Khosroshahi on 3/13/13.
//
//

#include "DrawTerrain.h"



Terrain::Terrain() {
   
}

Terrain::~Terrain() {
    for(int i = 0; i < l; i++) {
        delete[] hs[i];
    }
    delete[] hs;
    
    for(int i = 0; i < l; i++) {
        delete[] normals[i];
    }
    delete[] normals;
}




//Returns the height at (x, z)
float Terrain::getHeight(int x, int z) {
    return hs[z][x];
}



//TODO the compute normals should be impemented

void Terrain::computeNormals() {
    
    
    if (computedNormals) {
        return;
    }
    
    
}

//Returns the normal at (x, z)
vector3d Terrain::getNormal(int x, int z) {
    if (!computedNormals) {
        computeNormals();
    }
    return normals[z][x];
}


//Sets the height at (x, z) to y
void Terrain::setHeight(int x, int z, float y) {
    hs[z][x] = y;
    computedNormals = false;
}

//Loads a terrain from a heightmap.  The heights of the terrain range from
//-height / 2 to height / 2.
void Terrain::loadTerrain(const char* filename, float height) {
	Image* image = loadBMP(filename);
	w = image->width;
    l = image->height;
    std::cout << "image size width " << image->width <<std:: endl;
    std::cout << "image size height " << image->height << std::endl;
    
    
    hs = new float*[l];
    for(int i = 0; i < l; i++) {
        hs[i] = new float[w];
    }
    
    normals = new vector3d*[l];
    for(int i = 0; i < l; i++) {
        normals[i] = new vector3d[w];
    }
    
    computedNormals = false;
    
	for(int y = 0; y < image->height; y++) {
		for(int x = 0; x < image->width; x++) {
			unsigned char color = (unsigned char)image->pixels[3 * (y * image->width + x)];
			
            //float h = image->pixels[3 * (y * image->width + x)];
            float h = height * ((color / 255.0f) - 0.5f);
            //std::cout << image->pixels[3 * (y * image->width + x)];
			setHeight(x, y, h);
		}
        std::cout<< std::endl ;
	}
	
	delete image;
	computeNormals();
}



void Terrain::draw() {
	
	float scale = 5.0f / max( w - 1, l - 1);
    
    glPushMatrix();
	

    glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
    glScalef(scale, scale, scale);
    
    glTranslatef(-(float)(w - 1) / 2,
				 0.0f,
				 -(float)(l - 1) / 2);
	
	glColor3f(1.0f, 0.9f, 0.0f);
	for(int z = 0; z < l - 1; z++) {
		//Makes OpenGL draw a triangle at every three consecutive vertices
		glBegin(GL_TRIANGLE_STRIP);
		for(int x = 0; x < w; x++) {
      

            vector3d first = vector3d(x, getHeight(x, z), z);
            vector3d second = vector3d (x,getHeight(x, z + 1), z + 1 );
            vector3d third = vector3d(x +1 , getHeight(x+1, z), z);
//            //std::cout<< (third-first).x << "-" << (third-first).y << "-" << (third-first).z << "||" ;
//            
             vector3d normal =  (third-first).crossProduct(second- first);
           // vector3d normal = getNormal(x, z);
            //std::cout<< normal.x << "-" << normal.y << "-" << normal.z << "||";
			glNormal3f(normal.x, normal.y, normal.z);
			glVertex3f(x, getHeight(x, z), z);
			//normal = getNormal(x, z + 1);
			glNormal3f(normal.x, normal.y, normal.z);
			glVertex3f(x, getHeight(x, z + 1), z + 1);
		}
		glEnd();
	}
    glPopMatrix();
    
	
}

