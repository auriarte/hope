
#define STB_TRUETYPE_IMPLEMENTATION
#define STBTT_malloc(x,u)    malloc(x)
#define STBTT_free(x,u)      free(x)
#include "stb_truetype.h"

#include <errno.h>

#include "GUI.h"

static GLuint ftex;
static stbtt_bakedchar cdata[96]; // ASCII 32..126 is 95 glyphs

GUI::GUI(const char* fontpath) {

    // initialize vars
    _gfxCmdQueueSize = 0;
    _textPoolSize = 0;
    
    // precompute circle vertex (for round courners)
    for (int i = 0; i < CIRCLE_VERTS; ++i) {
        float a = (float)i/(float)CIRCLE_VERTS * M_PI*2;
        _circleVerts[i*2+0] = cosf(a);
        _circleVerts[i*2+1] = sinf(a);
    }
    
    // load font
	FILE* fp = fopen( fontpath, "rb" );
	if ( !fp ) {
        printf( "Failed to open font %s [%s]\n", fontpath, strerror(errno) );
        exit(1);
    }
	fseek( fp, 0, SEEK_END );
    long size = ftell( fp );
	fseek( fp, 0, SEEK_SET );
	
	unsigned char* ttfBuffer = (unsigned char*)malloc(size); 
	if ( !ttfBuffer ) {
        fclose( fp );
        printf( "Failed to open font %s [out of memory]\n", fontpath );
        exit(1);
    }
	
	fread( ttfBuffer, 1, size, fp );
	fclose( fp );
	fp = 0;
	
	unsigned char* bmap = (unsigned char*)malloc(512*512);
	if ( !bmap ) {
		free( ttfBuffer );
		printf( "Failed to open font %s [out of memory]\n", fontpath );
        exit(1);
	}
	
    // warning: no guarantee this fits!
	stbtt_BakeFontBitmap( ttfBuffer, 0, FONT_HEIGHT, bmap, 512, 512, 32, 96, cdata );
    
	glGenTextures( 1, &ftex );
	glBindTexture( GL_TEXTURE_2D, ftex );
	glTexImage2D( GL_TEXTURE_2D, 0, GL_ALPHA, 512,512, 0, GL_ALPHA, GL_UNSIGNED_BYTE, bmap );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
    
	free( ttfBuffer );
	free( bmap );
}

GUI::~GUI() {
    if ( ftex ) {
		glDeleteTextures( 1, &ftex );
		ftex = 0;
	}
}

unsigned int GUI::RGBA( unsigned char r, unsigned char g, unsigned char b, unsigned char a ) {
	return (r) | (g << 8) | (b << 16) | (a << 24);
}

void GUI::BeginRender() {
    // init openGL
    glDisable( GL_DEPTH_TEST );
    glDisable(GL_LIGHTING);     // need to disable lighting for proper text color
    glMatrixMode( GL_PROJECTION );
    glLoadIdentity();
    gluOrtho2D( 0, SCREEN_WIDTH, 0, SCREEN_HEIGHT ); // left, right, bottom, top
    glMatrixMode( GL_MODELVIEW );
    glLoadIdentity();

    // reset states
    GameStateS::Instance()->ui.mouseOverMenu = false;
	GameStateS::Instance()->ui.hotItem = GameStateS::Instance()->ui.hotToBeItem;
	GameStateS::Instance()->ui.hotToBeItem = 0;
	_wentActive = false;
   
	_widgetX = 0;
	_widgetY = 0;
	_widgetW = 0;
    
	_areaId = 1;
	_widgetId = 1;
    
	// reset queues and pools
    _gfxCmdQueueSize = 0;
	_textPoolSize = 0;
}

void GUI::EndRender() {
    // clear input
    GameStateS::Instance()->ClearUiMouse();
    
    Render();		

    glEnable(GL_DEPTH_TEST);
    glEnable(GL_LIGHTING);
}

void GUI::DrawText( int x, int y, int align, const char* text, unsigned int color ) {
	AddGfxCmdText (x, y, align, text, color );
}

void GUI::BeginScrollArea( const char* name, int x, int y, int w, int h, Sint16* scroll, bool isVertical ) {
    _areaId++;
	_widgetId = 0;
	_scrollId = (_areaId<<16) | _widgetId;
    _verticalScroll = isVertical;
    
	_widgetX = x + AREA_SCROLL_PADDING;
	_widgetY = y + h + (*scroll);
	_widgetW = w - AREA_SCROLL_PADDING * 4;
    
	_scrollTop = y + h;
	_scrollBottom = y + AREA_SCROLL_PADDING;
	_scrollRight = x + w - AREA_SCROLL_PADDING*3;
	_scrollVal = scroll;
	_scrollAreaTop = _widgetY;
    
	_focusTop = y;
	_focusBottom = y + h;
    
	_insideScrollArea = RegionHit(x, y, w, h);
    
	AddGfxCmdRoundedRect( (float)x, (float)y, (float)w, (float)h, 6, RGBA(0,0,0,192) );
    
    if (name && isVertical) {
        _widgetY -= AREA_HEADER;
        _scrollTop -= AREA_HEADER;
        _focusTop -= AREA_HEADER;
        _focusBottom -= AREA_HEADER;
        _scrollAreaTop -= AREA_HEADER; 
        
        AddGfxCmdText( x+AREA_HEADER/2, y+h-AREA_HEADER/2-TEXT_HEIGHT/2, ALIGN_LEFT, name, RGBA(255,255,255,128) );
        AddGfxCmdScissor( x+AREA_SCROLL_PADDING, y+AREA_SCROLL_PADDING, w-AREA_SCROLL_PADDING*4, h-AREA_HEADER-AREA_SCROLL_PADDING );
    } else {
        _widgetY -= AREA_SCROLL_PADDING;
        _scrollTop -= AREA_SCROLL_PADDING;
        _focusTop -= AREA_SCROLL_PADDING;
        _focusBottom -= AREA_SCROLL_PADDING;
        _scrollAreaTop -= AREA_SCROLL_PADDING; 
        
        AddGfxCmdScissor( x+AREA_SCROLL_PADDING, y+AREA_SCROLL_PADDING, w-AREA_SCROLL_PADDING*4, h-AREA_SCROLL_PADDING );
    }
    
	if (_insideScrollArea) GameStateS::Instance()->ui.mouseOverMenu = true;
}

void GUI::EndScrollArea() {
	// Disable scissoring.
	AddGfxCmdScissor(-1,-1,-1,-1);
    
	// Draw scroll bar
	int x = _scrollRight + AREA_SCROLL_PADDING/2;
	int y = _scrollBottom;
	int w = AREA_SCROLL_PADDING*2;
	int h = _scrollTop - _scrollBottom;
    
	int scrollAreaBottom = _widgetY;
	int scrollAreaHeight = _scrollAreaTop - scrollAreaBottom;
    
	float barHeight = (float)h/(float)scrollAreaHeight;
	
	if (barHeight < 1) {
		float barY = (float)(y - scrollAreaBottom)/(float)scrollAreaHeight;
		if (barY < 0) barY = 0;
		if (barY > 1) barY = 1;
		
		// Handle scroll bar logic.
		int hx = x;
		int hy = y + (int)(barY*h);
		int hw = w;
		int hh = (int)(barHeight*h);
		
		const int range = h - (hh-1);
		bool over = RegionHit(hx, hy, hw, hh);
		ButtonLogic(_scrollId, over);
		if (IsActive(_scrollId)) {
			float u = (float)(hy-y) / (float)range;
			if (_wentActive) {
				_dragY = GameStateS::Instance()->ui.mouseY;
				_dragOrig = u;
			}
			if (_dragY != GameStateS::Instance()->ui.mouseY) {
				u = _dragOrig + (GameStateS::Instance()->ui.mouseY - _dragY) / (float)range;
				if (u < 0) u = 0;
				if (u > 1) u = 1;
				*_scrollVal = (int)((1-u) * (scrollAreaHeight - h));
			}
		}
		
		// BG
		AddGfxCmdRoundedRect((float)x, (float)y, (float)w, (float)h, (float)w/2-1, RGBA(0,0,0,196));
		// Bar
		if (IsActive(_scrollId))
			AddGfxCmdRoundedRect((float)hx, (float)hy, (float)hw, (float)hh, (float)w/2-1, RGBA(255,196,0,196));
		else
			AddGfxCmdRoundedRect((float)hx, (float)hy, (float)hw, (float)hh, (float)w/2-1, IsHot(_scrollId) ? RGBA(255,196,0,96) : RGBA(255,255,255,64));
        
		// handle mouse scrolling by wheel
		if (_insideScrollArea && GameStateS::Instance()->ui.mouseScroll) {
            *_scrollVal += 20 * GameStateS::Instance()->ui.mouseScroll;
            if (*_scrollVal < 0) *_scrollVal = 0;
            if (*_scrollVal > (scrollAreaHeight - h)) *_scrollVal = (scrollAreaHeight - h);
		}
	}
}

void GUI::Label(const char* text) {
	int x = _widgetX;
	int y = _widgetY - BUTTON_HEIGHT;
	_widgetY -= BUTTON_HEIGHT;
	AddGfxCmdText( x, y+BUTTON_HEIGHT/2-TEXT_HEIGHT/2, ALIGN_LEFT, text, RGBA(255,255,255,255) );
}

void GUI::Value(const char* text) {
	int x = _widgetX;
	int y = _widgetY - BUTTON_HEIGHT;
	int w = _widgetW;
	_widgetY -= BUTTON_HEIGHT;
	
	AddGfxCmdText(x+w-BUTTON_HEIGHT/2, y+BUTTON_HEIGHT/2-TEXT_HEIGHT/2, ALIGN_RIGHT, text, RGBA(255,255,255,200));
}

bool GUI::Check(const char* text, bool checked, bool enabled) {
	_widgetId++;
	unsigned int id = (_areaId<<16) | _widgetId;
	
	int x = _widgetX;
	int y = _widgetY - BUTTON_HEIGHT;
	int w = _widgetW;
	int h = BUTTON_HEIGHT;
	_widgetY -= BUTTON_HEIGHT + DEFAULT_SPACING;
    
	bool over = enabled && RegionHit(x, y, w, h);
	bool res = ButtonLogic(id, over);
	
	const int cx = x+BUTTON_HEIGHT/2-CHECK_SIZE/2;
	const int cy = y+BUTTON_HEIGHT/2-CHECK_SIZE/2;
	AddGfxCmdRoundedRect((float)cx-3, (float)cy-3, (float)CHECK_SIZE+6, (float)CHECK_SIZE+6, 4, 
                         RGBA(128,128,128, IsActive(id)?196:96));
	if (checked) {
		if (enabled)
			AddGfxCmdRoundedRect((float)cx, (float)cy, (float)CHECK_SIZE, (float)CHECK_SIZE, (float)CHECK_SIZE/2-1, RGBA(255,255,255,IsActive(id)?255:200));
		else
			AddGfxCmdRoundedRect((float)cx, (float)cy, (float)CHECK_SIZE, (float)CHECK_SIZE, (float)CHECK_SIZE/2-1, RGBA(128,128,128,200));
	}
    
	if (enabled)
		AddGfxCmdText(x+BUTTON_HEIGHT, y+BUTTON_HEIGHT/2-TEXT_HEIGHT/2, ALIGN_LEFT, text, 
                      IsHot(id) ? RGBA(255,196,0,255) : RGBA(255,255,255,200));
	else
		AddGfxCmdText(x+BUTTON_HEIGHT, y+BUTTON_HEIGHT/2-TEXT_HEIGHT/2, ALIGN_LEFT, text, 
                      RGBA(128,128,128,200));
    
	return res;
}

bool GUI::Button(const char* text, bool enabled) {
	_widgetId++;
	unsigned int id = (_areaId<<16) | _widgetId;
	
	int x = _widgetX;
	int y = _widgetY - BUTTON_HEIGHT;
	int w = (_verticalScroll)? _widgetW : BUTTON_WIDTH;
	int h = BUTTON_HEIGHT;
    if (_verticalScroll) {
        _widgetY -= BUTTON_HEIGHT + DEFAULT_SPACING;
    } else {
        _widgetX += BUTTON_WIDTH + DEFAULT_SPACING;
    }
    
	bool over = enabled && RegionHit(x, y, w, h);
	bool res = ButtonLogic(id, over);
    
	AddGfxCmdRoundedRect((float)x, (float)y, (float)w, (float)h, (float)BUTTON_HEIGHT/2-1, 
                         RGBA(128,128,128, IsActive(id)?196:96));
	if (enabled)
		AddGfxCmdText(x+BUTTON_HEIGHT/2, y+BUTTON_HEIGHT/2-TEXT_HEIGHT/2, ALIGN_LEFT, text, 
                      IsHot(id) ? RGBA(255,196,0,255) : RGBA(255,255,255,200));
	else
		AddGfxCmdText(x+BUTTON_HEIGHT/2, y+BUTTON_HEIGHT/2-TEXT_HEIGHT/2, ALIGN_LEFT, text, 
                      RGBA(128,128,128,200));
    
	return res;
}

bool GUI::Item(const char* text, bool enabled) {
	_widgetId++;
	unsigned int id = (_areaId<<16) | _widgetId;
	
	int x = _widgetX;
	int y = _widgetY - BUTTON_HEIGHT;
	int w = (_verticalScroll)? _widgetW : BUTTON_WIDTH;
	int h = BUTTON_HEIGHT;
	if (_verticalScroll) {
        _widgetY -= BUTTON_HEIGHT + DEFAULT_SPACING;
    } else {
        _widgetX += BUTTON_WIDTH + DEFAULT_SPACING;
    }
	
	bool over = enabled && RegionHit(x, y, w, h);
	bool res = ButtonLogic(id, over);
	
	if (IsHot(id))
		AddGfxCmdRoundedRect((float)x, (float)y, (float)w, (float)h, 2.0f, RGBA(255,196,0,IsActive(id)?196:96));
    
	if (enabled)
		AddGfxCmdText(x+BUTTON_HEIGHT/2, y+BUTTON_HEIGHT/2-TEXT_HEIGHT/2, ALIGN_LEFT, text, RGBA(255,255,255,200));
	else
		AddGfxCmdText(x+BUTTON_HEIGHT/2, y+BUTTON_HEIGHT/2-TEXT_HEIGHT/2, ALIGN_LEFT, text, RGBA(128,128,128,200));
	
	return res;
}

void GUI::SeparatorLine() {
    int x = _widgetX;
	int y = _widgetY - DEFAULT_SPACING * 2;
	int w = _widgetW;
	int h = 1;
	_widgetY -= DEFAULT_SPACING * 4;
    
	AddGfxCmdRect((float)x, (float)y, (float)w, (float)h, RGBA(255,255,255,32));
}

bool GUI::Collapse(const char* text, const char* subtext, bool checked, bool enabled) {
	_widgetId++;
	unsigned int id = (_areaId<<16) | _widgetId;
	
	int x = _widgetX;
	int y = _widgetY - BUTTON_HEIGHT;
	int w = _widgetW;
	int h = BUTTON_HEIGHT;
	_widgetY -= BUTTON_HEIGHT;
    
	const int cx = x+BUTTON_HEIGHT/2-CHECK_SIZE/2;
	const int cy = y+BUTTON_HEIGHT/2-CHECK_SIZE/2;
    
	bool over = enabled && RegionHit(x, y, w, h);
	bool res = ButtonLogic(id, over);
	
	if (checked)
		AddGfxCmdTriangle(cx, cy, CHECK_SIZE, CHECK_SIZE, 2, RGBA(255,255,255,IsActive(id)?255:200));
	else
		AddGfxCmdTriangle(cx, cy, CHECK_SIZE, CHECK_SIZE, 1, RGBA(255,255,255,IsActive(id)?255:200));
    
	if (enabled)
		AddGfxCmdText(x+BUTTON_HEIGHT, y+BUTTON_HEIGHT/2-TEXT_HEIGHT/2, ALIGN_LEFT, text, 
                      IsHot(id) ? RGBA(255,196,0,255) : RGBA(255,255,255,200));
	else
		AddGfxCmdText(x+BUTTON_HEIGHT, y+BUTTON_HEIGHT/2-TEXT_HEIGHT/2, ALIGN_LEFT, text, RGBA(128,128,128,200));
    
	if (subtext)
		AddGfxCmdText(x+w-BUTTON_HEIGHT/2, y+BUTTON_HEIGHT/2-TEXT_HEIGHT/2, ALIGN_RIGHT, subtext, RGBA(255,255,255,128));
	
	return res;
}

void GUI::Indent() {
	_widgetX += INDENT_SIZE;
	_widgetW -= INDENT_SIZE;
}

void GUI::Unindent() {
	_widgetX -= INDENT_SIZE;
	_widgetW += INDENT_SIZE;
}

bool GUI::Slider(const char* text, float* val, float vmin, float vmax, float vinc, bool enabled) {
	_widgetId++;
	unsigned int id = (_areaId<<16) | _widgetId;
	
	int x = _widgetX;
	int y = _widgetY - BUTTON_HEIGHT;
	int w = _widgetW;
	int h = SLIDER_HEIGHT;
	_widgetY -= SLIDER_HEIGHT + DEFAULT_SPACING;
    
	AddGfxCmdRoundedRect((float)x, (float)y, (float)w, (float)h, 4.0f, RGBA(0,0,0,128));
    
	const int range = w - SLIDER_MARKER_WIDTH;
    
	float u = (*val - vmin) / (vmax-vmin);
	if (u < 0) u = 0;
	if (u > 1) u = 1;
	int m = (int)(u * range);
    
	bool over = enabled && RegionHit(x+m, y, SLIDER_MARKER_WIDTH, SLIDER_HEIGHT);
	bool res = ButtonLogic(id, over);
	bool valChanged = false;
    
	if (IsActive(id)) {
		if (_wentActive) {
			_dragX = GameStateS::Instance()->ui.mouseX;
			_dragOrig = u;
		}
		if (_dragX != GameStateS::Instance()->ui.mouseX) {
			u = _dragOrig + (float)(GameStateS::Instance()->ui.mouseX - _dragX) / (float)range;
			if (u < 0) u = 0;
			if (u > 1) u = 1;
			*val = vmin + u*(vmax-vmin);
			*val = floorf(*val/vinc+0.5f)*vinc; // Snap to vinc
			m = (int)(u * range);
			valChanged = true;
		}
	}
    
	if (IsActive(id))
		AddGfxCmdRoundedRect((float)(x+m), (float)y, (float)SLIDER_MARKER_WIDTH, (float)SLIDER_HEIGHT, 4.0f, RGBA(255,255,255,255));
	else
		AddGfxCmdRoundedRect((float)(x+m), (float)y, (float)SLIDER_MARKER_WIDTH, (float)SLIDER_HEIGHT, 4.0f, 
                             IsHot(id) ? RGBA(255,196,0,128) : RGBA(255,255,255,64));
    
	// TODO: fix this, take a look at 'nicenum'.
	int digits = (int)(ceilf(log10f(vinc)));
	char fmt[16];
	snprintf(fmt, 16, "%%.%df", digits >= 0 ? 0 : -digits);
	char msg[128];
	snprintf(msg, 128, fmt, *val);
	
	if (enabled) {
		AddGfxCmdText(x+SLIDER_HEIGHT/2, y+SLIDER_HEIGHT/2-TEXT_HEIGHT/2, ALIGN_LEFT, text, 
                      IsHot(id) ? RGBA(255,196,0,255) : RGBA(255,255,255,200));
		AddGfxCmdText(x+w-SLIDER_HEIGHT/2, y+SLIDER_HEIGHT/2-TEXT_HEIGHT/2, ALIGN_RIGHT, msg, 
                      IsHot(id) ? RGBA(255,196,0,255) : RGBA(255,255,255,200));
	} else {
		AddGfxCmdText(x+SLIDER_HEIGHT/2, y+SLIDER_HEIGHT/2-TEXT_HEIGHT/2, ALIGN_LEFT, text, RGBA(128,128,128,200));
		AddGfxCmdText(x+w-SLIDER_HEIGHT/2, y+SLIDER_HEIGHT/2-TEXT_HEIGHT/2, ALIGN_RIGHT, msg, RGBA(128,128,128,200));
	}
    
	return res || valChanged;
}

/********************************************************************************
 *                              PRIVATE METHODS                                 *
 ********************************************************************************/

bool GUI::ButtonLogic(unsigned int id, bool over) {
	bool res = false;
	
    // process down
	if (!AnyActive()) {
		if (over)
            GameStateS::Instance()->ui.hotToBeItem = id;
		if (IsHot(id) && GameStateS::Instance()->ui.mouseLeftPressed) {
            GameStateS::Instance()->ui.activeItem = id;
            _wentActive = true;
        }
	}
    
	// if button is active, then react on left up
	if (IsActive(id)) {
		if (over)
			GameStateS::Instance()->ui.hotToBeItem = id;
		if (GameStateS::Instance()->ui.mouseLeftReleased) {
			if (IsHot(id)) res = true;
            // clear active
            GameStateS::Instance()->ui.activeItem = 0;
            GameStateS::Instance()->ClearUiMouse();
		}
	}
    
	return res;
}

bool GUI::RegionHit(int x, int y, int w, int h/*, bool checkScroll*/) {
    return  GameStateS::Instance()->ui.mouseX >= x && 
            GameStateS::Instance()->ui.mouseX <= x + w && 
            GameStateS::Instance()->ui.mouseY >= y && 
            GameStateS::Instance()->ui.mouseY <= y + h;
}

const char* GUI::AllocText(const char* text) {
	unsigned long len = strlen(text)+1;
	if (_textPoolSize + len >= TEXT_POOL_SIZE) {
        printf( "Error: text pool overflow");
		return 0;
    }
	char* dst = &_textPool[_textPoolSize]; 
	memcpy(dst, text, len);
	_textPoolSize += len;
	return dst;
}

void GUI::AddGfxCmdText( int x, int y, int align, const char* text, unsigned int color ) {
	if ( _gfxCmdQueueSize >= GFXCMD_QUEUE_SIZE ) {
        printf( "Error: gfx queue overflow");
        return;
    }
	gfxCmd_t& cmd = _gfxCmdQueue[_gfxCmdQueueSize++];
	cmd.type = GFXCMD_TEXT;
	cmd.flags = 0;
	cmd.col = color;
	cmd.text.x = (short)x;
	cmd.text.y = (short)y;
	cmd.text.align = (short)align;
	cmd.text.text = AllocText(text);
}

void GUI::AddGfxCmdRoundedRect(float x, float y, float w, float h, float r, unsigned int color) {
	if (_gfxCmdQueueSize >= GFXCMD_QUEUE_SIZE) {
        printf( "Error: gfx queue overflow");
        return;
    }
	gfxCmd_t& cmd = _gfxCmdQueue[_gfxCmdQueueSize++];
	cmd.type = GFXCMD_RECT;
	cmd.flags = 0;
	cmd.col = color;
	cmd.rect.x = (short)(x*8.0f);
	cmd.rect.y = (short)(y*8.0f);
	cmd.rect.w = (short)(w*8.0f);
	cmd.rect.h = (short)(h*8.0f);
	cmd.rect.r = (short)(r*8.0f);
}

void GUI::AddGfxCmdScissor(int x, int y, int w, int h) {
	if (_gfxCmdQueueSize >= GFXCMD_QUEUE_SIZE) {
        printf( "Error: gfx queue overflow");
        return;
    }
	gfxCmd_t& cmd = _gfxCmdQueue[_gfxCmdQueueSize++];
	cmd.type = GFXCMD_SCISSOR;
	cmd.flags = x < 0 ? 0 : 1;	// on/off flag.
	cmd.col = 0;
	cmd.rect.x = (short)x;
	cmd.rect.y = (short)y;
	cmd.rect.w = (short)w;
	cmd.rect.h = (short)h;
}

void GUI::AddGfxCmdRect(float x, float y, float w, float h, unsigned int color) {
	if (_gfxCmdQueueSize >= GFXCMD_QUEUE_SIZE)
		return;
	gfxCmd_t& cmd = _gfxCmdQueue[_gfxCmdQueueSize++];
	cmd.type = GFXCMD_RECT;
	cmd.flags = 0;
	cmd.col = color;
	cmd.rect.x = (short)(x*8.0f);
	cmd.rect.y = (short)(y*8.0f);
	cmd.rect.w = (short)(w*8.0f);
	cmd.rect.h = (short)(h*8.0f);
	cmd.rect.r = 0;
}

void GUI::AddGfxCmdTriangle(int x, int y, int w, int h, int flags, unsigned int color) {
	if (_gfxCmdQueueSize >= GFXCMD_QUEUE_SIZE)
		return;
	gfxCmd_t& cmd = _gfxCmdQueue[_gfxCmdQueueSize++];
	cmd.type = GFXCMD_TRIANGLE;
	cmd.flags = (char)flags;
	cmd.col = color;
	cmd.rect.x = (short)(x*8.0f);
	cmd.rect.y = (short)(y*8.0f);
	cmd.rect.w = (short)(w*8.0f);
	cmd.rect.h = (short)(h*8.0f);
}

static void GetBakedQuad(stbtt_bakedchar *chardata, int pw, int ph, int char_index, 
                       float *xpos, float *ypos, stbtt_aligned_quad *q) {
	stbtt_bakedchar *b = chardata + char_index;
	int round_x = STBTT_ifloor(*xpos + b->xoff);
	int round_y = STBTT_ifloor(*ypos - b->yoff);
	
	q->x0 = (float)round_x;
	q->y0 = (float)round_y;
	q->x1 = (float)round_x + b->x1 - b->x0;
	q->y1 = (float)round_y - b->y1 + b->y0;
	
	q->s0 = b->x0 / (float)pw;
	q->t0 = b->y0 / (float)pw;
	q->s1 = b->x1 / (float)ph;
	q->t1 = b->y1 / (float)ph;
	
	*xpos += b->xadvance;
}

static float GetTextLength( stbtt_bakedchar *chardata, const char* text ) {
	float xpos = 0;
	float len = 0;
	while ( *text ) {
		int c = (unsigned char)*text;
		if ( c == '\t' ) {
			for ( int i = 0; i < 4; ++i ) {
				if ( xpos < TAB_STOPS[i] ) {
					xpos = TAB_STOPS[i];
					break;
				}
			}
		} else if ( c >= 32 && c < 128 ) {
			stbtt_bakedchar *b = chardata + c-32;
			int round_x = STBTT_ifloor((xpos + b->xoff) + 0.5);
			len = round_x + b->x1 - b->x0 + 0.5f;
			xpos += b->xadvance;
		}
		++text;
	}
	return len;
}

void GUI::Render() {
	const float s = 1.0f/8.0f;
    
	glDisable(GL_SCISSOR_TEST);
	for ( int i = 0; i < _gfxCmdQueueSize; ++i ) {
		const gfxCmd_t& cmd = _gfxCmdQueue[i];
        switch ( cmd.type ) {
            case GFXCMD_RECT:
                if (cmd.rect.r == 0) {
                    GlDrawRect((float)cmd.rect.x*s+0.5f, (float)cmd.rect.y*s+0.5f,
                               (float)cmd.rect.w*s-1, (float)cmd.rect.h*s-1,
                               1.0f, cmd.col);
                } else {
                    GlDrawRoundedRect((float)cmd.rect.x*s+0.5f, (float)cmd.rect.y*s+0.5f,
                                      (float)cmd.rect.w*s-1, (float)cmd.rect.h*s-1,
                                      (float)cmd.rect.r*s, 1.0f, cmd.col);
                }
                break;
            case GFXCMD_LINE:
//                drawLine(cmd.line.x0*s, cmd.line.y0*s, cmd.line.x1*s, cmd.line.y1*s, 
//                         cmd.line.r*s, 1.0f, cmd.col);
                break;
            case GFXCMD_TRIANGLE:
                if (cmd.flags == 1) {
                    const float verts[3*2] =
                    {
                        (float)cmd.rect.x*s+0.5f, (float)cmd.rect.y*s+0.5f,
                        (float)cmd.rect.x*s+0.5f+(float)cmd.rect.w*s-1, (float)cmd.rect.y*s+0.5f+(float)cmd.rect.h*s/2-0.5f,
                        (float)cmd.rect.x*s+0.5f, (float)cmd.rect.y*s+0.5f+(float)cmd.rect.h*s-1,
                    };
                    GlDrawPolygon(verts, 3, 1.0f, cmd.col);
                } else if (cmd.flags == 2) {
                    const float verts[3*2] =
                    {
                        (float)cmd.rect.x*s+0.5f, (float)cmd.rect.y*s+0.5f+(float)cmd.rect.h*s-1,
                        (float)cmd.rect.x*s+0.5f+(float)cmd.rect.w*s/2-0.5f, (float)cmd.rect.y*s+0.5f,
                        (float)cmd.rect.x*s+0.5f+(float)cmd.rect.w*s-1, (float)cmd.rect.y*s+0.5f+(float)cmd.rect.h*s-1,
                    };
                    GlDrawPolygon(verts, 3, 1.0f, cmd.col);
                }
                break;
            case GFXCMD_TEXT:
                GlDrawText( cmd.text.x, cmd.text.y, cmd.text.text, cmd.text.align, cmd.col );
                break;
            case GFXCMD_SCISSOR:
                if (cmd.flags) {
                    glEnable(GL_SCISSOR_TEST);
                    glScissor( cmd.rect.x, cmd.rect.y, cmd.rect.w, cmd.rect.h );
                } else {
                    glDisable(GL_SCISSOR_TEST);
                }
                break;
		}
	}
	glDisable(GL_SCISSOR_TEST);
}

void GUI::GlDrawText(float x, float y, const char *text, int align, unsigned int col) {
	if (!ftex) return;
	if (!text) return;
	
	if (align == ALIGN_CENTER)
		x -= GetTextLength( cdata, text )/2;
	else if (align == ALIGN_RIGHT)
		x -= GetTextLength( cdata, text );
	
	glColor4ub(col&0xff, (col>>8)&0xff, (col>>16)&0xff, (col>>24)&0xff);
	
	glEnable(GL_TEXTURE_2D);
	
	// assume orthographic projection with units = screen pixels, origin at top left
	glBindTexture(GL_TEXTURE_2D, ftex);
	
	glBegin(GL_TRIANGLES);
	
	const float ox = x;
	
	while (*text) {
		int c = (unsigned char)*text;
		if (c == '\t') {
			for (int i = 0; i < 4; ++i) {
				if (x < TAB_STOPS[i]+ox) {
					x = TAB_STOPS[i]+ox;
					break;
				}
			}
		} else if (c >= 32 && c < 128) {			
			stbtt_aligned_quad q;		
            GetBakedQuad( cdata, 512,512, c-32, &x,&y,&q );
			glTexCoord2f(q.s0, q.t0); glVertex2f(q.x0, q.y0);
			glTexCoord2f(q.s1, q.t1); glVertex2f(q.x1, q.y1);
			glTexCoord2f(q.s1, q.t0); glVertex2f(q.x1, q.y0);
			glTexCoord2f(q.s0, q.t0); glVertex2f(q.x0, q.y0);
			glTexCoord2f(q.s0, q.t1); glVertex2f(q.x0, q.y1);
			glTexCoord2f(q.s1, q.t1); glVertex2f(q.x1, q.y1);
		}
		++text;
	}
	
	glEnd();	
	glDisable(GL_TEXTURE_2D);
}

void GUI::GlDrawPolygon( const float* coords, unsigned numCoords, float r, unsigned int col ) {
	if (numCoords > TEMP_COORD_COUNT) {
        printf("[GUI] Polygon coordinates overflow");
        numCoords = TEMP_COORD_COUNT;
    }
	
	for (unsigned i = 0, j = numCoords-1; i < numCoords; j=i++) {
		const float* v0 = &coords[j*2];
		const float* v1 = &coords[i*2];
		float dx = v1[0] - v0[0];
		float dy = v1[1] - v0[1];
		float d = sqrtf(dx*dx+dy*dy);
		if (d > 0) {
			d = 1.0f/d;
			dx *= d;
			dy *= d;
		}
		_tempNormals[j*2+0] = dy;
		_tempNormals[j*2+1] = -dx;
	}
	
	for (unsigned i = 0, j = numCoords-1; i < numCoords; j=i++) {
		float dlx0 = _tempNormals[j*2+0];
		float dly0 = _tempNormals[j*2+1];
		float dlx1 = _tempNormals[i*2+0];
		float dly1 = _tempNormals[i*2+1];
		float dmx = (dlx0 + dlx1) * 0.5f;
		float dmy = (dly0 + dly1) * 0.5f;
		float	dmr2 = dmx*dmx + dmy*dmy;
		if (dmr2 > 0.000001f) {
			float	scale = 1.0f / dmr2;
			if (scale > 10.0f) scale = 10.0f;
			dmx *= scale;
			dmy *= scale;
		}
		_tempCoords[i*2+0] = coords[i*2+0]+dmx*r;
		_tempCoords[i*2+1] = coords[i*2+1]+dmy*r;
	}
	
	unsigned int colTrans = RGBA(col&0xff, (col>>8)&0xff, (col>>16)&0xff, 0);
	
	glBegin(GL_TRIANGLES);
	
	glColor4ubv((GLubyte*)&col);
	
	for (unsigned i = 0, j = numCoords-1; i < numCoords; j=i++) {
		glVertex2fv(&coords[i*2]);
		glVertex2fv(&coords[j*2]);
		glColor4ubv((GLubyte*)&colTrans);
		glVertex2fv(&_tempCoords[j*2]);
		
		glVertex2fv(&_tempCoords[j*2]);
		glVertex2fv(&_tempCoords[i*2]);
		
		glColor4ubv((GLubyte*)&col);
		glVertex2fv(&coords[i*2]);
	}
	
	glColor4ubv((GLubyte*)&col);
	for (unsigned i = 2; i < numCoords; ++i) {
		glVertex2fv(&coords[0]);
		glVertex2fv(&coords[(i-1)*2]);
		glVertex2fv(&coords[i*2]);
	}
	
	glEnd();
}

void GUI::GlDrawRect(float x, float y, float w, float h, float fth, unsigned int col) {
	float verts[4*2] =
	{
		x+0.5f, y+0.5f,
		x+w-0.5f, y+0.5f,
		x+w-0.5f, y+h-0.5f,
		x+0.5f, y+h-0.5f,
	};
	GlDrawPolygon(verts, 4, fth, col);
}

void GUI::GlDrawRoundedRect(float x, float y, float w, float h, float r, float fth, unsigned int col) {
	const unsigned n = CIRCLE_VERTS/4;
	float verts[(n+1)*4*2];
	const float* cverts = _circleVerts;
	float* v = verts;
	
	for (unsigned i = 0; i <= n; ++i) {
		*v++ = x+w-r + cverts[i*2]*r;
		*v++ = y+h-r + cverts[i*2+1]*r;
	}
	
	for (unsigned i = n; i <= n*2; ++i) {
		*v++ = x+r + cverts[i*2]*r;
		*v++ = y+h-r + cverts[i*2+1]*r;
	}
	
	for (unsigned i = n*2; i <= n*3; ++i) {
		*v++ = x+r + cverts[i*2]*r;
		*v++ = y+r + cverts[i*2+1]*r;
	}
	
	for (unsigned i = n*3; i < n*4; ++i) {
		*v++ = x+w-r + cverts[i*2]*r;
		*v++ = y+r + cverts[i*2+1]*r;
	}
	*v++ = x+w-r + cverts[0]*r;
	*v++ = y+r + cverts[1]*r;
	
	GlDrawPolygon(verts, (n+1)*4, fth, col);
}
