#include "KeyboardMouse.h"

std::list<action_t*> KeyboardMouse::HandleEvents( SDL_Event &event, bool &quit ){
    std::list<action_t*> actions;
    GameStateS::Instance()->ui.mouseScroll = 0;
    // while there are events to handle
    while( SDL_PollEvent( &event ) ) {
        action_t* action = new action_t;
        int x = 0;
        int y = 0;
        switch ( event.type ) {
            case SDL_QUIT:
                quit=true;
                break;
            case SDL_MOUSEMOTION:
                if(!GameStateS::Instance()->ui.mouseOverMenu){
                    if ( SDL_GetMouseState( NULL , NULL ) & SDL_BUTTON(SDL_BUTTON_RIGHT) ) {
                        action->action = AIM;
                        action->x = event.motion.xrel;
                        action->y = event.motion.yrel;
                    }
                    else if ( SDL_GetMouseState( NULL , NULL ) & SDL_BUTTON(SDL_BUTTON_LEFT) ) {
                        action->action = TRANSLATION;
                        action->x = event.motion.xrel;
                        action->y = event.motion.yrel;
                    }
                }
                GameStateS::Instance()->ui.mouseX = event.motion.x;
                GameStateS::Instance()->ui.mouseY = SCREEN_HEIGHT-1 - event.motion.y; // GUI origin is at bottom-left
                break;
            case SDL_MOUSEBUTTONDOWN:
                switch ( event.button.button ) {
                    case SDL_BUTTON_LEFT:
                        GameStateS::Instance()->UpdateUiMouse(true);
                        break;
                    case SDL_BUTTON_WHEELUP:
                        action->action = WHEEL_UP;
                        if (GameStateS::Instance()->ui.mouseOverMenu)
							GameStateS::Instance()->ui.mouseScroll--;
                        break;
                    case SDL_BUTTON_WHEELDOWN:
                        action->action = WHEEL_DOWN;
                        if (GameStateS::Instance()->ui.mouseOverMenu)
                            GameStateS::Instance()->ui.mouseScroll++;
                        break;
                }
                break;
            case SDL_MOUSEBUTTONUP:
                action->x = event.button.x;
                action->y = event.button.y;
                switch ( event.button.button ) {
                    case SDL_BUTTON_LEFT:
                        action->action = PRIMARY;
                        GameStateS::Instance()->UpdateUiMouse(false);
                        break;
                    case SDL_BUTTON_RIGHT:
                    case SDL_BUTTON_MIDDLE:
                        action->action = SECONDARY;
                        break;
                    default:
                        action->action = NONE;
                        break;
                }
                break;
            case SDL_KEYUP:
                SDL_GetMouseState( &x, &y );
                action->x = x;
                action->y = y;
                switch ( event.key.keysym.sym ) {
                    case SDLK_q:
                        action->action = TOGGLE;
                        break;
                    case SDLK_TAB:
                        action->action = TOGGLE_GUI;
                        break;
                    case SDLK_ESCAPE:
                        action->action = SPECIAL_ESCAPE;
                        break;
                    default:
                        action->action = NONE;
                        break;
                }
                break;
        }
        actions.push_back( action );   
    }
    
    Uint8 *keystate = SDL_GetKeyState(NULL);
    if ( ( keystate[SDLK_LEFT] || keystate[SDLK_a] ) && keystate[SDLK_RIGHT]==0 && keystate[SDLK_d]==0 ){
        actions.push_back( new action_t(LEFT) );
    }
    if ( ( keystate[SDLK_RIGHT] || keystate[SDLK_d] ) && keystate[SDLK_LEFT]==0 && keystate[SDLK_a]==0 ){
        actions.push_back( new action_t(RIGHT) );
    }
    if ( ( keystate[SDLK_UP] || keystate[SDLK_w] ) && keystate[SDLK_DOWN]==0 && keystate[SDLK_s]==0 ){
        actions.push_back( new action_t(UP) );
    }
    if ( ( keystate[SDLK_DOWN] || keystate[SDLK_s] ) && keystate[SDLK_UP]==0 && keystate[SDLK_w]==0 ){
        actions.push_back( new action_t(DOWN) );
    }
    
    return actions;
}