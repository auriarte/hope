#ifndef __WINDOW_H_
#define __WINDOW_H_

#include "Global.h"

const int triangle=1;

class Window {
public:
            Window( int screenWidth, int screenHeight, int screenBpp );
            ~Window();

private:
    void    initGL();
    void    RenderGameMap2();
    void    DrawBox( float x, float y, float w, float h, mapTileStyle_t color );

    int     _screenWidth;
    int     _screenHeight;
    
};

#endif