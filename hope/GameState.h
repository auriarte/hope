#include "Map.h"
#include "Actor.h"
#include "CollisionWorld.h"

struct point2_t {
    int x;
    int y;
};

enum gameStateFSM_t {
    GS_FSM_GUI,
    GS_FSM_RENDER,
    GS_FSM_NAVIGATION
};

class GameState {
public:
    //*********************************
    //*         GLOBAL VARS           *
    //*********************************
                    GameState();
                    ~GameState();
    
    gameStateFSM_t  state;
    
    // UI state
    struct uiState_t {
        Uint16  mouseX; // it cannot be grater than 65,535
        Uint16  mouseY;
        bool    mouseLeft;
        bool    mouseLeftPressed;
        bool    mouseLeftReleased;
        bool    mouseOverMenu;        
        Sint8   mouseScroll;
        Uint32  hotItem;
        Uint32  hotToBeItem;        
        Uint32  activeItem;
    } ui = {0,0,false,false,0,0,0};
    
    void        UpdateUiMouse(bool left);
    void        ClearUiMouse();
    Sint16      emptyScroll;
    bool        showGUI;
    
    //*********************************
    //*        GAME FLOW VARS         *
    //*********************************
    
    // DemoRender ---------------------
    bool                renderQuad;
    bool                renderCube;
    bool                cameraView;
    point2_t            position;
    point2_t            speed;
    Map *               map;
    CollisionWorld *    collisionWorld;
    std::vector<Actor>  actors;
    Actor *             actorCurrent;
    
    // DemoGUI -------------------------  
    Sint16      sampleScroll1;    
    Sint16      sampleScroll2;
    bool        showArea2;
    int         numLabels;
    bool        optionsExpanded;
    float       sliderValue;
    
    // DemoNavigation ------------------ 
    
};
