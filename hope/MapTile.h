#ifndef __MAPTILE_H_
#define __MAPTILE_H_

enum mapTileType_t {
    MAP_EMPTY,
    MAP_BLOCK
};
enum mapTileStyle_t {
    MAP_EMPTY_STREET,
    MAP_EMPTY_UNDEVELOPED,
    MAP_EMPTY_CONCRETE,
    MAP_EMPTY_GREEN,
    MAP_BLOCK_RESIDENTIAL,
    MAP_BLOCK_COMERCIAL,
    MAP_BLOCK_INDUSTRIAL
};

class MapTile{
public:
                        MapTile();
                        ~MapTile();
    mapTileType_t       type;
    mapTileStyle_t      style;
};

#endif