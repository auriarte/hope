#include "GameState.h"

GameState::GameState() {
    // Global
    showGUI = true;
    state = GS_FSM_NAVIGATION;
    emptyScroll = 0;
    
    // DemoRender ---------------------
    renderQuad = true;
    renderCube = true;
    position.x = 0;
    position.y = 0;
    speed.x = 0;
    speed.y = 0;
    map = new Map();
    
    // DemoGUI -------------------------
    sampleScroll1 = 0;    
    sampleScroll2 = 0;
    showArea2 = false;
    numLabels = 2;
    optionsExpanded = false;
    sliderValue = 0;
    
    // DemoNavigation ------------------
}

void GameState::UpdateUiMouse(bool left) {
    ui.mouseLeftPressed = !ui.mouseLeft && left;
	ui.mouseLeftReleased = ui.mouseLeft && !left;
    ui.mouseLeft = left;
}

void GameState::ClearUiMouse() {
	ui.mouseLeftPressed = false;
	ui.mouseLeftReleased = false;
}