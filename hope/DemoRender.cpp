

#include "DemoRender.h"
#include "Cam.h"



DemoRender* DemoRender::Instance()
{
    static DemoRender instance;
    return &instance;
}

DemoRender::DemoRender() {
    GameStateS::Instance()->map->parseTilesFromFile();
    Image* image = loadBMP("images/brk.bmp");
	_textureId = loadTexture(image);
	delete image;
    
    std::string test1= "meshes/building7.obj";
    _test1.LoadModel(test1.c_str());
    
    std::string test2= "meshes/new_building.obj";
    _test2.LoadModel(test2.c_str());
    
    
    std::string TestTerrain = "images/heightmap.bmp";
    _land.loadTerrain(TestTerrain.c_str(), 20);

}

void DemoRender::HandleInput( std::list<action_t*> actions ) {
    while ( !actions.empty() ) {
        action_t * action = actions.front();
        actions.pop_front();
        switch( action->action ){
            case TOGGLE:
                //GameStateS::Instance()->renderQuad = !GameStateS::Instance()->renderQuad;
                break;
            case PRIMARY:
                //GameStateS::Instance()->cameraView = !GameStateS::Instance()->cameraView;
                break;
            case SPECIAL_ESCAPE:
                GameStateS::Instance()->state = GS_FSM_GUI;
                break;
            case LEFT:
                // TODO Needs improvement in order to handle things like pressing both keys and releasing one
                GameStateS::Instance()->speed.x = GameStateS::Instance()->speed.x==0 ? -40: 0;
                break;
            case RIGHT:
                GameStateS::Instance()->speed.x = GameStateS::Instance()->speed.x==0 ? +40: 0;
                break;
            case UP:
                GameStateS::Instance()->speed.y = GameStateS::Instance()->speed.y==0 ? -40: 0;
                break;
            case DOWN:
                GameStateS::Instance()->speed.y = GameStateS::Instance()->speed.y==0 ? +40: 0;
                break;
            default:
                break;
        }
        delete action;
    }

}

void DemoRender::HandleCollisions( std::list<collision_t*> collisions ) {
    
}

void DemoRender::Update() {   
    GameStateS::Instance()->position.x += GameStateS::Instance()->speed.x * 0.001f;
    GameStateS::Instance()->position.y += GameStateS::Instance()->speed.y * 0.001f;
    UpdateCamera();  
}

void DemoRender::Render() {
    
    glViewport(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
    glClearColor(0.3f, 0.3f, 0.32f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glDisable(GL_TEXTURE_2D);
    
    // set perspective viewing frustum
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(60.0f, (float)SCREEN_WIDTH/(float)SCREEN_HEIGHT, 0.1f, 10000.0f); // FOV, AspectRatio, NearClip, FarClip
    
    // switch to modelview matrix in order to set scene
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    
    _cam.setPosition(GameStateS::Instance()->map->g_cameraPos[0],GameStateS::Instance()->map->g_cameraPos[1],10.0);
    //cam.setViewDir(1, 1, -1);
    
    _cam.Render();
    
    // activate and specify pointer to vertex array
    
    glEnable(GL_LIGHTING); //Enable lighting
	glEnable(GL_LIGHT0); //Enable light #0
    glEnable(GL_NORMALIZE);
    
    //Add ambient light
	GLfloat ambientColor[] = {0.2f, 0.2f, 0.2f, 1.0f}; //Color (0.2, 0.2, 0.2)
	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, ambientColor);
	
	//Add positioned light
	GLfloat lightColor0[] = {0.5f, 0.5f, 0.5f, 1.0f}; //Color (0.5, 0.5, 0.5)
	GLfloat lightPos0[] = {8.0f, 0.0f, 4.0f, 1.0f}; //Positioned at (8, 0, 4)
	glLightfv(GL_LIGHT0, GL_DIFFUSE, lightColor0);
	glLightfv(GL_LIGHT0, GL_POSITION, lightPos0);
	
    
   // drawing the sample objects
  
    glPushMatrix();
    glRotated(90, 0, 1, 0);
    
    _test1.Draw();
    
    glPopMatrix();
    
    //// test2
    glPushMatrix();
    glTranslatef(4.0f, 0.0f, 0.0f);
    
    _test2.Draw();
    
    glPopMatrix();

    // test for labd terrain
    glPushMatrix();
    glTranslatef(-8.0f, 0.0f, 0.0f);
    _land.draw();
    
    glPopMatrix();
    
    
    
    glBegin(GL_LINES);
    glColor3f(1, 1, 0);
    glVertex3f(1, 0, 0);
    glVertex3f(0, 1, 0);
    //    glVertex3f(0, 0, 1);
    glEnd();
    glFlush();
    
    DrawCube(2.0 , 2.0 , 1.0 , 0.5);
    DrawCube(-2.0 , 2.0, 0.5, 0.5);
    DrawCube(-1.5, -1.5, 1.0 ,0.5);
    
    glPushMatrix();
    glTranslatef(-2, 2, 0); // move to upper left corner
    glBegin(GL_TRIANGLES);
    // front faces
    glNormal3f(0,0,1);
    // face v0-v1-v2
    glColor3f(1,1,1);
    glVertex3f(1,1,1);
    glColor3f(1,1,0);
    glVertex3f(-1,1,1);
    glColor3f(1,0,0);
    glVertex3f(-1,-1,1);
    // face v2-v3-v0
    glColor3f(1,0,0);
    glVertex3f(-1,-1,1);
    glColor3f(1,0,1);
    glVertex3f(1,-1,1);
    glColor3f(1,1,1);
    glVertex3f(1,1,1);
    
    // right faces
    glNormal3f(1,0,0);
    // face v0-v3-v4
    glColor3f(1,1,1);
    glVertex3f(1,1,1);
    glColor3f(1,0,1);
    glVertex3f(1,-1,1);
    glColor3f(0,0,1);
    glVertex3f(1,-1,-1);
    // face v4-v5-v0
    glColor3f(0,0,1);
    glVertex3f(1,-1,-1);
    glColor3f(0,1,1);
    glVertex3f(1,1,-1);
    glColor3f(1,1,1);
    glVertex3f(1,1,1);
    
    // top faces
    glNormal3f(0,1,0);
    // face v0-v5-v6
    glColor3f(1,1,1);
    glVertex3f(1,1,1);
    glColor3f(0,1,1);
    glVertex3f(1,1,-1);
    glColor3f(0,1,0);
    glVertex3f(-1,1,-1);
    // face v6-v1-v0
    glColor3f(0,1,0);
    glVertex3f(-1,1,-1);
    glColor3f(1,1,0);
    glVertex3f(-1,1,1);
    glColor3f(1,1,1);
    glVertex3f(1,1,1);
    
    // left faces
    glNormal3f(-1,0,0);
    // face  v1-v6-v7
    glColor3f(1,1,0);
    glVertex3f(-1,1,1);
    glColor3f(0,1,0);
    glVertex3f(-1,1,-1);
    glColor3f(0,0,0);
    glVertex3f(-1,-1,-1);
    // face v7-v2-v1
    glColor3f(0,0,0);
    glVertex3f(-1,-1,-1);
    glColor3f(1,0,0);
    glVertex3f(-1,-1,1);
    glColor3f(1,1,0);
    glVertex3f(-1,1,1);
    
    // bottom faces
    glNormal3f(0,-1,0);
    // face v7-v4-v3
    glColor3f(0,0,0);
    glVertex3f(-1,-1,-1);
    glColor3f(0,0,1);
    glVertex3f(1,-1,-1);
    glColor3f(1,0,1);
    glVertex3f(1,-1,1);
    // face v3-v2-v7
    glColor3f(1,0,1);
    glVertex3f(1,-1,1);
    glColor3f(1,0,0);
    glVertex3f(-1,-1,1);
    glColor3f(0,0,0);
    glVertex3f(-1,-1,-1);
    
    // back faces
    glNormal3f(0,0,-1);
    // face v4-v7-v6
    glColor3f(0,0,1);
    glVertex3f(1,-1,-1);
    glColor3f(0,0,0);
    glVertex3f(-1,-1,-1);
    glColor3f(0,1,0);
    glVertex3f(-1,1,-1);
    // face v6-v5-v4
    glColor3f(0,1,0);
    glVertex3f(-1,1,-1);
    glColor3f(0,1,1);
    glVertex3f(1,1,-1);
    glColor3f(0,0,1);
    glVertex3f(1,-1,-1);
    glEnd();
    
    glPopMatrix();
    
    
  }




void DemoRender::RenderGUI(GUI* gui) {

}


// this function draw a cube in center and with the size
void DemoRender::DrawCube(float posx, float posy , float size , float hieght) {
    
    glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, _textureId);
    
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    
    glBegin( GL_QUADS );
    // front face
    //glColor3f(2.0, 0.0, 0.0);
    glTexCoord2f(0.0f, 0.0f);
    glVertex3f( posx +size/2 , posy +size/2, hieght );
    glTexCoord2f(1.0f, 0.0f);
    glVertex3f( posx -size/2 , posy +size/2, hieght );
    glTexCoord2f(1.0f, 1.0f);
    glVertex3f( posx -size/2 , posy -size/2, hieght );
    glTexCoord2f(0.0f, 1.0f);
    glVertex3f( posx +size/2 , posy -size/2, hieght );
    
    // left face
    //glColor3f(0.0, 2.0, 0.0);
    glVertex3f( posx -size/2 , posy +size/2, hieght );
    glVertex3f( posx -size/2 , posy +size/2, 0 );
    glVertex3f( posx -size/2 , posy -size/2, 0 );
    glVertex3f( posx -size/2 , posy -size/2, hieght );
    
    // back face
    //glColor3f(0.0, 0.0, 2.0);
    glVertex3f( posx +size/2 , posy +size/2, 0 );
    glVertex3f( posx -size/2 , posy +size/2, 0 );
    glVertex3f( posx -size/2 , posy -size/2, 0 );
    glVertex3f( posx +size/2 , posy -size/2, 0 );
    
    // rigth face
    //glColor3f(2.0, 2.0, 2.0);
    glVertex3f( posx +size/2 , posy +size/2, 0 );
    glVertex3f( posx +size/2 , posy +size/2, hieght );
    glVertex3f( posx +size/2 , posy -size/2, hieght );
    glVertex3f( posx +size/2 , posy -size/2, 0 );
    
    // top face
    //glColor3f(2.0, 0.0, 2.0);
    glVertex3f( posx +size/2 , posy +size/2, hieght );
    glVertex3f( posx -size/2 , posy +size/2, hieght );
    glVertex3f( posx -size/2 , posy +size/2, 0 );
    glVertex3f( posx +size/2 , posy +size/2, 0 );
    
    // bottom face
    //glColor3f(0.0, 2.0, 2.0);
    glVertex3f( posx +size/2 , posy -size/2, hieght );
    glVertex3f( posx -size/2 , posy -size/2, hieght );
    glVertex3f( posx -size/2 , posy -size/2, 0 );
    glVertex3f( posx +size/2 , posy -size/2, 0 );
    
    glEnd();
    glDisable(GL_TEXTURE_2D);
    
}


GLuint DemoRender::loadTexture(Image* image) {
	GLuint textureId;
	glGenTextures(1, &textureId); //Make room for our texture
	glBindTexture(GL_TEXTURE_2D, textureId); //Tell OpenGL which texture to edit
	//Map the image to the texture
	glTexImage2D(GL_TEXTURE_2D,                //Always GL_TEXTURE_2D
				 0,                            //0 for now
				 GL_RGB,                       //Format OpenGL uses for image
				 image->width, image->height,  //Width and height
				 0,                            //The border of the image
				 GL_RGB, //GL_RGB, because pixels are stored in RGB format
				 GL_UNSIGNED_BYTE, //GL_UNSIGNED_BYTE, because pixels are stored
                 //as unsigned numbers
				 image->pixels);               //The actual pixel data
	return textureId; //Returns the id of the texture
}


void DemoRender::RenderGameMap() {
    glViewport(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
    glClearColor(0.3f, 0.5f, 0.9f, 0.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(60, (float)SCREEN_WIDTH / (float)SCREEN_HEIGHT, 10, 0.1);
    
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    gluLookAt(GameStateS::Instance()->map->g_cameraPos[0], GameStateS::Instance()->map->g_cameraPos[1], GameStateS::Instance()->map->g_cameraPos[2],
              GameStateS::Instance()->map->g_targetPos[0], GameStateS::Instance()->map->g_targetPos[1], GameStateS::Instance()->map->g_targetPos[2],
              0.0f, 1.0f, 0.0f);
    
    glRotatef(GameStateS::Instance()->map->g_pitch, 1.0f, 0.0f, 0.0f);
    glRotatef(GameStateS::Instance()->map->g_heading, 0.0f, 1.0f, 0.0f);
    
    const ModelOBJ::Mesh *pMesh = 0;
    const ModelOBJ::Vertex *pVertices = 0;
    ModelOBJ g_model;
    g_model = GameStateS::Instance()->map->g_model;
    float * vertexarray;
    vertexarray = GameStateS::Instance()->map->vertexarray;
    float color[4] = {1.0f,0.0f,0.0f,1.0f};
    
    glClear(GL_COLOR_BUFFER_BIT );
    glColor4f(1.0f, 0.0f, 0.0f, 0.75f);
    for (int i = 0; i < 3; ++i)  {
        if(i > 1)
            glColor4f(1.0f, 1.0f, 0.0f, 0.75f);
        
        pMesh = &g_model.getMesh(0);
        pVertices = g_model.getVertexBuffer();
        
        glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, color);
        glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, color);
        glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, color);
        glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, 0.5f * 128.0f);
        
        
        glDisable(GL_TEXTURE_2D);
        
        if (g_model.hasPositions()) {
            glEnableClientState(GL_VERTEX_ARRAY);
            glVertexPointer(3, GL_FLOAT, 0, &vertexarray[i*3*g_model.getNumberOfVertices()]);
        }
        
        if (g_model.hasTextureCoords()) {
            glEnableClientState(GL_TEXTURE_COORD_ARRAY);
            glTexCoordPointer(2, GL_FLOAT, g_model.getVertexSize(), g_model.getVertexBuffer()->texCoord);
        }
        
        if (g_model.hasNormals()) {
            glEnableClientState(GL_NORMAL_ARRAY);
            glNormalPointer(GL_FLOAT, g_model.getVertexSize(), g_model.getVertexBuffer()->normal);
        }
        
        glDrawElements(GL_TRIANGLES, pMesh->triangleCount * 3, GL_UNSIGNED_INT, g_model.getIndexBuffer() + pMesh->startIndex);
        
        if (g_model.hasNormals())
            glDisableClientState(GL_NORMAL_ARRAY);
        
        if (g_model.hasTextureCoords())
            glDisableClientState(GL_TEXTURE_COORD_ARRAY);
        
        if (g_model.hasPositions())
            glDisableClientState(GL_VERTEX_ARRAY);
    }
}



void DemoRender::RenderGameActors() {
    if( GameStateS::Instance()->renderCube == true ) {
        float x = ( (float)GameStateS::Instance()->position.x / SCREEN_WIDTH * 2.0f ) - 1.0f;
        float y = ( (float)GameStateS::Instance()->position.y / SCREEN_HEIGHT * -2.0f ) + 1.0f;
        
        DrawCube(x, y, 0.1, 0.4);
    }
}

void DemoRender::UpdateCamera(){
    GameStateS::Instance()->map->g_targetPos[0] += GameStateS::Instance()->speed.x * 0.001f;
    GameStateS::Instance()->map->g_targetPos[2] += GameStateS::Instance()->speed.y * 0.001f;
    GameStateS::Instance()->map->g_targetPos[1]=0.001f;
    if(!GameStateS::Instance()->cameraView){
        GameStateS::Instance()->map->g_cameraPos[0] = GameStateS::Instance()->map->g_targetPos[0];
        GameStateS::Instance()->map->g_cameraPos[1] = GameStateS::Instance()->map->g_targetPos[1] + 0.1f;
        GameStateS::Instance()->map->g_cameraPos[2] = GameStateS::Instance()->map->g_targetPos[2] +0.01f;
    } else {
        GameStateS::Instance()->map->g_cameraPos[0] = GameStateS::Instance()->map->g_targetPos[0];
        GameStateS::Instance()->map->g_cameraPos[1] = 0.001f;
        GameStateS::Instance()->map->g_cameraPos[2] = GameStateS::Instance()->map->g_targetPos[2] -0.001f;
    }
    
}